package com.playdeezgames.common.statemachine;

import com.badlogic.gdx.scenes.scene2d.EventListener;

public interface State extends EventListener{

	void dispose();

	void render();

	void resize(int width, int height);

	void pause();

	void resume();

	void create();

	void finish();

	void start();
}
