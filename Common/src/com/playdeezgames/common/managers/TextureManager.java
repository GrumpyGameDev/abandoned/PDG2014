package com.playdeezgames.common.managers;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.graphics.Texture;
import com.playdeezgames.common.interfaces.FileDescriptor;
import com.playdeezgames.common.interfaces.TextureDescriptor;

public class TextureManager<T extends TextureDescriptor<U>, U extends FileDescriptor> extends ManagerBase {

	private FileManager<U> fileManager;
	private Map<T,Texture> table = new HashMap<T,Texture>();
	
	public TextureManager(){
	}
	public TextureManager(FileManager<U> fileManager){
		setFileManager(fileManager);
	}
	
	public Texture getTexture(T descriptor){
		if(table.containsKey(descriptor)){
			return table.get(descriptor);
		}else{
			Texture texture = new Texture(getFileManager().getFile(descriptor.getFileDescriptor()));
			texture.setFilter(descriptor.getMinFilter(), descriptor.getMagFilter());
			table.put(descriptor,texture);
			return texture;
		}
	}
	
	@Override
	public void dispose() {
		for(Texture texture : table.values()){
			texture.dispose();
		}
		table.clear();
		getFileManager().dispose();
	}
	public FileManager<U> getFileManager() {
		if(fileManager==null){
			fileManager = new FileManager<U>();
		}
		return fileManager;
	}
	private void setFileManager(FileManager<U> fileManager) {
		this.fileManager = fileManager;
	}

}
