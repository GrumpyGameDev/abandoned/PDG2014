package com.pdg.dungeondelver.pdgboardgames;

public interface PortalFactory<PortalType extends MazePortalBase> {
	PortalType newInstance();
}
