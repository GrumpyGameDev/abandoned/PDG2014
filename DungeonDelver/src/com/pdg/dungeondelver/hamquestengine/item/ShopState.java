package com.pdg.dungeondelver.hamquestengine.item;

import java.util.ArrayList;
import java.util.List;

import com.pdg.dungeondelver.hamquestengine.creatures.PlayerDescriptor;
import com.pdg.dungeondelver.hamquestengine.descriptorproperties.misc.ShopInventoryEntry;
import com.pdg.dungeondelver.hamquestengine.gameconstants.Properties;
import com.pdg.dungeondelver.hamquestengine.gamecore.Game;
import com.pdg.dungeondelver.hamquestengine.gamecore.GameClientBase;
import com.pdg.dungeondelver.pdgboardgames.CountedCollection;

public class ShopState extends GameClientBase
{
    private String shopTitle = "";
    public String getShopTitle()
    {
            return shopTitle;
    }
    private PlayerDescriptor playerDescriptor;
    private CountedCollection<String> shopItems = new CountedCollection<String>();
    public CountedCollection<String> getShopItems()
    {
            return shopItems;
    }
    private List<String> itemList = new ArrayList<String>();
    public List<String> getItemList()
    {
            return itemList;
    }
    public boolean canBuyOne(String identifier)
    {
        Descriptor descriptor = getGame().getTableSet().getItemTable().getItemDescriptor(identifier);
        return (getShopItems().get(identifier) > 0) && (playerDescriptor.getMoney() >= descriptor.<Integer>getProperty(Properties.Price));
    }
    public boolean canBuyAll(String identifier)
    {
        Descriptor descriptor = getGame().getTableSet().getItemTable().getItemDescriptor(identifier);
        return (getShopItems().get(identifier) > 0) && (playerDescriptor.getMoney() >= descriptor.<Integer>getProperty(Properties.Price) * getShopItems().get(identifier));
    }
    public boolean buyOne(String identifier)
    {
        if (!canBuyOne(identifier)) return false;
        Descriptor descriptor = getGame().getTableSet().getItemTable().getItemDescriptor(identifier);
        playerDescriptor.addItem(identifier);
        getShopItems().remove(identifier);
        playerDescriptor.setMoney(playerDescriptor.getMoney()-descriptor.<Integer>getProperty(Properties.Price));
        return true;
    }
    public boolean buyAll(String identifier)
    {
        if (!canBuyAll(identifier)) return false;
        Descriptor descriptor = getGame().getTableSet().getItemTable().getItemDescriptor(identifier);
        int count = getShopItems().get(identifier);
        playerDescriptor.setMoney(playerDescriptor.getMoney()-descriptor.<Integer>getProperty(Properties.Price) * count);
        getShopItems().remove(identifier, count);
        while (count > 0)
        {
            playerDescriptor.addItem(identifier);
            count--;
        }
        return true;
    }
    private int itemIndex = 0;
    public int getItemIndex()
    {
            return itemIndex;
        }
    public void setItemIndex(int value){
        if (value >= 0 && value < itemList.size())
            itemIndex = value;
    }
    
    public ShopState(Descriptor shopDescriptor, PlayerDescriptor thePlayerDescriptor,Game theGame)
    {
    	super(theGame);
        shopTitle = getGame().getTableSet().getMessageTable().translateMessageText(shopDescriptor.<String>getProperty(Properties.ShopTitle));
        playerDescriptor = thePlayerDescriptor;
        for (ShopInventoryEntry entry : shopDescriptor.<List<ShopInventoryEntry>>getProperty(Properties.Inventory))
        {
            for (int index = 0; index < entry.getNumberInStock(); ++index)
            {
                if (getGame().getRandomNumberGenerator().next(entry.getNumberInStockDie()) < entry.getNumberInStockRoll())
                {
                    shopItems.add(entry.getItemIdentifier());
                    if (!itemList.contains(entry.getItemIdentifier()))
                    {
                        itemList.add(entry.getItemIdentifier());
                    }
                }
            }
        }
    }
}

