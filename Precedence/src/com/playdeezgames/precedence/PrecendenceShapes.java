package com.playdeezgames.precedence;

public enum PrecendenceShapes {
	CIRCLE,
	TRIANGLE,
	SQUARE,
	PENTAGON,
	HEXAGON,
	OCTAGON;
}
