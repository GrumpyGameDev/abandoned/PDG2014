package com.playdeezgames.splorr.items;

import java.io.Serializable;

import com.playdeezgames.splorr.enums.Items;

public class SimpleItem implements IItem, Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8031912507452218786L;
	private Items item;
	
	public Items getItem() {
		return item;
	}

	public SimpleItem(Items item){
		this.item=item;
	}

}
