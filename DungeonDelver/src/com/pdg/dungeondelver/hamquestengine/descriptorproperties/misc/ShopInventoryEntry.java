package com.pdg.dungeondelver.hamquestengine.descriptorproperties.misc;

import com.badlogic.gdx.utils.XmlReader.Element;
import com.pdg.dungeondelver.hamquestengine.gameconstants.Properties;
import com.pdg.dungeondelver.pdgboardgames.IRandomNumberGenerator;

public class ShopInventoryEntry
{
    private String itemIdentifier;
    private int numberInStock;
    private int numberInStockRoll;
    private int numberInStockDie;
    public String getItemIdentifier()
    {
            return itemIdentifier;
    }
    public int getNumberInStock()
    {
            return numberInStock;
    }
    public int getNumberInStockRoll()
    {
            return numberInStockRoll;
    }
    public int getNumberInStockDie()
    {
            return numberInStockDie;
    }
    public int generateNumberInStock(IRandomNumberGenerator theRandomNumberGenerator)
    {
        int total = 0;
        for (int count = 0; count < getNumberInStock(); ++count)
        {
            if (theRandomNumberGenerator.next(6) < numberInStockRoll)
            {
                total++;
            }
        }
        return total;
    }
    public ShopInventoryEntry(String theItemIdentifier, int theNumberInStock, int thenumberInStockRoll)
    {
        itemIdentifier = theItemIdentifier;
        numberInStock = theNumberInStock;
        numberInStockRoll = thenumberInStockRoll;
    }
    public ShopInventoryEntry(Element node)
    {
        for (int index=0;index<node.getChildCount();++index){
        	Element element = node.getChild(index);
            if (element.getName().compareToIgnoreCase(Properties.ItemIdentifier)==0)
            {
                itemIdentifier = element.getText();
            }
            else if (element.getName().compareToIgnoreCase(Properties.NumberInStock)==0)
            {
            	numberInStock = Integer.parseInt(element.getText());
            }
            else if (element.getName().compareToIgnoreCase(Properties.NumberInStockRoll)==0)
            {
            	numberInStockRoll = Integer.parseInt(element.getText());
            }
            else if (element.getName().compareToIgnoreCase(Properties.NumberInStockDie)==0)
            {
            	numberInStockDie = Integer.parseInt(element.getText());
            }
        }
    }

}

