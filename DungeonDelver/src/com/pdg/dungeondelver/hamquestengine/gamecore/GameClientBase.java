package com.pdg.dungeondelver.hamquestengine.gamecore;

public abstract class GameClientBase implements IGameClient
{
    private Game game;
    public Game getGame()
    {
            return game;
    }
    public GameClientBase(Game theGame)
    {
        game = theGame;
    }
}