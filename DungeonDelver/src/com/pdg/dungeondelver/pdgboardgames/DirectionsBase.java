package com.pdg.dungeondelver.pdgboardgames;

public abstract class DirectionsBase
{
    public abstract int getCount();
    public abstract int getOpposite(int direction);
    public abstract int getNextColumn(int startColumn, int startRow, int direction);
    public abstract int getNextRow(int startColumn, int startRow, int direction);
}