package com.pdg.dungeondelver.hamquestengine.descriptorproperties.playerspecific;

import com.pdg.dungeondelver.hamquestengine.descriptorproperties.misc.IStatisticHolder;

public class PlayerStatisticHolder implements IStatisticHolder
{
    public PlayerStatisticHolderDelegate GetValue;
    public PlayerStatisticHolder()
    {
    }
	@Override
	public int getValue() {
		return GetValue.get();
	}
}
