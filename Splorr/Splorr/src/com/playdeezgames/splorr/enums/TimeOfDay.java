package com.playdeezgames.splorr.enums;

public enum TimeOfDay {
	DAWN(0,1.0/86400,"It is dawn."),
	EARLY_MORNING(1.0/86400,0.125,"It is early morning."),
	MIDMORNING(0.125,0.125+1.0/86400,"It is mid-morning."),
	LATE_MORNING(0.125+1.0/86400,0.25,"It is late morning."),
	NOON(0.25,0.25+1.0/86400,"It is noon."),
	EARLY_AFTERNOON(0.25+1.0/86400,0.375,"It is early afternoon."),
	LATE_AFTERNOON(0.375,0.5,"It is late afternoon."),
	DUSK(0.5,0.5+1.0/86400,"It is dusk."),
	EARLY_EVENING(0.5+1.0/86400,0.625,"It is early evening."),
	LATE_EVENING(0.625,0.75,"It is late evening."),
	MIDNIGHT(0.75,0.75+1.0/86400,"It is midnight."),
	PRE_DAWN(0.75+1.0/86400,1.0,"It is pre-dawn.")
	;
	private double start;
	private double end;
	private String description;

	TimeOfDay(double start,double end, String description){
		this.start=start;
		this.end=end;
		this.description = description;
	}
	public double getStart(){
		return this.start;
	}
	public double getEnd(){
		return this.end;
	}
	public String getDescription(){
		return this.description;
	}
}
