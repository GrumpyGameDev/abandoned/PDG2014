package com.pdg.dungeondelver.hamquestengine.descriptorproperties.rollers;

import com.pdg.dungeondelver.hamquestengine.gamecore.Game;
import com.pdg.dungeondelver.hamquestengine.item.Descriptor;

public interface IRoller
{
    int roll(Descriptor theDescriptor,Game theGame);
    int getMaximumRoll(Descriptor theDescriptor, Game theGame);
    int getMinimumRoll(Descriptor theDescriptor, Game theGame);
}
