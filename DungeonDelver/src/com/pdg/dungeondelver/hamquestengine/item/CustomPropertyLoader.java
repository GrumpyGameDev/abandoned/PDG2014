package com.pdg.dungeondelver.hamquestengine.item;

import com.badlogic.gdx.utils.XmlReader.Element;

public interface CustomPropertyLoader {
	Object loadFromNode(Element node);	
}
