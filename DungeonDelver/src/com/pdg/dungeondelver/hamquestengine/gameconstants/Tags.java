package com.pdg.dungeondelver.hamquestengine.gameconstants;

public class Tags {
    public static final String Durable = "durable";
    public static final String Exit = "exit";
    public static final String Item = "item";
    public static final String Portal = "portal";
    public static final String Quest = "quest";
    public static final String Spawns = "spawns";
    public static final String Trap = "trap";
}
