package com.pdg.dungeondelver.hamquestengine.descriptorproperties.movers;

public class DoMoveResult {
	private int column;
	private int row;
	public int getColumn(){
		return column;
	}
	public int getRow(){
		return row;
	}
	public DoMoveResult(int column,int row){
		this.column=column;
		this.row=row;
	}
}
