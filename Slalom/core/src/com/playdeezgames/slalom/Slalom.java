package com.playdeezgames.slalom;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

public class Slalom extends ApplicationAdapter {
	
	private Stage stage;
	
	@Override
	public void create () {
		stage = new Stage(new ScreenViewport());
		BlockActor actor = new BlockActor();
		actor.setX(0);
		actor.setY(0);
		actor.setWidth(100);
		actor.setHeight(100);
		MoveToAction action = Actions.moveTo(1180, 620, 1);
		action.setInterpolation(Interpolation.bounceOut);
		actor.addAction(action);
		stage.addActor(actor);
		Gdx.input.setInputProcessor(stage);
	}

	@Override
	public void render() {
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		stage.act(Gdx.graphics.getDeltaTime());
		stage.draw();
	}
	
	@Override
	public void resize(int width,int height){
		stage.getViewport().update(width, height, true);
	}
	
	@Override
	public void dispose(){
		stage.dispose();
	}
}
