package com.pdg.dungeondelver.hamquestengine.descriptorproperties.playerspecific;

import java.util.Set;

public interface IMessageStatistics
{
    Set<String> getEntries();
    void addEntry(String entry);
    int getCount(String entry);
}
