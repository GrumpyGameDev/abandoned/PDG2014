package com.pdg.dungeondelver.hamquestengine.descriptorproperties.misc;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.utils.XmlReader.Element;
import com.pdg.dungeondelver.hamquestengine.item.CustomPropertyLoader;

public class ShopInventoryEntryLoader implements CustomPropertyLoader{
    public Object loadFromNode(Element node)
    {
        List<ShopInventoryEntry> entries = new ArrayList<ShopInventoryEntry>();
        for(int index=0;index<node.getChildCount();++index){
            Element element = node.getChild(index);
            if (element != null)
            {
                entries.add(new ShopInventoryEntry(element));
            }
        }
        return entries;
    }
}
