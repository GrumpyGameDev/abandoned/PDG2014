package com.pdg.dungeondelver.hamquestengine.gameconstants;

public class ItemTypes {
    public static final String Armor = "Armor";
    public static final String Chest = "Chest";
    public static final String Door = "Door";
    public static final String Exit = "Exit";
    public static final String Healing = "Healing";
    public static final String Hidden = "Hidden";
    public static final String Key = "Key";
    public static final String Light = "Light";
    public static final String Portal = "Portal";
    public static final String Quest = "Quest";
    public static final String Shop = "Shop";
    public static final String Trap = "Trap";
    public static final String Treasure = "Treasure";
    public static final String Weapon = "Weapon";
}
