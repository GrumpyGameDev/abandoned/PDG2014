package com.pdg.dungeondelver.hamquestengine.gamecore;

public interface IGameClient
{
    Game getGame();
}
