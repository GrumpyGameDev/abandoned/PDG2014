package com.pdg.dungeondelver.pdgboardgames;

public interface DirectionsFactory<DirectionsType extends DirectionsBase> {
	DirectionsType newInstance();
}
