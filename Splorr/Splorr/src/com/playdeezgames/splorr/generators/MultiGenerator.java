package com.playdeezgames.splorr.generators;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class MultiGenerator<T> implements IGenerator<List<T>> {
	
	private Map<T,IGenerator<Boolean>> itemGenerators = new HashMap<T, IGenerator<Boolean>>();
	
	public MultiGenerator<T> put(T key,IGenerator<Boolean> generator){
		if(generator==null){
			itemGenerators.remove(key);
		}else{
			itemGenerators.put(key,generator);
		}
		return this;
	}

	public List<T> generate(Random random) {
		List<T> items = new ArrayList<T>();
		for(T key : itemGenerators.keySet()){
			if(itemGenerators.get(key).generate(random)){
				items.add(key);
			}
		}
		return items;
	}
	
}
