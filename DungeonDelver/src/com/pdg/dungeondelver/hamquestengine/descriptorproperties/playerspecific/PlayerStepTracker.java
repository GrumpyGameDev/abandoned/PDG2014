package com.pdg.dungeondelver.hamquestengine.descriptorproperties.playerspecific;

public class PlayerStepTracker implements IStepTracker
{
    private int steps = 0;
    private int total = 0;
    private StepTrackerEndOfTurnCallback onEndOfTurn=null;

    public int getSteps()
    {
        return steps; 
    }

    public int getTotal()
    {
        return total;
    }

    public void reset()
    {
        steps = 0;
    }

    public void reset(int newTotal)
    {
        total = newTotal;
        reset();
    }

    public void doStep()
    {
        steps++;
        if (getSteps() >= getTotal())
        {
            if (getOnEndOfTurn() != null)
            {
                getOnEndOfTurn().endOfTurn();;
            }
        }
    }

    public StepTrackerEndOfTurnCallback getOnEndOfTurn()
        { return onEndOfTurn; }
        public void setOnEndOfTurn(StepTrackerEndOfTurnCallback value) { onEndOfTurn = value; }
}
