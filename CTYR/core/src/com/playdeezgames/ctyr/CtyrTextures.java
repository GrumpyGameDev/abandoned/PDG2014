package com.playdeezgames.ctyr;

public enum CtyrTextures implements TextureDescriptor{
	BAD_LOGIC("badlogic.jpg"),
	RHOMBUS("rhombus.png"),
	ROMFONT_8X8("romfont8x8.png")
	;
	private String filename;
	CtyrTextures(String filename){
		this.filename=filename;
	}
	@Override
	public String getFilename() {
		return filename;
	}
}
