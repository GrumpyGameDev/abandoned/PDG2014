package com.playdeezgames.common.managers;

public abstract class ManagerBase {
	public abstract void dispose();
}
