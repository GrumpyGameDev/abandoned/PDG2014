package com.playdeezgames.splorr.tokenprocessors;

import java.util.Stack;

import com.playdeezgames.splorr.enums.Hands;

public class CommonPhraseParsers {
	private static final String WITH = "with";
	private static final String HAND = "hand";
	private static final String LEFT = "left";
	private static final String RIGHT = "right";

	public static Hands WithHandParser(Stack<String> tokenizer){
		if(tokenizer.size()<3){
			return null;
		}
		String withConst = tokenizer.pop();
		String which = tokenizer.pop();
		String handConst = tokenizer.pop();
		
		if(withConst.compareToIgnoreCase(WITH)!=0 ||
				handConst.compareToIgnoreCase(HAND)!=0){
			return null;
		}else{
			if(which.compareToIgnoreCase(LEFT)==0){
				return Hands.LEFT;
			}else if(which.compareTo(RIGHT)==0){
				return Hands.RIGHT;
			}else{
				return null;
			}
		}
	}
}
