package com.playdeezgames.splorr.world;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class World implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7866575171477221856L;
	private Map<Integer,WorldColumn> columns = new HashMap<Integer,WorldColumn>();
	
	public WorldColumn get(Integer column){
		if(columns.containsKey(column)){
			return columns.get(column);
		}else{
			WorldColumn worldColumn = new WorldColumn();
			columns.put(column, worldColumn);
			return worldColumn;
		}
	}
}
