package com.pdg.dungeondelver.hamquestengine.descriptorproperties.playerspecific;

public interface IStepTracker
{
    int getSteps();
    int getTotal();
    void reset();
    void reset(int newTotal);
    void doStep();
    StepTrackerEndOfTurnCallback getOnEndOfTurn();
}
