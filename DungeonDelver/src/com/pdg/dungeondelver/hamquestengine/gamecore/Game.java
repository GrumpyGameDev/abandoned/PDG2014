package com.pdg.dungeondelver.hamquestengine.gamecore;

import com.pdg.dungeondelver.hamquestengine.gameconstants.Properties;
import com.pdg.dungeondelver.hamquestengine.gameconstants.PropertyGroups;
import com.pdg.dungeondelver.hamquestengine.tables.CreatureTable;
import com.pdg.dungeondelver.hamquestengine.tables.ItemTable;
import com.pdg.dungeondelver.hamquestengine.tables.MessageTable;
import com.pdg.dungeondelver.hamquestengine.tables.PropertyGroupTable;
import com.pdg.dungeondelver.hamquestengine.tables.TableSet;
import com.pdg.dungeondelver.hamquestengine.tables.TerrainTable;
import com.pdg.dungeondelver.pdgboardgames.IRandomNumberGenerator;

public class Game
{
    public IRandomNumberGenerator getRandomNumberGenerator()
    {
        return getTableSet().getPropertyGroupTable().getPropertyDescriptor(PropertyGroups.GameConfiguration).<IRandomNumberGenerator>getProperty(Properties.RandomNumberGenerator);
    }
    private TableSet tableSet;
    public TableSet getTableSet()
    {
            return tableSet;
    }
    //private Maze maze;
    //public Maze Maze
    //{
    //    get
    //    {
    //        return maze;
    //    }
    //}
    private MessageQueue messageQueue = null;
    public MessageQueue getMessageQueue()
    {
            if (messageQueue == null)
            {
                messageQueue = new MessageQueue(5, this);
            }
            return messageQueue;
    }
    public Game()
    {
    }
    public void Initialize()
    {
        tableSet = new TableSet(new CreatureTable(null), new MessageTable(null),new PropertyGroupTable(null),new TerrainTable(null),new ItemTable(null));
        tableSet.getCreatureTable().reset();
        //maze = new Maze(this);
        //maze.Generate(RandomNumberGenerator);
        //maze.PlayerDescriptor.Initialize();
    }
}
