package com.playdeezgames.precedence.states;

import com.playdeezgames.common.statemachine.StateMachine;
import com.playdeezgames.precedence.ResourceManager;

public abstract class StateBase<T> {
	private ResourceManager resourceManager;
	private StateMachine<T> stateMachine;
	
	public StateBase(StateMachine<T> stateMachine){
		setStateMachine(stateMachine);
	}
	
	private void setStateMachine(StateMachine<T> stateMachine) {
		this.stateMachine=stateMachine;
	}
	
	protected StateMachine<T> getStateMachine(){
		return stateMachine;
	}

	protected ResourceManager getResourceManager() {
		if(resourceManager==null){
			resourceManager = new ResourceManager();
		}
		return resourceManager;
	}

	private void setResourceManager(ResourceManager resourceManager) {
		this.resourceManager = resourceManager;
	}
	
	public StateBase(StateMachine<T> stateMachine, ResourceManager resourceManager){
		setStateMachine(stateMachine);
		setResourceManager(resourceManager);
	}
	
	public void dispose(){
		getResourceManager().dispose();
	}
}
