package com.pdg.dungeondelver.hamquestengine.creatures;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.graphics.Color;
import com.pdg.dungeondelver.hamquestengine.descriptorproperties.playerspecific.PlayerMessageStatistics;
import com.pdg.dungeondelver.hamquestengine.descriptorproperties.playerspecific.PlayerStepTracker;
import com.pdg.dungeondelver.hamquestengine.gameconstants.ArmorTypes;
import com.pdg.dungeondelver.hamquestengine.gameconstants.ItemTypes;
import com.pdg.dungeondelver.hamquestengine.gameconstants.PlayerStates;
import com.pdg.dungeondelver.hamquestengine.gameconstants.Properties;
import com.pdg.dungeondelver.hamquestengine.gameconstants.PropertyGroups;
import com.pdg.dungeondelver.hamquestengine.gamecore.MessageQueue;
import com.pdg.dungeondelver.hamquestengine.item.Descriptor;
import com.pdg.dungeondelver.hamquestengine.maps.Creature;
import com.pdg.dungeondelver.pdgboardgames.CountedCollection;
import com.pdg.dungeondelver.pdgboardgames.WeightedGenerator;
import com.pdg.dungeondelver.hamquestengine.item.ShopState;
import com.pdg.dungeondelver.hamquestengine.maze.Maze;

public class PlayerDescriptor extends Descriptor
{
    public ShopState ShopState = null;
    public String PlayerState = PlayerStates.Play;
    public int getQuestItems()
    {
            return Items.get(this.<String>getProperty(Properties.QuestItemName));
    }
    public void setQuestItems(int value)
    {
        String theQuestItemName = this.<String>getProperty(Properties.QuestItemName);
        if (Items.get(this.<String>getProperty(Properties.QuestItemName)) > value)
        {
            Items.remove(theQuestItemName, Items.get(this.<String>getProperty(Properties.QuestItemName)) - value);
        }
        else if (Items.get(this.<String>getProperty(Properties.QuestItemName)) < value)
        {
            Items.add(theQuestItemName, value - Items.get(this.<String>getProperty(Properties.QuestItemName)));
        }
    }
    public int ExperiencePoints = 0;
    public int ExperienceGoal = 0;
    public int ExperienceLevel = 0;
    public Creature MapCreature = null;
    public MessageQueue getMessageQueue()
    {
            return Maze.getGame().getMessageQueue();
    }
    public String Weapon = "";
    public Map<String, String> Armors = new HashMap<String, String>();
    public Maze Maze = null;
    public int MazeColumn = 0;
    public int MazeRow = 0;
    public CountedCollection<Integer> Keys = new CountedCollection<Integer>();
    public CountedCollection<String> Items = new CountedCollection<String>();

    public String getLightSource()
    {
            int level = -1;
            String result = "";
            for (String itemIdentifier : Items.getIdentifiers())
            {
                Descriptor itemDescriptor = Maze.getGame().getTableSet().getItemTable().getItemDescriptor(itemIdentifier);
                if (itemDescriptor.<String>getProperty(Properties.ItemType).equalsIgnoreCase(ItemTypes.Light))
                {
                    if (itemDescriptor.<Integer>getProperty(Properties.LightLevel) > level)
                    {
                        level = itemDescriptor.<Integer>getProperty(Properties.LightLevel);
                        result = itemIdentifier;
                    }
                }
            }
            return (result);
    }
    public int getMoney()
    {

        return Items.get(Maze.getGame().getTableSet().getPropertyGroupTable().getPropertyDescriptor(PropertyGroups.PlayerConstants).<String>getProperty(Properties.CurrencyItemIdentifier));
    }
    public void setMoney(int value)
    {
        Items.remove(Maze.getGame().getTableSet().getPropertyGroupTable().getPropertyDescriptor(PropertyGroups.PlayerConstants).<String>getProperty(Properties.CurrencyItemIdentifier), getMoney());
        Items.add(Maze.getGame().getTableSet().getPropertyGroupTable().getPropertyDescriptor(PropertyGroups.PlayerConstants).<String>getProperty(Properties.CurrencyItemIdentifier), value);
    }
    private void messageQueueCallback(String message, Color color)
    {
        this.<PlayerMessageStatistics>getProperty(Properties.MessageStatistics).addEntry(message);
    }
    private void addItemDurability(String itemIdentifier)
    {
        Descriptor descriptor = Maze.getGame().getTableSet().getItemTable().getItemDescriptor(itemIdentifier);
        if ((descriptor.<String>getProperty(Properties.ItemType).equalsIgnoreCase(ItemTypes.Weapon)) || (descriptor.<String>getProperty(Properties.ItemType).equalsIgnoreCase(ItemTypes.Armor)))
        {
            CountedCollection<String> durabilities = this.<CountedCollection<String>>getProperty(Properties.ItemDurabilities);
            if (!durabilities.has(itemIdentifier))
            {
                durabilities.add(itemIdentifier, descriptor.<Integer>getProperty(Properties.Durability));
            }
            else
            {
                durabilities.put(itemIdentifier,durabilities.get(itemIdentifier)+descriptor.<Integer>getProperty(Properties.Durability));
            }
        }
    }
    private void autoEquipArmors()
    {
        boolean twoHandedWeapon = false;
        if (Weapon != "")
        {
            twoHandedWeapon = (Maze.getGame().getTableSet().getItemTable().getItemDescriptor(Weapon)).<Boolean>getProperty(Properties.TwoHanded);
        }
        for (String itemIdentifier : Items.getIdentifiers())
        {
            Descriptor newArmorDescriptor = Maze.getGame().getTableSet().getItemTable().getItemDescriptor(itemIdentifier);
            if (newArmorDescriptor.<String>getProperty(Properties.ItemType).equalsIgnoreCase(ItemTypes.Armor))
            {
                String armorType = newArmorDescriptor.<String>getProperty(Properties.ArmorType);
                if (!armorType.equalsIgnoreCase(ArmorTypes.Shield) || !twoHandedWeapon)
                {
                    if (Armors.get(armorType) == "")
                    {
                    	Armors.put(armorType, itemIdentifier);
                    }
                    else
                    {
                        Descriptor currentArmorDescriptor = Maze.getGame().getTableSet().getItemTable().getItemDescriptor(Armors.get(armorType));
                        String newGeneratorName = newArmorDescriptor.<String>getProperty(Properties.DefendGenerator);
                        String currentGeneratorName = currentArmorDescriptor.<String>getProperty(Properties.DefendGenerator);
                        WeightedGenerator<Integer> newGenerator = Maze.getGame().getTableSet().getPropertyGroupTable().getPropertyDescriptor(PropertyGroups.Generators).<WeightedGenerator<Integer>>getProperty(newGeneratorName);
                        WeightedGenerator<Integer> currentGenerator = Maze.getGame().getTableSet().getPropertyGroupTable().getPropertyDescriptor(PropertyGroups.Generators).<WeightedGenerator<Integer>>getProperty(currentGeneratorName);
                        if (newGenerator.getMaximalValue() > currentGenerator.getMaximalValue())
                        {
                            Armors.put(armorType,itemIdentifier);
                        }
                    }
                }
            }
        }
    }
    private void autoEquipWeapon()
    {
        Descriptor currentWeaponDescriptor = null;
        for (String itemIdentifier : Items.getIdentifiers())
        {
            Descriptor newWeaponDescriptor = Maze.getGame().getTableSet().getItemTable().getItemDescriptor(itemIdentifier);
            if (newWeaponDescriptor.<String>getProperty(Properties.ItemType).equalsIgnoreCase(ItemTypes.Weapon))
            {
                if (!newWeaponDescriptor.<Boolean>getProperty(Properties.TwoHanded) || Armors.get(ArmorTypes.Shield) == "")
                {
                    if (currentWeaponDescriptor == null)
                    {
                        Weapon = itemIdentifier;
                        currentWeaponDescriptor = newWeaponDescriptor;
                    }
                    else if (newWeaponDescriptor.<Integer>getProperty(Properties.AttackValue) > currentWeaponDescriptor.<Integer>getProperty(Properties.AttackValue))
                    {
                        Weapon = itemIdentifier;
                        currentWeaponDescriptor = newWeaponDescriptor;
                    }
                }
            }
        }
    }
    private void rollSpeed()
    {
        if (Maze == null) return;
        int speed = this.<WeightedGenerator<Integer>>getProperty(Properties.SpeedRoll).generate(Maze.getGame().getRandomNumberGenerator());
        float multiplier = 1.0f;
        for (String itemIdentifier : Armors.values())
        {
            if (itemIdentifier != "")
            {
                Descriptor armorDescriptor = Maze.getGame().getTableSet().getItemTable().getItemDescriptor(itemIdentifier);
                multiplier *= armorDescriptor.<Float>getProperty(Properties.SpeedMultiplier);
            }
        }
        speed = (int)((float)speed * multiplier);
        if (speed == 0) speed = 1;
        this.<PlayerStepTracker>getProperty(Properties.StepTracker).reset(speed);
    }
    private void decrementLightSource()
    {
        String lightSource = getLightSource();
        if (lightSource != "")
        {
            Descriptor itemDescriptor = Maze.getGame().getTableSet().getItemTable().getItemDescriptor(lightSource);
            if (itemDescriptor.<Integer>getProperty(Properties.Duration) == 0) return;

            CountedCollection<String> durabilities = this.<CountedCollection<String>>getProperty(Properties.ItemDurabilities);
            durabilities.put(lightSource,durabilities.get(lightSource)-1);
            if (durabilities.get(lightSource) % itemDescriptor.<Integer>getProperty(Properties.Duration) == 0)
            {
                getMessageQueue().addMessage(itemDescriptor.<String>getProperty(Properties.GoOutMessage));
                Items.remove(lightSource);
            }
        }
    }

    @Override
    public int getHealth()
    {
        return ExperienceLevel + this.<Integer>getProperty(Properties.InitialHealth);
    }
    protected int getAttack()
    {
        int result = 0;
        if (Weapon != "")
        {
            Descriptor weaponDescriptor = Maze.getGame().getTableSet().getItemTable().getItemDescriptor(Weapon);
            result += weaponDescriptor.<Integer>getProperty(Properties.AttackValue);
        }
        return (result);
    }
    protected int getSpeed()
    {
        return this.<PlayerStepTracker>getProperty(Properties.StepTracker).getTotal();
    }

    public void addExperience(int experiencePoints)
    {
        if (experiencePoints == 1)
        {
            MessageQueue.AddMessage(string.Format(GetProperty<string>(GameConstants.Properties.GainExperiencePointMessage), experiencePoints) , experiencePoints);
        }
        else
        {
            MessageQueue.AddMessage(string.Format(GetProperty<string>(GameConstants.Properties.GainExperiencePointsMessage), experiencePoints) , experiencePoints);
        }
        ExperiencePoints += experiencePoints;
        if (ExperiencePoints >= ExperienceGoal)
        {
            MessageQueue.AddMessage(GetProperty<string>(GameConstants.Properties.GainExperienceLevelMessage));
            ExperiencePoints -= ExperienceGoal;
            ExperienceGoal *= GetProperty<int>(GameConstants.Properties.ExeperienceGoalMultiplier);
            ExperienceGoal /= GetProperty<int>(GameConstants.Properties.ExeperienceGoalDivisor);
            MapCreature.Wounds = 0;
            ExperienceLevel++;
        }
    }
    public void takeDamage(int damage, int defendRoll)
    {
        MapCreature.Wounds += damage;
        MapCreature.BeenHit = true;
        CheckForArmorBreakage(defendRoll);
        while (MapCreature.Dead)
        {
            bool found = false;
            foreach (string identifier in Items.Identifiers)
            {
                Descriptor descriptor = Maze.Game.TableSet.ItemTable.GetItemDescriptor(identifier);
                if (descriptor.GetProperty<string>(GameConstants.Properties.ItemType) == GameConstants.ItemTypes.Healing && descriptor.GetProperty<string>(GameConstants.Properties.HealingType) == GameConstants.HealingTypes.Save)
                {
                    MessageQueue.AddMessage(descriptor.GetProperty<string>(GameConstants.Properties.UseMessage));
                    MapCreature.Wounds -= descriptor.GetProperty<int>(GameConstants.Properties.HealingAmount);
                    Items.Remove(identifier);
                    found = true;
                    break;
                }
            }
            if (!found)
            {
                PlayerState = HamQuestEngine.GameConstants.PlayerStates.Lose;
                MessageQueue.AddMessage(GetProperty<string>(GameConstants.Properties.KilledMessage));
                return;
            }
        }
    }
    public void eatIfNeeded()
    {
        while (MapCreature.Wounds > 0)
        {
            bool found = false;
            foreach (string identifier in Items.Identifiers)
            {
                Descriptor descriptor = Maze.Game.TableSet.ItemTable.GetItemDescriptor(identifier);
                if (descriptor.GetProperty<string>(GameConstants.Properties.ItemType) == GameConstants.ItemTypes.Healing && descriptor.GetProperty<string>(GameConstants.Properties.HealingType) == GameConstants.HealingTypes.Eaten)
                {
                    MessageQueue.AddMessage(descriptor.GetProperty<string>(GameConstants.Properties.UseMessage));
                    MapCreature.Wounds -= descriptor.GetProperty<int>(GameConstants.Properties.HealingAmount);
                    Items.Remove(identifier);
                    found = true;
                    break;
                }
            }
            if (!found) return;
        }
    }
    public void updatePathfinding()
    {
        Map theMap = MapCreature.Map;
        int column;
        int row;
        int direction;
        int nextColumn;
        int nextRow;
        Queue<int> columns = new Queue<int>();
        Queue<int> rows = new Queue<int>();
        Directions directions = new Directions();
        int lowestValue;
        for (column = 0; column < theMap.Columns; ++column)
        {
            for (row = 0; row < theMap.Rows; ++row)
            {
                theMap[column][row].PathfindingValue = int.MaxValue;
            }
        }
        column = MapCreature.Column;
        row = MapCreature.Row;
        theMap[column][row].PathfindingValue = 0;
        for (direction = 0; direction < directions.Count; ++direction)
        {
            nextColumn = directions.GetNextColumn(column, row, direction);
            nextRow = directions.GetNextRow(column, row, direction);
            if (nextColumn < 0) continue;
            if (nextRow < 0) continue;
            if (nextColumn >= theMap.Columns) continue;
            if (nextRow >= theMap.Rows) continue;
            if (theMap[nextColumn][nextRow].PathfindingValue != int.MaxValue) continue;
            if (theMap[nextColumn][nextRow].ItemIdentifier != String.Empty) continue;
            if (!Maze.Game.TableSet.TerrainTable.GetTerrainDescriptor(theMap[nextColumn][nextRow].TerrainIdentifier).GetProperty<bool>(GameConstants.Properties.Passable)) continue;
            columns.Enqueue(nextColumn);
            rows.Enqueue(nextRow);
        }
        while (columns.Count > 0)
        {
            column = columns.Dequeue();
            row = rows.Dequeue();
            if (theMap[column][row].PathfindingValue != int.MaxValue) continue;
            lowestValue = int.MaxValue;
            for (direction = 0; direction < directions.Count; ++direction)
            {
                nextColumn = directions.GetNextColumn(column, row, direction);
                nextRow = directions.GetNextRow(column, row, direction);
                if (nextColumn < 0) continue;
                if (nextRow < 0) continue;
                if (nextColumn >= theMap.Columns) continue;
                if (nextRow >= theMap.Rows) continue;
                if (theMap[nextColumn][nextRow].PathfindingValue < lowestValue)
                {
                    lowestValue = theMap[nextColumn][nextRow].PathfindingValue;
                }
            }
            theMap[column][row].PathfindingValue = lowestValue + 1;
            for (direction = 0; direction < directions.Count; ++direction)
            {
                nextColumn = directions.GetNextColumn(column, row, direction);
                nextRow = directions.GetNextRow(column, row, direction);
                if (nextColumn < 0) continue;
                if (nextRow < 0) continue;
                if (nextColumn >= theMap.Columns) continue;
                if (nextRow >= theMap.Rows) continue;
                if (theMap[nextColumn][nextRow].PathfindingValue != int.MaxValue) continue;
                if (theMap[nextColumn][nextRow].ItemIdentifier != String.Empty) continue;
                if (!Maze.Game.TableSet.TerrainTable.GetTerrainDescriptor(theMap[nextColumn][nextRow].TerrainIdentifier).GetProperty<bool>(GameConstants.Properties.Passable)) continue;
                columns.Enqueue(nextColumn);
                rows.Enqueue(nextRow);
            }
        }
    }
    public void revealTraps()
    {
        Directions directions = new Directions();
        for (int direction = 0; direction < directions.Count; ++direction)
        {
            MapCreature.Map.RevealHiddens(directions.GetNextColumn(MapCreature.Column, MapCreature.Row, direction), directions.GetNextRow(MapCreature.Column, MapCreature.Row, direction));
        }
    }
    public boolean isEquipped(String itemIdentifier)
    {
        if (itemIdentifier == String.Empty) return (false);
        if (itemIdentifier == Weapon) return (true);
        if (itemIdentifier == LightSource) return (true);
        if (Armors.ContainsValue(itemIdentifier)) return (true);
        return (false);
    }
    public void checkForWeaponBreakage(int damage)
    {
        if (Weapon != String.Empty)
        {
            Descriptor weaponDescriptor = (Maze.Game.TableSet.ItemTable.GetItemDescriptor(Weapon));
            CountedCollection<string> durabilities = GetProperty<CountedCollection<string>>(GameConstants.Properties.ItemDurabilities);
            if (damage > durabilities[Weapon])
            {
                durabilities[Weapon] = 0;
            }
            else
            {
                durabilities[Weapon] -= (uint)damage;
            }
            if (((durabilities[Weapon] + weaponDescriptor.GetProperty<int>(GameConstants.Properties.Durability) - 1) / weaponDescriptor.GetProperty<int>(GameConstants.Properties.Durability)) < Items[Weapon])
            {
                MessageQueue.AddMessage(weaponDescriptor.GetProperty<string>(GameConstants.Properties.BrokenMessage));
                Items.Remove(Weapon);
                AutoEquip();
            }
        }
    }
    public void checkForArmorBreakage(int damage)
    {
        if (damage <= 0) return;
        List<string> brokenList = new List<string>();
        List<string> wornList = new List<string>();
        foreach (string itemIdentifier in Armors.Values)
        {
            if (itemIdentifier != String.Empty)
            {
                if (Maze.Game.TableSet.ItemTable.GetItemDescriptor(itemIdentifier).HasTag(GameConstants.Tags.Durable))
                {
                    wornList.Add(itemIdentifier);
                }
            }
        }
        while ((damage > 0) && (wornList.Count > 0))
        {
            string itemIdentifier = wornList[Maze.Game.RandomNumberGenerator.Next(wornList.Count)];
            CountedCollection<string> durabilities = GetProperty<CountedCollection<string>>(GameConstants.Properties.ItemDurabilities);
            if (durabilities[itemIdentifier] > 0)
            {
                durabilities[itemIdentifier]--;
                damage--;
            }
            Descriptor armorDescriptor = Maze.Game.TableSet.ItemTable.GetItemDescriptor(itemIdentifier);
            if (((durabilities[itemIdentifier] + armorDescriptor.GetProperty<int>(GameConstants.Properties.Durability) - 1) / armorDescriptor.GetProperty<int>(GameConstants.Properties.Durability)) < Items[itemIdentifier])
            {
                MessageQueue.AddMessage(armorDescriptor.GetProperty<string>(GameConstants.Properties.BrokenMessage));
                brokenList.Add(itemIdentifier);
                wornList.Remove(itemIdentifier);
            }
        }
        if (brokenList.Count > 0)
        {
            foreach (string itemIdentifier in brokenList)
            {
                Items.Remove(itemIdentifier);
            }
            AutoEquip();
        }
    }
    public void autoEquip()
    {
        Weapon = String.Empty;
        foreach(string armorType in Maze.Game.TableSet.PropertyGroupTable.GetPropertyDescriptor(GameConstants.PropertyGroups.PlayerConstants).GetProperty<HashSet<string>>(GameConstants.Properties.ArmorTypes))
        {
            Armors[armorType] = String.Empty;
        }
        if (GetProperty<PlayerConfiguration>(GameConstants.Properties.PlayerConfiguration).Current == HamQuestEngine.GameConstants.PlayerConfigurationValues.Attack)
        {
            AutoEquipWeapon();
            AutoEquipArmors();
        }
        else if (GetProperty<PlayerConfiguration>(GameConstants.Properties.PlayerConfiguration).Current == HamQuestEngine.GameConstants.PlayerConfigurationValues.Defend)
        {
            AutoEquipArmors();
            AutoEquipWeapon();
        }
    }
    public void teleport()
    {
        Directions directions = new Directions();
        int count = 0;
        int mazeWidth = Maze.Game.TableSet.PropertyGroupTable.GetPropertyDescriptor(GameConstants.PropertyGroups.MazeDimensions).GetProperty<int>(GameConstants.Properties.Columns);
        int mazeHeight = Maze.Game.TableSet.PropertyGroupTable.GetPropertyDescriptor(GameConstants.PropertyGroups.MazeDimensions).GetProperty<int>(GameConstants.Properties.Rows);
        do
        {
            MazeColumn = Maze.Game.RandomNumberGenerator.Next(mazeWidth);
            MazeRow = Maze.Game.RandomNumberGenerator.Next(mazeHeight);
            count = 0;
            for (int direction = 0; direction < directions.Count; ++direction)
            {
                if (Maze[MazeColumn][MazeRow].Portals[direction] != null && Maze[MazeColumn][MazeRow].Portals[direction].Open)
                {
                    count++;
                }
            }
        } while (count == 1);
        Map.PlayerCreature = null;
        MapCreature.Map = Maze[MazeColumn][MazeRow].CellInfo.Map;
        bool done = false;
        while (!done)
        {
            MapCreature.Column = Maze.Game.RandomNumberGenerator.Next(MapCreature.Map.Columns);
            MapCreature.Row = Maze.Game.RandomNumberGenerator.Next(MapCreature.Map.Rows);
            done = true;
            if (!Maze.Game.TableSet.TerrainTable.GetTerrainDescriptor(MapCreature.Map[MapCreature.Column][MapCreature.Row].TerrainIdentifier).GetProperty<bool>("passable"))
            {
                done = false;
                continue;
            }
            if (MapCreature.Map[MapCreature.Column][MapCreature.Row].ItemIdentifier != String.Empty)
            {
                done = false;
                continue;
            }
            if (MapCreature.Map[MapCreature.Column][MapCreature.Row].Creature != null)
            {
                done = false;
                continue;
            }
        }
        Map.PlayerCreature = MapCreature;
        Maze[MazeColumn][MazeRow].CellInfo.AddVisit();
    }
    public void addItem(String itemIdentifier)
    {
        addItem(itemIdentifier, true);
    }
    public boolean handleKey(Key key)
    {
        HashSet<string> keyHandlerNames = GetProperty<HashSet<string>>(GameConstants.Properties.KeyHandlers);
        foreach (string keyHandlerName in keyHandlerNames)
        {
            IPlayerKeyHandler keyHandler = GetProperty<IPlayerKeyHandler>(keyHandlerName);
            if (keyHandler.HandleKey(key, this))
            {
                return true;
            }
        }
        return false;
    }
    public void addItem(String itemIdentifier, boolean reportToMessageQueue)
    {
        Descriptor itemDescriptor = Maze.Game.TableSet.ItemTable.GetItemDescriptor(itemIdentifier);
        string theItemType = itemDescriptor.GetProperty<string>(GameConstants.Properties.ItemType);
        if (theItemType == GameConstants.ItemTypes.Portal)
        {
            if (reportToMessageQueue) MessageQueue.AddMessage(itemDescriptor.GetProperty<string>(GameConstants.Properties.PickUpMessage));
            MapCreature.Remove();
            Teleport();
            MapCreature.Place();
        }
        else
            if (theItemType == GameConstants.ItemTypes.Treasure)
            {
                if (reportToMessageQueue) MessageQueue.AddMessage(itemDescriptor.GetProperty<string>(GameConstants.Properties.PickUpMessage));
                setMoney(getMoney()+(Maze.Game.RandomNumberGenerator.Next(itemDescriptor.GetProperty<int>(GameConstants.Properties.MaximumValue) - itemDescriptor.GetProperty<int>(GameConstants.Properties.MinimumValue) + 1) + itemDescriptor.GetProperty<int>(GameConstants.Properties.MinimumValue));
            }
            else
                if (theItemType == GameConstants.ItemTypes.Quest)
                {
                    if (reportToMessageQueue) MessageQueue.AddMessage(itemDescriptor.GetProperty<string>(GameConstants.Properties.PickUpMessage));
                    Maze.SpawnMonsters(1.0f / (float)(Maze.Game.TableSet.ItemTable.QuestItemCount - QuestItems));
                    QuestItems++;
                    if (QuestItems == Maze.Game.TableSet.ItemTable.QuestItemCount)
                    {
                        MessageQueue.AddMessage(GetProperty<string>(GameConstants.Properties.QuestCompletedMessage));
                    }
                }
                else
                    if (theItemType == GameConstants.ItemTypes.Chest)
                    {
                        if (reportToMessageQueue) MessageQueue.AddMessage(itemDescriptor.GetProperty<string>(GameConstants.Properties.PickUpMessage));
                    }
                    else
                        if (theItemType == GameConstants.ItemTypes.Armor)
                        {
                            if (reportToMessageQueue) MessageQueue.AddMessage(itemDescriptor.GetProperty<string>(GameConstants.Properties.PickUpMessage));
                            AddItemDurability(itemIdentifier);
                            Items.Add(itemIdentifier);
                            AutoEquip();
                        }
                        else
                            if (theItemType == GameConstants.ItemTypes.Weapon)
                            {
                                if (reportToMessageQueue) MessageQueue.AddMessage(itemDescriptor.GetProperty<string>(GameConstants.Properties.PickUpMessage));
                                Items.Add(itemIdentifier);
                                AddItemDurability(itemIdentifier);
                                AutoEquip();
                            }
                            else
                                if (theItemType == GameConstants.ItemTypes.Trap)
                                {
                                    CountedCollection<string> disarms = itemDescriptor.GetProperty<CountedCollection<string>>(GameConstants.Properties.ItemDisarms);
                                    bool canDisarm = true;
                                    foreach (string disarmIdentifier in disarms.Identifiers)
                                    {
                                        if (disarms[disarmIdentifier] > Items[disarmIdentifier])
                                        {
                                            canDisarm = false;
                                            break;
                                        }
                                    }
                                    if (canDisarm)
                                    {
                                        foreach (string disarmIdentifier in disarms.Identifiers)
                                        {
                                            Items.Remove(disarmIdentifier, disarms[disarmIdentifier]);
                                        }
                                        if (reportToMessageQueue) MessageQueue.AddMessage(itemDescriptor.GetProperty<string>(GameConstants.Properties.DisarmMessage));
                                    }
                                    else
                                    {
                                        if (reportToMessageQueue) MessageQueue.AddMessage(itemDescriptor.GetProperty<string>(GameConstants.Properties.PickUpMessage));
                                        TakeDamage(itemDescriptor.GetProperty<int>(GameConstants.Properties.AttackValue), 0);
                                        Map.PlayerCreature.BeenHit = true;
                                        if (MapCreature.Dead)
                                        {
                                            if (reportToMessageQueue) MessageQueue.AddMessage(itemDescriptor.GetProperty<string>(GameConstants.Properties.KilledByMessage));
                                        }
                                    }
                                }
                                else
                                    if (theItemType == GameConstants.ItemTypes.Light)
                                    {
                                        if (reportToMessageQueue) MessageQueue.AddMessage(itemDescriptor.GetProperty<string>(GameConstants.Properties.PickUpMessage));
                                        Items.Add(itemIdentifier);
                                        Descriptor lightItemDescriptor = Maze.Game.TableSet.ItemTable.GetItemDescriptor(itemIdentifier);
                                        CountedCollection<string> durabilities = GetProperty<CountedCollection<string>>(GameConstants.Properties.ItemDurabilities);
                                        if (durabilities.Has(itemIdentifier))
                                        {
                                            durabilities[itemIdentifier] += (uint)lightItemDescriptor.GetProperty<int>(GameConstants.Properties.Duration);
                                        }
                                        else
                                        {
                                            durabilities.Add(itemIdentifier, (uint)lightItemDescriptor.GetProperty<int>(GameConstants.Properties.Duration));
                                        }
                                    }
                                    else
                                    {
                                        if (reportToMessageQueue) MessageQueue.AddMessage(itemDescriptor.GetProperty<string>(GameConstants.Properties.PickUpMessage));
                                        Items.Add(itemIdentifier);
                                    }
    }
    
    private void handleEndOfTurn()
    {
        decrementLightSource();
        rollSpeed();
    }
    public void step()
    {
        GetProperty<PlayerStepTracker>(GameConstants.Properties.StepTracker).DoStep();
    }

    public static Descriptor LoadCustomDescriptor(PropertyValuePair[] thePropertyValues)
    {
        return new PlayerDescriptor(thePropertyValues);
    }
    public PlayerDescriptor(PropertyValuePair[] thePropertyValues)
    {
    	super(thePropertyValues);
        GetProperty<PlayerStatisticHolder>(GameConstants.Properties.Attack).GetValue = this.getAttack;
        GetProperty<PlayerStatisticHolder>(GameConstants.Properties.Health).GetValue = this.getHealth;
        GetProperty<PlayerStatisticHolder>(GameConstants.Properties.Speed).GetValue = this.getSpeed;
        GetProperty<PlayerStepTracker>(GameConstants.Properties.StepTracker).OnEndOfTurn = HandleEndOfTurn;
        GetProperty<PlayerConfiguration>(GameConstants.Properties.PlayerConfiguration).OnChange = AutoEquip;
        rollSpeed();
    }

    public void Initialize()
    {
        ExperienceLevel = this.<Integer>getProperty(Properties.InitialExperienceLevel);
        ExperiencePoints = this.<Integer>getProperty(Properties.InitialExperiencePoints);
        ExperienceGoal = this.<Integer>getProperty(Properties.InitialExperienceGoal);
        MessageQueue.CallBacks -= messageQueueCallback;
        MessageQueue.CallBacks += messageQueueCallback;
        MessageQueue.AddMessage(GameConstants.Messages.Welcome1);
        MessageQueue.AddMessage(GameConstants.Messages.Welcome2);
        MessageQueue.AddMessage(GameConstants.Messages.Welcome3);
        MessageQueue.AddMessage(GameConstants.Messages.Welcome4);
        MessageQueue.AddMessage(GameConstants.Messages.Welcome5);
    }
}

