package com.pdg.dungeondelver.hamquestengine.maps;

import com.pdg.dungeondelver.hamquestengine.creatures.PlayerDescriptor;
import com.pdg.dungeondelver.hamquestengine.descriptorproperties.misc.IStatisticHolder;
import com.pdg.dungeondelver.hamquestengine.descriptorproperties.movers.DoMoveResult;
import com.pdg.dungeondelver.hamquestengine.descriptorproperties.movers.IMover;
import com.pdg.dungeondelver.hamquestengine.descriptorproperties.rollers.IRoller;
import com.pdg.dungeondelver.hamquestengine.directions.Directions;
import com.pdg.dungeondelver.hamquestengine.gameconstants.Properties;
import com.pdg.dungeondelver.hamquestengine.gamecore.Game;
import com.pdg.dungeondelver.hamquestengine.gamecore.GameClientBase;

public class Creature extends GameClientBase
{
    private String creatureIdentifier;
    public boolean Summoned = false;
    public boolean BeenHit = false;
    private int wounds = 0;
    public int getWounds()
    {
            return wounds;
    }
    public void setWounds(int value)
        {
            wounds = (value < 0) ? 0 : value;
    }
    public int Column = 0;
    public int Row = 0;
    public Map Map = null;
    public float Steps = 0.0f;
    public String getCreatureIdentifier()
    {
            return (creatureIdentifier);
    }
    public boolean isDead()
    {
            return (getWounds() >= getGame().getTableSet().getCreatureTable().getCreatureDescriptor(getCreatureIdentifier()).getHealth());
    }
    public void remove()
    {
        if (this.Column >= 0 && this.Row >= 0 && this.Column < Map.getColumns() && this.Row < Map.getRows())
        {
            Map.get(this.Column).get(this.Row).Creature=null;
        }
    }
    public void place()
    {
        if (this.Column >= 0 && this.Row >= 0 && this.Column < Map.getColumns() && this.Row < Map.getRows())
        {
            Map.get(this.Column).get(this.Row).Creature=this;
        }
    }
    public void move(int direction)
    {
        remove();
        Directions directions = new Directions();
        int nextColumn = directions.getNextColumn(this.Column, this.Row, direction);
        int nextRow = directions.getNextRow(this.Column, this.Row, direction);
        IMover mover = getGame().getTableSet().getCreatureTable().getCreatureDescriptor(this.getCreatureIdentifier()).<IMover>getProperty("mover");
        DoMoveResult result = mover.doMove(this, nextColumn, nextRow, getGame());
        this.Column = result.getColumn();
        this.Row = result.getRow();
        place();
        PlayerDescriptor playerDescriptor = (PlayerDescriptor)getGame().getTableSet().getCreatureTable().getCreatureDescriptor(this.getCreatureIdentifier());
        if (playerDescriptor != null)
        {
            playerDescriptor.updatePathfinding();
            playerDescriptor.revealTraps();
        }
    }
    public int rollAttack()
    {
        int result = 0;
        if (getGame().getTableSet().getCreatureTable().getCreatureDescriptor(getCreatureIdentifier()).<IStatisticHolder>getProperty(Properties.Attack).getValue() > 0)
        {
            for (int index = 0; index < getGame().getTableSet().getCreatureTable().getCreatureDescriptor(getCreatureIdentifier()).<IStatisticHolder>getProperty(Properties.Attack).getValue(); ++index)
            {
                if (getGame().getRandomNumberGenerator().next(6) < getGame().getTableSet().getCreatureTable().getCreatureDescriptor(getCreatureIdentifier()).<Integer>getProperty(Properties.AttackDie))
                {
                    result++;
                }
            }
        }
        else
        {
            //unarmed combat
            if (getGame().getRandomNumberGenerator().next(6) == 0) result++;
        }
        return (result);
    }
    public int rollDefend()
    {
        return getGame().getTableSet().getCreatureTable().getCreatureDescriptor(getCreatureIdentifier()).<IRoller>getProperty(Properties.DefendRoller).roll(getGame().getTableSet().getCreatureTable().getCreatureDescriptor(getCreatureIdentifier()), getGame());
    }
    public Creature(String theCreatureIdentifier, int theColumn, int theRow,Game theGame)
    {
    	super(theGame);
        creatureIdentifier = theCreatureIdentifier;
        Column = theColumn;
        Row = theRow;
    }
}

