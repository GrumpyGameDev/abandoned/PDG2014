package com.pdg.dungeondelver.hamquestengine.maze;

import com.pdg.dungeondelver.hamquestengine.directions.Directions;
import com.pdg.dungeondelver.pdgboardgames.BoardCellFactory;
import com.pdg.dungeondelver.pdgboardgames.MazeCellBase;

public class MazeCellFactory implements
		BoardCellFactory<MazeCellBase<Directions, Portal, CellInfo>> {

	@Override
	public MazeCellBase<Directions, Portal, CellInfo> newInstance() {
		return new MazeCellBase<Directions,Portal,CellInfo>();
	}

}
