package com.pdg.dungeondelver.hamquestengine.descriptorproperties.misc;

import java.util.HashSet;
import java.util.Set;

import com.badlogic.gdx.utils.XmlReader.Element;
import com.pdg.dungeondelver.hamquestengine.item.CustomPropertyLoader;

public class TagSetLoader  implements CustomPropertyLoader
{
    public Object loadFromNode(Element node)
    {
        Set<String> result = new HashSet<String>();
        for(int index=0;index<node.getChildCount();++index){
        	Element subElement = node.getChild(index);
        	if(subElement.getName().compareToIgnoreCase("tag")==0){
        		result.add(subElement.getText());
        	}
        }
        return result;
    }
}