package com.playdeezgames.splorr;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.Stack;
import com.playdeezgames.splorr.enums.FloraFeatures;
import com.playdeezgames.splorr.tokenprocessors.ITokenProcessor;
import com.playdeezgames.splorr.tokenprocessors.MainTokenProcessor;
import com.playdeezgames.splorr.world.Avatar;

public class Splorr {
	public static void main(String[] args) {
		Avatar avatar = new Avatar();
		ITokenProcessor mainTokenProcessor = new MainTokenProcessor();
		
		System.out.println();
		System.out.println("Splorr!!");
		
		Scanner scanner = new Scanner(System.in);
		while(!avatar.isQuit()){
			
			System.out.println(avatar.getTimeOfDay().getDescription());
			System.out.println(avatar.getCurrentCell().getBiome().getDescription());
			for(FloraFeatures feature : avatar.getCurrentCell().getFloraFeatures()){
				System.out.println(feature.getDescription());
			}
			if(!avatar.getCurrentCell().getItems().isEmpty()){
				System.out.println("There are items on the ground.");
			}
			System.out.println();
			System.out.print(">");
			String line = scanner.nextLine();
			
			Stack<String> tokenizer = new Stack<String>();
			List<String> tokens = Arrays.asList(line.split("\\s"));
			Collections.reverse(tokens);
			tokenizer.addAll(tokens);
			
			if(!mainTokenProcessor.processTokens(tokenizer, avatar)){
				System.out.println();
				System.out.printf("I don't know what you mean by '%s'.\n",new Object[]{line});
			}
		}
		scanner.close();
		
	}
}
