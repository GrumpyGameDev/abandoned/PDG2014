package com.playdeezgames.precedence;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.playdeezgames.common.managers.ManagerBase;
import com.playdeezgames.common.managers.TextureRegionManager;

public class ResourceManager extends ManagerBase {
	private static final float STAGE_WIDTH = 720;
	private static final float STAGE_HEIGHT = 1280;
	private TextureRegionManager<PrecedenceTextureRegions,PrecedenceTextures,PrecedenceFiles> textureRegionManager;
	private Stage stage;
	public Stage getStage(){
		if(stage==null){
			stage = new Stage();
			stage.setViewport(STAGE_WIDTH, STAGE_HEIGHT, true,0,0,Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
		}
		return stage;
	}
	public TextureRegionManager<PrecedenceTextureRegions,PrecedenceTextures,PrecedenceFiles> getTextureRegionManager(){
		if(textureRegionManager==null){
			textureRegionManager=new TextureRegionManager<PrecedenceTextureRegions,PrecedenceTextures,PrecedenceFiles>();
		}
		return textureRegionManager;
	}
	@Override
	public void dispose() {
		getStage().dispose();
		getTextureRegionManager().dispose();
	}

}
