package com.pdg.dungeondelver.pdgboardgames;

import java.util.HashMap;
import java.util.Map;

public class WeightedGenerator<GeneratedType extends Comparable<GeneratedType>>
{
    private Map<GeneratedType, Integer> generationTable = new HashMap<GeneratedType, Integer>();
    private Integer weightTotal=0;
    public void clear()
    {
        generationTable.clear();
    }
    public GeneratedType getMinimalValue()
    {
        GeneratedType result = null;
        for(GeneratedType key : generationTable.keySet()){
        	if(result==null || result.compareTo(key)>0){
        		result = key;
        	}
        }
        return result;
    }
    public GeneratedType getMaximalValue()
    {
        GeneratedType result = null;
        for(GeneratedType key : generationTable.keySet()){
        	if(result==null || result.compareTo(key)<0){
        		result = key;
        	}
        }
        return result;
    }
    private void setWeight(GeneratedType theValue, Integer theWeight)
    {
        if (generationTable.containsKey(theValue))
        {
            weightTotal -= generationTable.get(theValue);
            generationTable.put(theValue,theWeight);
        }
        else
        {
            generationTable.put(theValue,theWeight);
        }
        weightTotal += generationTable.get(theValue);
    }
    private Integer getWeight(GeneratedType theValue)
    {
        if (generationTable.containsKey(theValue))
        {
            return generationTable.get(theValue);
        }
        else
        {
        	return 0;
        }
    }
    public GeneratedType generate(IRandomNumberGenerator theRandomNumberGenerator)
    {
        if (weightTotal > 0){
            Integer randomValue = theRandomNumberGenerator.next(weightTotal);
            for (GeneratedType generatedValue : generationTable.keySet())
            {
                if (randomValue < generationTable.get(generatedValue))
                {
                    return (generatedValue);
                }
                else
                {
                    randomValue -= generationTable.get(generatedValue);
                }
            }
        }
        return null;
    }
    public Integer get(GeneratedType theValue)
    {
        return getWeight(theValue);
    }
    public void put(GeneratedType theValue, Integer value){
        setWeight(theValue, value);
    }
    public WeightedGenerator()
    {
    }
}
