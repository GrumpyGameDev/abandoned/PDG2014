package com.pdg.dungeondelver.hamquestengine.item;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.XmlReader;
import com.badlogic.gdx.utils.XmlReader.Element;
import com.pdg.dungeondelver.hamquestengine.gameconstants.Attributes;
import com.pdg.dungeondelver.hamquestengine.gameconstants.Properties;

public class PropertyValuePair
{
    private String property;
    private Object value;
    public String getProperty()
    {
        return property;
    }
    public Object getValue()
    {
        return value;
    }
    public PropertyValuePair(String theProperty, Object theValue)
    {
        property = theProperty;
        value = theValue;
    }
    private static Map<String,CustomPropertyLoader> registeredTypes = new HashMap<String,CustomPropertyLoader>();
    public static void RegisterType(String typeName,CustomPropertyLoader loader)
    {
        UnregisterType(typeName);
        registeredTypes.put(typeName, loader);
    }
    public static void UnregisterType(String typeName)
    {
        registeredTypes.remove(typeName);
    }
    private static final String Delimiter = ",";
    public static IdentifiedPropertyValuePairList loadPropertyValuesFromXmlNode(Element node)
    {
        IdentifiedPropertyValuePairList properties = new IdentifiedPropertyValuePairList();
        if (node.getAttribute(Attributes.Template) != null)
        {
            String[] filenames = node.getAttribute(Attributes.Template).split(Delimiter);
            for(String fileName : filenames)
            {
            	XmlReader reader = new XmlReader();
            	FileHandle handle = Gdx.files.internal(fileName);
				try {
	            	Element doc;
					doc = reader.parse(handle);
	            	IdentifiedPropertyValuePairList data = loadPropertyValuesFromXmlNode(doc.getChildByName(node.getName()));
	            	properties.identifier+=data.identifier;
	            	properties.result.addAll(data.result);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            }
        }
        for (int index=0;index<node.getChildCount();++index)
        {
        	Element subElement=node.getChild(index);
            if (subElement.getName() == Properties.Identifier)
            {
                properties.identifier += subElement.getText();
            }
            else if (subElement.getAttribute(Attributes.Type) != null)
            {
                String name = subElement.getName();
                String typeName = subElement.getAttribute(Attributes.Type);
                
                if(registeredTypes.containsKey(typeName))
                {
	                properties.result.add(new PropertyValuePair(name,registeredTypes.get(typeName).loadFromNode(subElement)));
                }
                else
                {
	                Class<?> instantiator;
					try {
						instantiator = Class.forName(typeName);
		                CustomPropertyLoader loader = (CustomPropertyLoader)instantiator.newInstance();
		                properties.result.add(new PropertyValuePair(name,loader.loadFromNode(subElement)));
					} catch (ClassNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (InstantiationException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IllegalAccessException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
                }
            }
            else
            {
                String name = subElement.getName();
                String value = subElement.getText();
                
                try{
	                int intValue = Integer.parseInt(value);
	                properties.result.add(new PropertyValuePair(name, intValue));
                }
                catch(NumberFormatException e){
                	try{
                        float floatValue = Float.parseFloat(value);
                        properties.result.add(new PropertyValuePair(name, floatValue));
                	}catch(NumberFormatException e1){
                		if(value.compareToIgnoreCase("true")==0 || value.compareToIgnoreCase("false")==0){
                            boolean boolValue = Boolean.parseBoolean(value);
                            properties.result.add(new PropertyValuePair(name, boolValue));
                		}else{
                            properties.result.add(new PropertyValuePair(name, value));
                		}
                	}
                }
            }
        }
        return properties;
    }
    public static List<PropertyValuePair> combineArrays(List<PropertyValuePair> first, List<PropertyValuePair> second)
    {
        List<PropertyValuePair> temp = new ArrayList<PropertyValuePair>(first);
        temp.addAll(second);
        return temp;
    }
}
