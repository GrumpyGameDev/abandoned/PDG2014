package com.playdeezgames.ctyr.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.playdeezgames.ctyr.ClickTheYellowRhombus;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width=512;
		config.height=512;
		config.resizable=false;
		new LwjglApplication(new ClickTheYellowRhombus(), config);
	}
}
