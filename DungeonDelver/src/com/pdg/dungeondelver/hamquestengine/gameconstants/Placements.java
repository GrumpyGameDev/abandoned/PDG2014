package com.pdg.dungeondelver.hamquestengine.gameconstants;

public class Placements {
    public static final String DeadEnd = "DeadEnd";
    public static final String Either = "Either";
    public static final String NonDeadEnd = "NonDeadEnd";
    public static final String None = "None";
}
