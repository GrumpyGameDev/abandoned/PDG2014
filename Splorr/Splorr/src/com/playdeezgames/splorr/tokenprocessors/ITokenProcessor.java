package com.playdeezgames.splorr.tokenprocessors;

import java.util.Stack;
import com.playdeezgames.splorr.world.Avatar;

public interface ITokenProcessor {
	boolean processTokens(Stack<String> tokenizer, Avatar avatar);
}
