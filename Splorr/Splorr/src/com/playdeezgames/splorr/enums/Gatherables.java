package com.playdeezgames.splorr.enums;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.playdeezgames.splorr.generators.IGenerator;
import com.playdeezgames.splorr.generators.WeightedGenerator;
import com.playdeezgames.splorr.items.IItem;
import com.playdeezgames.splorr.items.SimpleItem;

public enum Gatherables {
	MOSS("moss", new ArrayList<FloraFeatures>(Arrays.asList(FloraFeatures.MOSS)), new WeightedGenerator<Boolean>().put(true, 1).put(false, 1), Items.MOSS),
	GRASS("grass", new ArrayList<FloraFeatures>(Arrays.asList(FloraFeatures.GRASS)), new WeightedGenerator<Boolean>().put(true, 1).put(false, 1), Items.GRASS),
	FLOWERS("flowers", new ArrayList<FloraFeatures>(Arrays.asList(FloraFeatures.FLOWERS)), new WeightedGenerator<Boolean>().put(true, 1).put(false, 1), Items.FLOWERS),
	BERRIES("berries", new ArrayList<FloraFeatures>(Arrays.asList(FloraFeatures.BERRY_BUSHES)), new WeightedGenerator<Boolean>().put(true, 1).put(false, 1), Items.BERRIES),
	LEAVES("leaves", new ArrayList<FloraFeatures>(Arrays.asList(FloraFeatures.DECIDUOUS_TREES, FloraFeatures.SHRUBS)), new WeightedGenerator<Boolean>().put(true, 1).put(false, 1), Items.LEAVES),
	STICKS("sticks", new ArrayList<FloraFeatures>(Arrays.asList(FloraFeatures.CONIFEROUS_TREES, FloraFeatures.DECIDUOUS_TREES, FloraFeatures.SHRUBS)), new WeightedGenerator<Boolean>().put(true, 1).put(false, 1), Items.STICKS);
	private String token;
	private List<FloraFeatures> floraFeatures;
	private IGenerator<Boolean> generator;
	private Items item;
	Gatherables(String token,List<FloraFeatures> floraFeatures, IGenerator<Boolean> generator, Items item){
		this.token = token;
		this.floraFeatures=floraFeatures;
		this.generator = generator;
		this.item = item;
	}
	public String getToken() {
		return token;
	}
	public List<FloraFeatures> getFloraFeatures(){
		return floraFeatures;
	}
	public IGenerator<Boolean> getGenerator(){
		return generator;
	}
	public static Gatherables parse(String token){
		for(Gatherables gatherable : Gatherables.values()){
			if(token.compareToIgnoreCase(gatherable.token)==0){
				return gatherable;
			}
		}
		return null;
	}
	public IItem generateItem() {
		return new SimpleItem(item);
	}
}
