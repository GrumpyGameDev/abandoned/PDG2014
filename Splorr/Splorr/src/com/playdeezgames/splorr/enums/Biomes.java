package com.playdeezgames.splorr.enums;

import java.util.List;
import java.util.Random;

import com.playdeezgames.splorr.generators.IGenerator;
import com.playdeezgames.splorr.generators.MultiGenerator;
import com.playdeezgames.splorr.generators.WeightedGenerator;

public enum Biomes {
	TUNDRA("You are in an frozen wasteland.",1.0/480, 
			new MultiGenerator<FloraFeatures>()
			.put(FloraFeatures.MOSS, new WeightedGenerator<Boolean>().put(true, 1).put(false, 100))
			.put(FloraFeatures.FLOWERS, new WeightedGenerator<Boolean>().put(true, 1).put(false, 1000))
			.put(FloraFeatures.CONIFEROUS_TREES, new WeightedGenerator<Boolean>().put(true, 1).put(false, 10000))
			),
	
	TAIGA("You are surrounded by evergreen trees.",1.0/120, 
			new MultiGenerator<FloraFeatures>()
			.put(FloraFeatures.CONIFEROUS_TREES, new WeightedGenerator<Boolean>().put(true, 1))
			.put(FloraFeatures.MOSS, new WeightedGenerator<Boolean>().put(true, 1).put(false,10))
			),
	
	GRASSLAND("You are in a field of grass.",1.0/480, 
			new MultiGenerator<FloraFeatures>()
			.put(FloraFeatures.GRASS, new WeightedGenerator<Boolean>().put(true, 1))
			.put(FloraFeatures.MOSS, new WeightedGenerator<Boolean>().put(true, 1).put(false,10))
			.put(FloraFeatures.FLOWERS, new WeightedGenerator<Boolean>().put(true, 1).put(false,5))
			.put(FloraFeatures.SHRUBS, new WeightedGenerator<Boolean>().put(true, 1).put(false,20))
			.put(FloraFeatures.BERRY_BUSHES, new WeightedGenerator<Boolean>().put(true, 1).put(false,20))
			),
	
	FOREST("You are in a wooded area.",1.0/120, 
			new MultiGenerator<FloraFeatures>()
			.put(FloraFeatures.DECIDUOUS_TREES, new WeightedGenerator<Boolean>().put(true, 1))
			.put(FloraFeatures.FLOWERS, new WeightedGenerator<Boolean>().put(true, 1).put(false,5))
			.put(FloraFeatures.SHRUBS, new WeightedGenerator<Boolean>().put(true, 1).put(false,20))
			.put(FloraFeatures.BERRY_BUSHES, new WeightedGenerator<Boolean>().put(true, 1).put(false,20))
			),
	
	CHAPARRAL("You are surrounded by shrubs.",1.0/240, 
			new MultiGenerator<FloraFeatures>()
			.put(FloraFeatures.SHRUBS, new WeightedGenerator<Boolean>().put(true, 1))
			.put(FloraFeatures.FLOWERS, new WeightedGenerator<Boolean>().put(true, 1).put(false,10))
			.put(FloraFeatures.BERRY_BUSHES, new WeightedGenerator<Boolean>().put(true, 1).put(false,50))
			),
	
	DESERT("You are in a desert.",1.0/480, 
			new MultiGenerator<FloraFeatures>()
			.put(FloraFeatures.CACTI, new WeightedGenerator<Boolean>().put(true, 1).put(false, 1))
			.put(FloraFeatures.FLOWERS, new WeightedGenerator<Boolean>().put(true, 1).put(false,100))
			.put(FloraFeatures.SHRUBS, new WeightedGenerator<Boolean>().put(true, 1).put(false,500))
			.put(FloraFeatures.BERRY_BUSHES, new WeightedGenerator<Boolean>().put(true, 1).put(false,500))
			),
	
	SCRUB("You are in a scrub desert.",1.0/180, 
			new MultiGenerator<FloraFeatures>()
			.put(FloraFeatures.SHRUBS, new WeightedGenerator<Boolean>().put(true, 1).put(false, 1))
			.put(FloraFeatures.FLOWERS, new WeightedGenerator<Boolean>().put(true, 1).put(false,50))
			.put(FloraFeatures.BERRY_BUSHES, new WeightedGenerator<Boolean>().put(true, 1).put(false,250))
			),
	
	SAVANNA("You are in a sparsely wooded area.",1.0/180, 
			new MultiGenerator<FloraFeatures>()
			.put(FloraFeatures.DECIDUOUS_TREES, new WeightedGenerator<Boolean>().put(true, 10).put(false, 1))
			.put(FloraFeatures.FLOWERS, new WeightedGenerator<Boolean>().put(true, 1).put(false,10))
			.put(FloraFeatures.SHRUBS, new WeightedGenerator<Boolean>().put(true, 1).put(false,50))
			.put(FloraFeatures.BERRY_BUSHES, new WeightedGenerator<Boolean>().put(true, 1).put(false,50))
			),
	
	RAINFOREST("You are in a thickly wooded, humid area.",1.0/90, 
			new MultiGenerator<FloraFeatures>()
			.put(FloraFeatures.DECIDUOUS_TREES, new WeightedGenerator<Boolean>().put(true, 1))
			),
	
	ALPINE("You are in a mountainous area.",1.0/60, 
			new MultiGenerator<FloraFeatures>()
			.put(FloraFeatures.MOSS, new WeightedGenerator<Boolean>().put(true, 1).put(false, 100))
			);
	
	private String description;
	private double travelTime;
	private IGenerator<List<FloraFeatures>> floraFeaturesGenerator;

	Biomes(String description, double travelTime, IGenerator<List<FloraFeatures>> floraFeaturesGenerator){
		this.description = description;
		this.travelTime = travelTime;
		this.floraFeaturesGenerator = floraFeaturesGenerator;
	}
	public String getDescription(){
		return this.description;
	}
	
	public double getTravelTime(){
		return this.travelTime;
	}
	
	public List<FloraFeatures> generateFloraFeatures(Random random){
		return floraFeaturesGenerator.generate(random);
	}
}
