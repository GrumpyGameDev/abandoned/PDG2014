package com.playdeezgames.common.managers;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.playdeezgames.common.interfaces.FileDescriptor;
import com.playdeezgames.common.interfaces.TextureDescriptor;
import com.playdeezgames.common.interfaces.TextureRegionDescriptor;

public class TextureRegionManager<T extends TextureRegionDescriptor<U,V>, U extends TextureDescriptor<V>, V extends FileDescriptor> extends ManagerBase{

	private Map<T,TextureRegion> table = new HashMap<T,TextureRegion>();
	private TextureManager<U, V> textureManager;
	
	public TextureRegionManager(){
		
	}
	
	public TextureRegionManager(TextureManager<U,V> textureManager){
		setTextureManager(textureManager);
	}
	
	private void setTextureManager(TextureManager<U, V> textureManager) {
		this.textureManager = textureManager;
	}
	
	public TextureManager<U, V> getTextureManager(){
		if(textureManager==null){
			textureManager=new TextureManager<U,V>();
		}
		return textureManager;
	}

	public TextureRegion getTextureRegion(T descriptor){
		if(table.containsKey(descriptor)){
			return table.get(descriptor);
		}else{
			TextureRegion region = new TextureRegion(getTextureManager().getTexture(descriptor.getTextureDescriptor()), descriptor.getX(), descriptor.getY(), descriptor.getWidth(), descriptor.getHeight());
			table.put(descriptor, region);
			return region;
		}
	}
	
	@Override
	public void dispose() {
		table.clear();
		getTextureManager().dispose();
	}

}
