package com.pdg.dungeondelver.pdgboardgames;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MazeBase<DirectionsType extends DirectionsBase,PortalType extends MazePortalBase,CellInfoType extends MazeCellInfoBase> extends Board<MazeCellBase<DirectionsType,PortalType,CellInfoType>>
{
	public MazeGenerationDelegate OnPostGenerate;
	private List<PortalType> portals= new ArrayList<PortalType>();
	private DirectionsFactory<DirectionsType> directionsInstantiator;
	public List<PortalType> getPortals()
    {
        return (portals);
    }
	public MazeBase(int theColumns, int theRows,BoardCellFactory<MazeCellBase<DirectionsType,PortalType,CellInfoType>> instantiator, MazeCellInfoFactory<CellInfoType> cellInfoInstantiator, DirectionsFactory<DirectionsType> directionsInstantiator, PortalFactory<PortalType> portalInstantiator)
	{
		super(theColumns,theRows,instantiator);
		this.directionsInstantiator = directionsInstantiator;
	    int column;
	    int row;
	    int neighborColumn;
	    int neighborRow;
	    int direction;
	    int oppositeDirection;
	    MazeCellBase<DirectionsType, PortalType, CellInfoType> cell;
	    MazeCellBase<DirectionsType, PortalType, CellInfoType> neighborCell;
	    DirectionsType directions;
		directions = directionsInstantiator.newInstance();
	    PortalType portal;
	    for (column = 0; column < getColumns(); ++column)
	    {
	        for (row = 0; row < getRows(); ++row)
	        {
	            cell = this.get(column).get(row);
	            cell.createCellInfo(cellInfoInstantiator);
	            cell.createDirections(directionsInstantiator);
	            for (direction = 0; direction < directions.getCount(); ++direction)
	            {
	                if (cell.getNeighbors().get(direction) == null)
	                {
	                    neighborColumn = directions.getNextColumn(column, row, direction);
	                    neighborRow = directions.getNextRow(column, row, direction);
	                    oppositeDirection = directions.getOpposite(direction);
	                    if (neighborColumn >= 0 && neighborRow >= 0 && neighborColumn < getColumns() && neighborRow < getRows())
	                    {
	                        neighborCell = this.get(neighborColumn).get(neighborRow);
	                        cell.getNeighbors().set(direction,neighborCell);
	                        portal = portalInstantiator.newInstance();
	                        cell.getPortals().set(direction,portal);
	                        neighborCell.getNeighbors().set(oppositeDirection,cell);
	                        neighborCell.getPortals().set(oppositeDirection,portal);
	                        this.portals.add(portal);
	                    }
	                }
	            }
	        }
	    }
	}
	public void clear()
	{
	    super.clear();
	    for (PortalType portal : getPortals())
	    {
	        portal.clear();
	    }
	}
	/// <summary>
	/// GeneratorState
	/// This one stays an enum!
	/// </summary>
	private enum GeneratorState
	{
	    Outside,
	    Frontier,
	    Inside
	}
	public void generate(IRandomNumberGenerator theRandomNumberGenerator)
	{
	    clear();
	    Map<MazeCellBase<DirectionsType, PortalType, CellInfoType>, GeneratorState> generatorStates = new HashMap<MazeCellBase<DirectionsType, PortalType, CellInfoType>, GeneratorState>();
	    List<MazeCellBase<DirectionsType, PortalType, CellInfoType>> frontierCells = new ArrayList<MazeCellBase<DirectionsType, PortalType, CellInfoType>>(getColumns() * getRows());
	    MazeCellBase<DirectionsType, PortalType, CellInfoType> cell;
	    MazeCellBase<DirectionsType, PortalType, CellInfoType> neighborCell;
	    int column;
	    int row;
	    int direction;
	    DirectionsType directions;
		directions = directionsInstantiator.newInstance();
	    for (column = 0; column < getColumns(); ++column)
	    {
	        for (row = 0; row < getRows(); ++row)
	        {
	            generatorStates.put(this.get(column).get(row), GeneratorState.Outside);
	        }
	    }
	    cell = this.get(theRandomNumberGenerator.next(getColumns())).get(theRandomNumberGenerator.next(getRows()));
	    generatorStates.put(cell,GeneratorState.Inside);
	    for (direction = 0; direction < directions.getCount(); ++direction)
	    {
	        neighborCell = cell.getNeighbors().get(direction);
	        if (neighborCell != null)
	        {
	            frontierCells.add(neighborCell);
	            generatorStates.put(neighborCell,GeneratorState.Frontier);
	        }
	    }
	    while (frontierCells.size() > 0)
	    {
	        cell = frontierCells.get(theRandomNumberGenerator.next(frontierCells.size()));
	        frontierCells.remove(cell);
	        generatorStates.put(cell,GeneratorState.Inside);
	        do
	        {
	            direction = theRandomNumberGenerator.next(directions.getCount());
	            neighborCell = cell.getNeighbors().get(direction);
	        }while(neighborCell == null || generatorStates.get(neighborCell)!=GeneratorState.Inside);
	        cell.getPortals().get(direction).Open = true;
	        for (direction = 0; direction < directions.getCount(); ++direction)
	        {
	            neighborCell = cell.getNeighbors().get(direction);
	            if (neighborCell != null && generatorStates.get(neighborCell) == GeneratorState.Outside)
	            {
	                frontierCells.add(neighborCell);
	                generatorStates.put(neighborCell,GeneratorState.Frontier);
	            }
	        }
	    }
	    if (OnPostGenerate != null)
	    {
	        OnPostGenerate.PostGenerate();
	    }
	}
}
