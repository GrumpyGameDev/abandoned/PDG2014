package com.pdg.dungeondelver.hamquestengine.tables;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;
import com.badlogic.gdx.utils.XmlReader.Element;
import com.pdg.dungeondelver.hamquestengine.gameconstants.FileNames;
import com.pdg.dungeondelver.hamquestengine.gameconstants.Properties;
import com.pdg.dungeondelver.hamquestengine.item.Descriptor;
import com.pdg.dungeondelver.hamquestengine.item.IdentifiedPropertyValuePairList;
import com.pdg.dungeondelver.hamquestengine.item.PropertyValuePair;

public class PropertyGroupTable
{
    private Map<String, Descriptor> table = new HashMap<String, Descriptor>();
    public Descriptor getPropertyDescriptor(String thePropertyIdentifier)
    {
        Descriptor result = null;
        if (table.containsKey(thePropertyIdentifier))
        {
            result = table.get(thePropertyIdentifier);
        }
        return (result);
    }
    public Set<String> getPropertyIdentifiers()
    {
            return (table.keySet());
    }
    public PropertyGroupTable(String theFileName)
    {
    	if(theFileName==null || theFileName==""){
    		theFileName=FileNames.DefaultPropertyGroupsFile;
    	}
        table.clear();
    	XmlReader reader = new XmlReader();
    	FileHandle handle = Gdx.files.internal(theFileName);
		try {
        	Element doc;
			doc = reader.parse(handle);
			Array<Element> subElements = doc.getChildByName(Properties.PropertyGroups).getChildrenByName(Properties.PropertyGroup);
			for(Element subElement:subElements){
				IdentifiedPropertyValuePairList properties = PropertyValuePair.loadPropertyValuesFromXmlNode(subElement);
				table.put(properties.identifier, new Descriptor(properties.result));
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}
