package com.playdeezgames.splorr.tokenprocessors;

import java.util.Stack;
import com.playdeezgames.splorr.world.Avatar;

public class LoginTokenProcessor implements ITokenProcessor {

	public boolean processTokens(Stack<String> tokenizer, Avatar avatar) {
		if(tokenizer.empty()){
			return false;
		}else{
			String userName = tokenizer.pop();
			if(tokenizer.empty()){
				return false;
			}else{
				String userToken = tokenizer.pop();
				if(!tokenizer.empty()){
					return false;
				}else{
					if(avatar.authenticate(userName, userToken)){
						System.out.println();
						System.out.println("Authentication success!");
					}else{
						System.out.println();
						System.out.println("Authentication failed!");
					}
					return true;
				}
			}
		}
	}

}
