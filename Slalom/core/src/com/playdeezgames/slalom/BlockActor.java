package com.playdeezgames.slalom;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class BlockActor extends Actor {
	private Texture texture;
	private TextureRegion region;
	
	public BlockActor(){
		texture = new Texture(Gdx.files.internal("badlogic.jpg"));
		region = new TextureRegion(texture);
	}
	
	public void draw(Batch batch, float parentAlpha){
		Color color = getColor();
		batch.setColor(color.r,color.g,color.b,color.a*parentAlpha);
		batch.draw(region,getX(),getY(),getOriginX(),getOriginY(), getWidth(),getHeight(), getScaleX(), getScaleY(),getRotation());
	}
}
