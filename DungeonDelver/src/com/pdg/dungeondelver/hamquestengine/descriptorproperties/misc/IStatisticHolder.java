package com.pdg.dungeondelver.hamquestengine.descriptorproperties.misc;

public interface IStatisticHolder
{
    int getValue();
}
