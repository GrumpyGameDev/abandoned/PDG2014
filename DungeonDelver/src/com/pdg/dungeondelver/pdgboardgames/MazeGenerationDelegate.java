package com.pdg.dungeondelver.pdgboardgames;

public interface MazeGenerationDelegate {
	void PostGenerate();
}
