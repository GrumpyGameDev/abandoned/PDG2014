package com.playdeezgames.precedence;

import com.playdeezgames.common.interfaces.FileDescriptor;


public enum PrecedenceFiles implements FileDescriptor{
	ONE("data/images/numbers/one.png"),
	TWO("data/images/numbers/two.png"),
	THREE("data/images/numbers/three.png"),
	FOUR("data/images/numbers/four.png"),
	FIVE("data/images/numbers/five.png"),
	SIX("data/images/numbers/six.png"),
	RED_CIRCLE("data/images/shapes/circles/red.png"),
	YELLOW_CIRCLE("data/images/shapes/circles/yellow.png"),
	GREEN_CIRCLE("data/images/shapes/circles/green.png"),
	CYAN_CIRCLE("data/images/shapes/circles/cyan.png"),
	BLUE_CIRCLE("data/images/shapes/circles/blue.png"),
	MAGENTA_CIRCLE("data/images/shapes/circles/magenta.png"),
	RED_TRIANGLE("data/images/shapes/triangles/red.png"),
	YELLOW_TRIANGLE("data/images/shapes/triangles/yellow.png"),
	GREEN_TRIANGLE("data/images/shapes/triangles/green.png"),
	CYAN_TRIANGLE("data/images/shapes/triangles/cyan.png"),
	BLUE_TRIANGLE("data/images/shapes/triangles/blue.png"),
	MAGENTA_TRIANGLE("data/images/shapes/triangles/magenta.png"),
	RED_SQUARE("data/images/shapes/squares/red.png"),
	YELLOW_SQUARE("data/images/shapes/squares/yellow.png"),
	GREEN_SQUARE("data/images/shapes/squares/green.png"),
	CYAN_SQUARE("data/images/shapes/squares/cyan.png"),
	BLUE_SQUARE("data/images/shapes/squares/blue.png"),
	MAGENTA_SQUARE("data/images/shapes/squares/magenta.png"),
	RED_PENTAGON("data/images/shapes/pentagons/red.png"),
	YELLOW_PENTAGON("data/images/shapes/pentagons/yellow.png"),
	GREEN_PENTAGON("data/images/shapes/pentagons/green.png"),
	CYAN_PENTAGON("data/images/shapes/pentagons/cyan.png"),
	BLUE_PENTAGON("data/images/shapes/pentagons/blue.png"),
	MAGENTA_PENTAGON("data/images/shapes/pentagons/magenta.png"),
	RED_HEXAGON("data/images/shapes/hexagons/red.png"),
	YELLOW_HEXAGON("data/images/shapes/hexagons/yellow.png"),
	GREEN_HEXAGON("data/images/shapes/hexagons/green.png"),
	CYAN_HEXAGON("data/images/shapes/hexagons/cyan.png"),
	BLUE_HEXAGON("data/images/shapes/hexagons/blue.png"),
	MAGENTA_HEXAGON("data/images/shapes/hexagons/magenta.png"),
	RED_OCTAGON("data/images/shapes/octagons/red.png"),
	YELLOW_OCTAGON("data/images/shapes/octagons/yellow.png"),
	GREEN_OCTAGON("data/images/shapes/octagons/green.png"),
	CYAN_OCTAGON("data/images/shapes/octagons/cyan.png"),
	BLUE_OCTAGON("data/images/shapes/octagons/blue.png"),
	MAGENTA_OCTAGON("data/images/shapes/octagons/magenta.png"),
	MAIN_MENU("data/images/ui/main_menu.png"),
	SPLASH("data/images/ui/splash.png");
	private String fileName;
	PrecedenceFiles(String fileName){
		setFileName(fileName);
	}
	private void setFileName(String fileName) {
		this.fileName=fileName;
	}
	@Override
	public String getFileName() {
		return fileName;
	}

}
