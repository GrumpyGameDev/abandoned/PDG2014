package com.pdg.dungeondelver.hamquestengine.tables;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;
import com.badlogic.gdx.utils.XmlReader.Element;
import com.pdg.dungeondelver.hamquestengine.gameconstants.FileNames;
import com.pdg.dungeondelver.hamquestengine.gameconstants.ItemTypes;
import com.pdg.dungeondelver.hamquestengine.gameconstants.Properties;
import com.pdg.dungeondelver.hamquestengine.item.Descriptor;
import com.pdg.dungeondelver.hamquestengine.item.IdentifiedPropertyValuePairList;
import com.pdg.dungeondelver.hamquestengine.item.PropertyValuePair;
import com.pdg.dungeondelver.pdgboardgames.WeightedGenerator;

public class ItemTable
{
    public WeightedGenerator<Integer> LockTypeGenerator = new WeightedGenerator<Integer>();
    private Map<String, Descriptor> table = new HashMap<String, Descriptor>();
    public Descriptor getItemDescriptor(String theItemIdentifier)
    {
        Descriptor result = null;
        if (table.containsKey(theItemIdentifier))
        {
            result = table.get(theItemIdentifier);
        }
        return (result);
    }
    public Set<String> getItemIdentifiers()
    {
            return (table.keySet());
    }
    public ItemTable(String theItemsFileName)
    {
    	if(theItemsFileName==null || theItemsFileName==""){
    		theItemsFileName = FileNames.DefaultItemsFile;
    	}
        table.clear();
        table.put("", null);
        table.clear();
    	XmlReader reader = new XmlReader();
    	FileHandle handle = Gdx.files.internal(theItemsFileName);
		try {
        	Element doc;
			doc = reader.parse(handle);
			Array<Element> subElements = doc.getChildByName(Properties.Items).getChildrenByName(Properties.Item);
			for(Element subElement:subElements){
				IdentifiedPropertyValuePairList properties = PropertyValuePair.loadPropertyValuesFromXmlNode(subElement);
				table.put(properties.identifier, new Descriptor(properties.result));
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        keyTable.clear();
        doorTable.clear();
        for (String itemIdentifier : table.keySet())
        {
            Descriptor itemDescriptor = table.get(itemIdentifier);
            if (itemDescriptor == null) continue;
            if (itemDescriptor.<String>getProperty(Properties.ItemType) == ItemTypes.Key)
            {
                keyTable.put(itemDescriptor.<Integer>getProperty(Properties.LockType), itemIdentifier);
                LockTypeGenerator.put(itemDescriptor.<Integer>getProperty(Properties.LockType),itemDescriptor.<Integer>getProperty(Properties.LockTypeWeight));
            }
            if (itemDescriptor.<String>getProperty(Properties.ItemType) == ItemTypes.Door)
            {
                if (!doorTable.containsKey(itemDescriptor.<Integer>getProperty(Properties.LockType)))
                {
                    doorTable.put(itemDescriptor.<Integer>getProperty(Properties.LockType), new HashMap<Integer, String>());
                }
                doorTable.get(itemDescriptor.<Integer>getProperty(Properties.LockType)).put(itemDescriptor.<Integer>getProperty(Properties.Direction), itemIdentifier);
            }
        }

    }
    private Map<Integer, Map<Integer, String>> doorTable = new HashMap<Integer, Map<Integer, String>>();
    private Map<Integer, String> keyTable = new HashMap<Integer, String>();
    public String getDoorItemIdentifier(int lockType, int direction)
    {
        return (doorTable.get(lockType).get(direction));
    }
    public String getKeyItemIdentifier(int lockType)
    {
        return (keyTable.get(lockType));
    }
    public int getQuestItemCount()
    {
            int result = 0;
            for (String identifier : table.keySet())
            {
                Descriptor descriptor = getItemDescriptor(identifier);
                if (descriptor != null && descriptor.<String>getProperty(Properties.ItemType) == ItemTypes.Quest)
                {
                    result += descriptor.<Integer>getProperty(Properties.ItemCount);
                }
            }
            return result;
    }
}