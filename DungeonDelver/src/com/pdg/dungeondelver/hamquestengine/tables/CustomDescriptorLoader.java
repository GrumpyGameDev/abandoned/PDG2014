package com.pdg.dungeondelver.hamquestengine.tables;

import java.util.List;

import com.pdg.dungeondelver.hamquestengine.item.Descriptor;
import com.pdg.dungeondelver.hamquestengine.item.PropertyValuePair;

public interface CustomDescriptorLoader {
	Descriptor loadCustomDescriptor(List<PropertyValuePair> thePropertyValues);
}
