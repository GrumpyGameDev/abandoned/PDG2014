package com.playdeezgames.precedence;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.playdeezgames.common.statemachine.StateMachine;
import com.playdeezgames.precedence.states.MainMenuState;
import com.playdeezgames.precedence.states.SplashState;

public class Precedence extends StateMachine<PrecedenceStates> implements ApplicationListener {
	private ResourceManager resourceManager;
	private ResourceManager getResourceManager(){
		if(resourceManager==null){
			resourceManager=new ResourceManager();
		}
		return resourceManager;
	}
	public Precedence(){
		setState(PrecedenceStates.SPLASH,new SplashState(this,getResourceManager()));
		setState(PrecedenceStates.MAIN_MENU,new MainMenuState(this,getResourceManager()));
	}
	
	@Override
	public void create(){
		super.create();
		getResourceManager().getStage().addListener(this);
		Gdx.input.setInputProcessor(getResourceManager().getStage());
		setCurrent(PrecedenceStates.SPLASH);
	}
	
	@Override
	public void dispose(){
		getResourceManager().dispose();
	}
}
