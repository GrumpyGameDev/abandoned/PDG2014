package com.pdg.dungeondelver.hamquestengine.descriptorproperties.playerspecific;

import com.badlogic.gdx.utils.XmlReader.Element;
import com.pdg.dungeondelver.hamquestengine.item.CustomPropertyLoader;

public class PlayerConfigurationLoader implements CustomPropertyLoader{

	@Override
	public Object loadFromNode(Element node) {
		return new PlayerConfiguration(node.getText());
	}

}
