package com.pdg.dungeondelver.hamquestengine.descriptorproperties.bumphandlers;

import com.pdg.dungeondelver.hamquestengine.item.Descriptor;
import com.pdg.dungeondelver.hamquestengine.maps.Creature;

public interface IBumpHandler
{
    BumpResult bump(Descriptor theDescriptor, Creature theCreature);
}
