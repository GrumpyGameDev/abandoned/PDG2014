package com.playdeezgames.splorr.generators;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class WeightedGenerator<T> implements IGenerator<T> {
	private Map<T,Integer> table = new HashMap<T,Integer>();
	private int total = 0;
	
	public WeightedGenerator(){
		
	}
	
	public WeightedGenerator<T> put(T key, Integer weight){
		total-=get(key);
		if(weight<=0){
			table.remove(key);
		}else{
			table.put(key, weight);
		}
		total+=get(key);
		return this;
	}
	
	public Integer get(T key){
		if(table.containsKey(key)){
			return table.get(key);
		}else{
			return 0;
		}
	}
	
	public T generate(Random random){
		if(total==0) return null;
		int generated = random.nextInt(total);
		
		for(T key : table.keySet()){
			Integer value = get(key);
			if(generated<value){
				return key;
			}else{
				generated-=value;
			}
		}
		
		return null;
	}
	
}
