package com.playdeezgames.common.interfaces;

public interface TextureRegionDescriptor<T extends TextureDescriptor<U>,U extends FileDescriptor> {
	T getTextureDescriptor();
	int getX();
	int getY();
	int getWidth();
	int getHeight();
}
