package com.playdeezgames.splorr.world;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.playdeezgames.splorr.enums.Biomes;
import com.playdeezgames.splorr.enums.FloraFeatures;
import com.playdeezgames.splorr.generators.BiomeNeighborGenerator;
import com.playdeezgames.splorr.items.IItem;

public class WorldCell implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -4566950429740468061L;
	private Biomes biome;
	private List<FloraFeatures> floraFeatures;
	private List<IItem> items = new ArrayList<IItem>();
	
	public WorldCell(Random random){
		setBiome(Biomes.values()[random.nextInt(Biomes.values().length)]);
		setFloraFeatures(getBiome().generateFloraFeatures(random));
	}
	
	public WorldCell(){
		
	}

	public Biomes getBiome() {
		return biome;
	}
	
	public List<IItem> getItems(){
		return items;
	}

	public void setBiome(Biomes biome) {
		this.biome = biome;
	}

	public WorldCell generateNeighbor(Random random) {
		
		WorldCell neighbor = new WorldCell();
		
		neighbor.setBiome(BiomeNeighborGenerator.generateNeighborBiome(getBiome(),random));
		
		neighbor.setFloraFeatures(neighbor.getBiome().generateFloraFeatures(random));
		
		return neighbor;
		
	}

	private void setFloraFeatures(List<FloraFeatures> floraFeatures) {
		this.floraFeatures = floraFeatures;
	}

	public List<FloraFeatures> getFloraFeatures() {
		return floraFeatures;
	}
}
