package com.playdeezgames.splorr.tokenprocessors;

import java.util.Stack;
import com.playdeezgames.splorr.world.Avatar;

public class QuitTokenProcessor implements ITokenProcessor {

	public boolean processTokens(Stack<String> tokenizer, Avatar avatar) {
		if(tokenizer.empty()){
			avatar.setQuit(true);
			System.out.println();
			System.out.println("Quitting game...");
			return true;
		}else{
			return false;
		}
	}

}
