package com.pdg.dungeondelver.pdgboardgames;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class CountedCollection<CountedType extends Comparable<CountedType>> {
    private Map<CountedType, Integer> counts = new HashMap<CountedType, Integer>();
    public Set<CountedType> getIdentifiers()
    {
    	return (counts.keySet());
    }
    public CountedType getMinimalValue()
    {
        CountedType result = null;
        for (CountedType value : counts.keySet())
        {
            if (result==null || result.compareTo(value) > 0)
            {
                result = value;
            }
        }
        return result;
    }
    public CountedType getMaximalValue()
    {
        CountedType result = null;
        for (CountedType value : counts.keySet())
        {
            if (result==null || result.compareTo(value) < 0)
            {
                result = value;
            }
        }
        return result;
    }
    public void add(CountedType identifier, Integer count)
    {
        if (counts.containsKey(identifier))
        {
            counts.put(identifier,counts.get(identifier)+count);
        }
        else
        {
            counts.put(identifier,count);
        }
    }
    public void add(CountedType identifier)
    {
        add(identifier, 1);
    }
    public boolean has(CountedType identifier)
    {
        return counts.containsKey(identifier) && counts.get(identifier) > 0;
    }
    public Integer get(CountedType identifier)
    {
        if (has(identifier))
        {
            return counts.get(identifier);
        }
        else
        {
            return 0;
        }
    }
    public void put(CountedType identifier,Integer value){
    	counts.put(identifier, value);
    }
    public Integer remove(CountedType identifier, Integer count)
    {
        if (counts.containsKey(identifier))
        {
            if (count >= counts.get(identifier))
            {
                count = counts.get(identifier);
                counts.remove(identifier);
                return (count);
            }
            else
            {
            	counts.put(identifier, counts.get(identifier)-count);
                return (count);
            }
        }
        else
        {
            return (0);
        }
    }
    public boolean remove(CountedType identifier)
    {
        return remove(identifier, 1) > 0;
    }

}
