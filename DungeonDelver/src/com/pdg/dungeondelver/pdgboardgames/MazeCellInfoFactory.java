package com.pdg.dungeondelver.pdgboardgames;

public interface MazeCellInfoFactory<CellInfoType extends MazeCellInfoBase> {
	CellInfoType newInstance();
}
