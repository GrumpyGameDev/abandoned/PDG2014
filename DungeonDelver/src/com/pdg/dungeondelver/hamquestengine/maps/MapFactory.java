package com.pdg.dungeondelver.hamquestengine.maps;

import com.pdg.dungeondelver.pdgboardgames.BoardCellFactory;

public class MapFactory implements BoardCellFactory<MapCell> {

	@Override
	public MapCell newInstance() {
		return new MapCell();
	}

}
