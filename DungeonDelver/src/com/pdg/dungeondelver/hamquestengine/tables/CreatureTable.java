package com.pdg.dungeondelver.hamquestengine.tables;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;
import com.badlogic.gdx.utils.XmlReader.Element;
import com.pdg.dungeondelver.hamquestengine.gameconstants.Attributes;
import com.pdg.dungeondelver.hamquestengine.gameconstants.FileNames;
import com.pdg.dungeondelver.hamquestengine.gameconstants.Properties;
import com.pdg.dungeondelver.hamquestengine.item.Descriptor;
import com.pdg.dungeondelver.hamquestengine.item.IdentifiedPropertyValuePairList;
import com.pdg.dungeondelver.hamquestengine.item.PropertyValuePair;
import com.pdg.dungeondelver.pdgboardgames.IRandomNumberGenerator;

public class CreatureTable
{
    private Map<String, Descriptor> table = new HashMap<String, Descriptor>();
    private Map<String, List<String>> generatorTables = new HashMap<String, List<String>>();
    public Set<String> getCreatureIdentifiers()
    {
    	return (table.keySet());
    }
    public Descriptor getCreatureDescriptor(String creatureIdentifier)
    {
        return (table.get(creatureIdentifier));
    }
    private List<String> getGeneratorTable(String generatorTable)
    {
        if (generatorTables.containsKey(generatorTable))
        {
            return generatorTables.get(generatorTable);
        }
        else
        {
            List<String> theTable = new ArrayList<String>();
            for(String identifier : table.keySet())
            {
                Descriptor descriptor = getCreatureDescriptor(identifier);
                if (descriptor.hasProperty(generatorTable))
                {
                    int count = descriptor.<Integer>getProperty(generatorTable);
                    while (count > 0)
                    {
                        theTable.add(identifier);
                        count--;
                    }
                }
            }
            generatorTables.put(generatorTable, theTable);
            return theTable;
        }
    }
    public String generateCreature(String generatorTable,IRandomNumberGenerator theRandomNumberGenerator)
    {
        List<String> theTable = getGeneratorTable(generatorTable);
        return theTable.get(theRandomNumberGenerator.next(theTable.size()));
    }
    private String creaturesFile;
    public void reset()
    {
        table.clear();
    	XmlReader reader = new XmlReader();
    	FileHandle handle = Gdx.files.internal(creaturesFile);
		try {
        	Element doc;
			doc = reader.parse(handle);
			Array<Element> subElements = doc.getChildByName(Properties.Creatures).getChildrenByName(Properties.Creature);
			for(Element subElement:subElements){
				IdentifiedPropertyValuePairList properties = PropertyValuePair.loadPropertyValuesFromXmlNode(subElement);
				if(subElement.getAttribute(Attributes.Type)!=null){
					String typeName=subElement.getAttribute(Attributes.Type);
					CustomDescriptorLoader customLoader = (CustomDescriptorLoader)Class.forName(typeName).newInstance();
					table.put(properties.identifier,customLoader.loadCustomDescriptor(properties.result));
				}else{
					table.put(properties.identifier, new Descriptor(properties.result));
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    public CreatureTable(String theCreaturesFile)
    {
    	if(theCreaturesFile==null || theCreaturesFile==""){
    		theCreaturesFile = FileNames.DefaultCreaturesFile;
    	}
        creaturesFile = theCreaturesFile;
        reset();
    }
}

