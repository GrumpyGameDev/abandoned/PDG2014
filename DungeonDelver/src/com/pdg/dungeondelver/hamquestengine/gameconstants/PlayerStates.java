package com.pdg.dungeondelver.hamquestengine.gameconstants;

public class PlayerStates {
    public static final String Lose = "Lose";
    public static final String Play = "Play";
    public static final String Win = "Win";
}
