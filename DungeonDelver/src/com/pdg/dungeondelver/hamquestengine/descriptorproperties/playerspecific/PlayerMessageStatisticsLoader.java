package com.pdg.dungeondelver.hamquestengine.descriptorproperties.playerspecific;

import com.badlogic.gdx.utils.XmlReader.Element;
import com.pdg.dungeondelver.hamquestengine.item.CustomPropertyLoader;

public class PlayerMessageStatisticsLoader implements CustomPropertyLoader{

	@Override
	public Object loadFromNode(Element node) {
		return new PlayerMessageStatistics();
	}

}
