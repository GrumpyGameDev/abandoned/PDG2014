package com.playdeezgames.common.interfaces;

import com.badlogic.gdx.graphics.Texture.TextureFilter;

public interface TextureDescriptor<T extends FileDescriptor>{
	T getFileDescriptor();
	TextureFilter getMinFilter();
	TextureFilter getMagFilter();
}
