package com.pdg.dungeondelver.pdgboardgames;

import java.util.ArrayList;
import java.util.List;

public class MazeCellBase<DirectionsType extends DirectionsBase,PortalType extends MazePortalBase,CellInfoType extends MazeCellInfoBase> extends BoardCellBase
{
	private List<MazeCellBase<DirectionsType, PortalType, CellInfoType>> neighbors;
	private List<PortalType> portals;
	private CellInfoType cellInfo;
	private DirectionsType directions;
	private DirectionsFactory<DirectionsType> directionsInstantiator;
	public void createCellInfo(MazeCellInfoFactory<CellInfoType> cellInfoTypeInstantiator){
		if(cellInfo!=null) return;
		cellInfo = cellInfoTypeInstantiator.newInstance();
	}
	public void createDirections(DirectionsFactory<DirectionsType> directionsTypeInstantiator){
		if(directionsInstantiator==directionsTypeInstantiator) return;
		directionsInstantiator=directionsTypeInstantiator;
		directions = directionsTypeInstantiator.newInstance();
	    neighbors = new ArrayList<MazeCellBase<DirectionsType, PortalType, CellInfoType>>();//[directions.Count];
	    while(neighbors.size()<directions.getCount()) neighbors.add(null);
	    portals = new ArrayList<PortalType>();//[directions.Count];
	    while(portals.size()<directions.getCount()) portals.add(null);
	}


	public List<MazeCellBase<DirectionsType, PortalType, CellInfoType>> getNeighbors()
    {
        return (neighbors);
    }
	public List<PortalType> getPortals()
    {
        return (portals);
    }
	public int getOpenPortalCount()
    {
        int openCount = 0;
        for (int direction = 0; direction < directions.getCount(); ++direction)
        {
            if (getPortals().get(direction) != null && getPortals().get(direction).Open)
            {
                openCount++;
            }
        }
        return openCount;
    }
	public CellInfoType getCellInfo()
    {
        return (cellInfo);
    }
	@SuppressWarnings("unchecked")
	public void setCellInfo(CellInfoType value)
    {
        cellInfo = (CellInfoType)value.clone();
    }
	@Override
	public void clear()
	{
	    cellInfo.clear();
	}
	public MazeCellBase()
	{
	}

	public Object clone()
	{
	    MazeCellBase<DirectionsType, PortalType, CellInfoType> result = new MazeCellBase<DirectionsType, PortalType, CellInfoType>();
	    result.createDirections(directionsInstantiator);
	    result.setCellInfo(getCellInfo());
	    return (result);
	}


}

