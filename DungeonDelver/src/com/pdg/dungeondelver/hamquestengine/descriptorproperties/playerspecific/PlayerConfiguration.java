package com.pdg.dungeondelver.hamquestengine.descriptorproperties.playerspecific;

public class PlayerConfiguration
{

    private String current = "";
    private PlayerConfigurationChangeDelegate onChange=null;
    public String getCurrent()
    {
        return current;
    }
    public void setCurrent(String value)
    {
        current = value;
        if (onChange != null)
        {
            onChange.onChange();
        }
    }
    public PlayerConfigurationChangeDelegate getOnChange()
    {
        return onChange;
    }
    public void setOnChange(PlayerConfigurationChangeDelegate value)
    {
        onChange = value;
    }
    public PlayerConfiguration(String theCurrent)
    {
        current = theCurrent;
    }
}
