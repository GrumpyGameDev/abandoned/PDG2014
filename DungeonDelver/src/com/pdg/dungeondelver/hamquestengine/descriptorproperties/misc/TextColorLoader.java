package com.pdg.dungeondelver.hamquestengine.descriptorproperties.misc;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.utils.XmlReader.Element;
import com.pdg.dungeondelver.hamquestengine.gameconstants.Properties;
import com.pdg.dungeondelver.hamquestengine.item.CustomPropertyLoader;

public class TextColorLoader  implements CustomPropertyLoader
{
    public Color loadFromNode(Element node)
    {
        float a = Byte.parseByte(node.getChildByName(Properties.Alpha).getText())/255.0f;
        float r = Byte.parseByte(node.getChildByName(Properties.Red).getText())/255.0f;
        float g = Byte.parseByte(node.getChildByName(Properties.Green).getText())/255.0f;
        float b = Byte.parseByte(node.getChildByName(Properties.Blue).getText())/255.0f;
        return new Color(r,g,b,a);
    }
}
