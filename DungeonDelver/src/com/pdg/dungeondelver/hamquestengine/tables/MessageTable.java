package com.pdg.dungeondelver.hamquestengine.tables;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;
import com.badlogic.gdx.utils.XmlReader.Element;
import com.pdg.dungeondelver.hamquestengine.gameconstants.FileNames;
import com.pdg.dungeondelver.hamquestengine.gameconstants.Properties;
import com.pdg.dungeondelver.hamquestengine.item.Descriptor;
import com.pdg.dungeondelver.hamquestengine.item.IdentifiedPropertyValuePairList;
import com.pdg.dungeondelver.hamquestengine.item.PropertyValuePair;

public class MessageTable
{
    private Map<String, Descriptor> table = new HashMap<String, Descriptor>();
    public Descriptor getMessageDescriptor(String theMessageIdentifier)
    {
        Descriptor result = null;
        if (table.containsKey(theMessageIdentifier))
        {
            result = table.get(theMessageIdentifier);
        }
        return (result);
    }
    public MessageTable(String theFileName)
    {
    	if(theFileName==null || theFileName==""){
    		theFileName=FileNames.DefaultMessagesFile;
    	}
        table.clear();
    	XmlReader reader = new XmlReader();
    	FileHandle handle = Gdx.files.internal(theFileName);
		try {
        	Element doc;
			doc = reader.parse(handle);
			Array<Element> subElements = doc.getChildByName(Properties.Messages).getChildrenByName(Properties.Message);
			for(Element subElement:subElements){
				IdentifiedPropertyValuePairList properties = PropertyValuePair.loadPropertyValuesFromXmlNode(subElement);
				table.put(properties.identifier, new Descriptor(properties.result));
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    public String translateMessageText(String message)
    {
        Descriptor messageDescriptor = getMessageDescriptor(message);
        if (messageDescriptor != null)
        {
            if (messageDescriptor.hasProperty(Properties.Text))
            {
                return messageDescriptor.<String>getProperty(Properties.Text);
            }
            else
            {
                return message;
            }
        }
        else
        {
            return message;
        }
    }
}
