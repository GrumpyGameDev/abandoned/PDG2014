package com.playdeezgames.precedence.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.TimeUtils;
import com.playdeezgames.common.statemachine.State;
import com.playdeezgames.common.statemachine.StateMachine;
import com.playdeezgames.precedence.PrecedenceStates;
import com.playdeezgames.precedence.PrecedenceTextureRegions;
import com.playdeezgames.precedence.ResourceManager;

public class SplashState extends StateBase<PrecedenceStates> implements State{
	
	private Image splashImage;
	private long startTime;

	public SplashState(StateMachine<PrecedenceStates> stateMachine, ResourceManager resourceManager) {
		super(stateMachine,resourceManager);
	}

	@Override
	public boolean handle(Event event) {
		return false;
	}

	@Override
	public void render() {
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		
		getResourceManager().getStage().act(Gdx.graphics.getDeltaTime());
		getResourceManager().getStage().draw();
		
		if(TimeUtils.millis()-startTime>3000){
			getStateMachine().setCurrent(PrecedenceStates.MAIN_MENU);
		}
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
	}

	private Actor getSplashImage() {
		if(splashImage==null){
			Stage stage = getResourceManager().getStage();
			splashImage=new Image(getResourceManager().getTextureRegionManager().getTextureRegion(PrecedenceTextureRegions.SPLASH));
			splashImage.setX(stage.getWidth()/2-splashImage.getWidth()/2);
			splashImage.setY(stage.getHeight()/2-splashImage.getHeight()/2);
			splashImage.setTouchable(Touchable.enabled);
			splashImage.addAction(Actions.alpha(0));
		}
		return splashImage;
	}

	@Override
	public void resume() {
	}

	@Override
	public void create() {
		Stage stage = getResourceManager().getStage();
		stage.addActor(getSplashImage());
	}

	@Override
	public void finish() {
		getSplashImage().addAction(Actions.alpha(0,1));
	}

	@Override
	public void start() {
		getSplashImage().addAction(Actions.alpha(1,1));
		startTime=TimeUtils.millis();
	}

}
