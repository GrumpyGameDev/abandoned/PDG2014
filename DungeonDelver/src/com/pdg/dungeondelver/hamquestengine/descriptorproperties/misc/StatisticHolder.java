package com.pdg.dungeondelver.hamquestengine.descriptorproperties.misc;

import com.badlogic.gdx.utils.XmlReader.Element;

public class StatisticHolder implements IStatisticHolder
{
    private int value;
    public int getValue()
    {
            return value;
    }
    public StatisticHolder(int theValue)
    {
        value = theValue;
    }
    public static StatisticHolder LoadFromNode(Element node)
    {
        int theValue;
        theValue = Integer.parseInt(node.getText());
        return new StatisticHolder(theValue);
    }
}
