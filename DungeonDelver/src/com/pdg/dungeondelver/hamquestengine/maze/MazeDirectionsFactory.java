package com.pdg.dungeondelver.hamquestengine.maze;

import com.pdg.dungeondelver.hamquestengine.directions.Directions;
import com.pdg.dungeondelver.pdgboardgames.DirectionsFactory;

public class MazeDirectionsFactory implements DirectionsFactory<Directions> {

	@Override
	public Directions newInstance() {
		return new Directions();
	}

}
