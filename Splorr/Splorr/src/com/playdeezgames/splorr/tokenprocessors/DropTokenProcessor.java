package com.playdeezgames.splorr.tokenprocessors;

import java.util.Stack;

import com.playdeezgames.splorr.enums.Hands;
import com.playdeezgames.splorr.enums.Items;
import com.playdeezgames.splorr.generators.WeightedGenerator;
import com.playdeezgames.splorr.items.IItem;
import com.playdeezgames.splorr.world.Avatar;

public class DropTokenProcessor implements ITokenProcessor {

	public boolean processTokens(Stack<String> tokenizer, Avatar avatar) {
		if(tokenizer.empty()){
			return false;
		}else{
			//first, the item
			Items item = Items.parse(tokenizer.pop());
			if(item==null){
				return false;
			}else{
				//next... which hand?
				Hands hand = null;
				WeightedGenerator<Hands> handGenerator = new WeightedGenerator<Hands>();
				//no hand specified?
				if(tokenizer.empty()){
					//determine which hand, if any, have the item
					for(Hands which : Hands.values()){
						IItem heldItem = avatar.getHeldItem(which);
						if(heldItem!=null && heldItem.getItem()==item){
							handGenerator.put(which,1);
						}
					}
					hand = handGenerator.generate(avatar.getRandom());
					if(hand==null){
						System.out.println();
						System.out.println("You are not holding any.");
						return true;
					}
				}else if(tokenizer.size()==3){
					hand = CommonPhraseParsers.WithHandParser(tokenizer);
					if(hand==null){
						return false;
					}else{
						IItem heldItem = avatar.getHeldItem(hand);
						if(heldItem.getItem()!=item){
							System.out.println();
							System.out.println("You are not holding any in that hand.");
							return true;
						}
					}
				}else{
					//something else... fail parsing
					return false;
				}
				avatar.getCurrentCell().getItems().add(avatar.getHeldItem(hand));
				avatar.setHeldItem(hand, null);
				System.out.println();
				System.out.println("You drop it.");
				return true;
			}
		}
	}

}
