package com.playdeezgames.ctyr;

public interface TextureDescriptor {
	String getFilename();
}
