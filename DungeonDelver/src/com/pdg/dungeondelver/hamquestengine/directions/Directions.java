package com.pdg.dungeondelver.hamquestengine.directions;

import com.pdg.dungeondelver.pdgboardgames.DirectionsBase;

public class Directions extends DirectionsBase
{
    public final static int North = 0;
    public final static int East = 1;
    public final static int South = 2;
    public final static int West = 3;

    @Override
    public int getCount()
    {
        return (4);
    }

    @Override
    public int getOpposite(int direction)
    {
        return ((direction + 2) % 4);
    }

    public int getNextColumn(int startColumn, int startRow, int direction)
    {
        switch (direction % 4)
        {
            case 1:
                return (startColumn + 1);
            case 3:
                return (startColumn - 1);
            default:
                return (startColumn);
        }
    }

    @Override
    public int getNextRow(int startColumn, int startRow, int direction)
    {
        switch (direction % 4)
        {
            case 2:
                return (startRow + 1);
            case 0:
                return (startRow - 1);
            default:
                return (startRow);
        }
    }
}
