package com.playdeezgames.precedence;

import com.playdeezgames.common.interfaces.TextureRegionDescriptor;


public enum PrecedenceTextureRegions implements TextureRegionDescriptor<PrecedenceTextures,PrecedenceFiles>{
	ONE(PrecedenceTextures.ONE,0,0,256,256),
	TWO(PrecedenceTextures.TWO,0,0,256,256),
	THREE(PrecedenceTextures.THREE,0,0,256,256),
	FOUR(PrecedenceTextures.FOUR,0,0,256,256),
	FIVE(PrecedenceTextures.FIVE,0,0,256,256),
	SIX(PrecedenceTextures.SIX,0,0,256,256),
	RED_CIRCLE(PrecedenceTextures.RED_CIRCLE,0,0,512,512),
	YELLOW_CIRCLE(PrecedenceTextures.YELLOW_CIRCLE,0,0,512,512),
	GREEN_CIRCLE(PrecedenceTextures.GREEN_CIRCLE,0,0,512,512),
	CYAN_CIRCLE(PrecedenceTextures.CYAN_CIRCLE,0,0,512,512),
	BLUE_CIRCLE(PrecedenceTextures.BLUE_CIRCLE,0,0,512,512),
	MAGENTA_CIRCLE(PrecedenceTextures.MAGENTA_CIRCLE,0,0,512,512),
	RED_TRIANGLE(PrecedenceTextures.RED_TRIANGLE,0,0,512,512),
	YELLOW_TRIANGLE(PrecedenceTextures.YELLOW_TRIANGLE,0,0,512,512),
	GREEN_TRIANGLE(PrecedenceTextures.GREEN_TRIANGLE,0,0,512,512),
	CYAN_TRIANGLE(PrecedenceTextures.CYAN_TRIANGLE,0,0,512,512),
	BLUE_TRIANGLE(PrecedenceTextures.BLUE_TRIANGLE,0,0,512,512),
	MAGENTA_TRIANGLE(PrecedenceTextures.MAGENTA_TRIANGLE,0,0,512,512),
	RED_SQUARE(PrecedenceTextures.RED_SQUARE,0,0,512,512),
	YELLOW_SQUARE(PrecedenceTextures.YELLOW_SQUARE,0,0,512,512),
	GREEN_SQUARE(PrecedenceTextures.GREEN_SQUARE,0,0,512,512),
	CYAN_SQUARE(PrecedenceTextures.CYAN_SQUARE,0,0,512,512),
	BLUE_SQUARE(PrecedenceTextures.BLUE_SQUARE,0,0,512,512),
	MAGENTA_SQUARE(PrecedenceTextures.MAGENTA_SQUARE,0,0,512,512),
	RED_PENTAGON(PrecedenceTextures.RED_PENTAGON,0,0,512,512),
	YELLOW_PENTAGON(PrecedenceTextures.YELLOW_PENTAGON,0,0,512,512),
	GREEN_PENTAGON(PrecedenceTextures.GREEN_PENTAGON,0,0,512,512),
	CYAN_PENTAGON(PrecedenceTextures.CYAN_PENTAGON,0,0,512,512),
	BLUE_PENTAGON(PrecedenceTextures.BLUE_PENTAGON,0,0,512,512),
	MAGENTA_PENTAGON(PrecedenceTextures.MAGENTA_PENTAGON,0,0,512,512),
	RED_HEXAGON(PrecedenceTextures.RED_HEXAGON,0,0,512,512),
	YELLOW_HEXAGON(PrecedenceTextures.YELLOW_HEXAGON,0,0,512,512),
	GREEN_HEXAGON(PrecedenceTextures.GREEN_HEXAGON,0,0,512,512),
	CYAN_HEXAGON(PrecedenceTextures.CYAN_HEXAGON,0,0,512,512),
	BLUE_HEXAGON(PrecedenceTextures.BLUE_HEXAGON,0,0,512,512),
	MAGENTA_HEXAGON(PrecedenceTextures.MAGENTA_HEXAGON,0,0,512,512),
	RED_OCTAGON(PrecedenceTextures.RED_OCTAGON,0,0,512,512),
	YELLOW_OCTAGON(PrecedenceTextures.YELLOW_OCTAGON,0,0,512,512),
	GREEN_OCTAGON(PrecedenceTextures.GREEN_OCTAGON,0,0,512,512),
	CYAN_OCTAGON(PrecedenceTextures.CYAN_OCTAGON,0,0,512,512),
	BLUE_OCTAGON(PrecedenceTextures.BLUE_OCTAGON,0,0,512,512),
	MAGENTA_OCTAGON(PrecedenceTextures.MAGENTA_OCTAGON,0,0,512,512),
	MAIN_MENU_INSTRUCTIONS_NORMAL(PrecedenceTextures.MAIN_MENU,0,0,640,112),
	MAIN_MENU_INSTRUCTIONS_HILITE(PrecedenceTextures.MAIN_MENU,0,112,640,112),
	MAIN_MENU_ABOUT_HILITE(PrecedenceTextures.MAIN_MENU,640,0,320,112),
	MAIN_MENU_ABOUT_NORMAL(PrecedenceTextures.MAIN_MENU,640,112,320,112),
	MAIN_MENU_OPTIONS_NORMAL(PrecedenceTextures.MAIN_MENU,0,224,416,112),
	MAIN_MENU_OPTIONS_HILITE(PrecedenceTextures.MAIN_MENU,0,336,416,112),
	MAIN_MENU_START_NORMAL(PrecedenceTextures.MAIN_MENU,416,336,288,112),
	MAIN_MENU_START_HILITE(PrecedenceTextures.MAIN_MENU,416,224,288,112),
	MAIN_MENU_QUIT_HILITE(PrecedenceTextures.MAIN_MENU,416+288,336,224,112),
	MAIN_MENU_QUIT_NORMAL(PrecedenceTextures.MAIN_MENU,416+288,224,224,112),
	SPLASH(PrecedenceTextures.SPLASH,0,0,512,512);
	private PrecedenceTextures textureDescriptor;
	private int x;
	private int y;
	private int width;
	private int height;
	PrecedenceTextureRegions(PrecedenceTextures textureDescriptor, int x,int y, int width,int height){
		setTextureDescriptor(textureDescriptor);
		setX(x);
		setY(y);
		setWidth(width);
		setHeight(height);
	}
	private void setTextureDescriptor(PrecedenceTextures textureDescriptor) {
		this.textureDescriptor=textureDescriptor;
	}
	private void setX(int x) {
		this.x=x;
	}
	private void setY(int y) {
		this.y=y;
	}
	private void setWidth(int width) {
		this.width=width;
	}
	private void setHeight(int height) {
		this.height=height;
	}
	@Override
	public PrecedenceTextures getTextureDescriptor() {
		return textureDescriptor;
	}

	@Override
	public int getX() {
		return x;
	}

	@Override
	public int getY() {
		return y;
	}

	@Override
	public int getWidth() {
		return width;
	}

	@Override
	public int getHeight() {
		return height;
	}

}
