package com.pdg.dungeondelver.hamquestengine.tables;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;
import com.badlogic.gdx.utils.XmlReader.Element;
import com.pdg.dungeondelver.hamquestengine.gameconstants.FileNames;
import com.pdg.dungeondelver.hamquestengine.gameconstants.Properties;
import com.pdg.dungeondelver.hamquestengine.item.Descriptor;
import com.pdg.dungeondelver.hamquestengine.item.IdentifiedPropertyValuePairList;
import com.pdg.dungeondelver.hamquestengine.item.PropertyValuePair;

public class TerrainTable
{
    private Map<String, Descriptor> table = new HashMap<String, Descriptor>();
    public Descriptor getTerrainDescriptor(String theMapTerrainIdentifier)
    {
        Descriptor result = null;
        if (table.containsKey(theMapTerrainIdentifier))
        {
            result = table.get(theMapTerrainIdentifier);
        }
        return (result);
    }
    public TerrainTable(String theFileName)
    {
    	if(theFileName==null || theFileName==""){
    		theFileName=FileNames.DefaultTerrainsFile;
    	}
        table.clear();
    	XmlReader reader = new XmlReader();
    	FileHandle handle = Gdx.files.internal(theFileName);
		try {
        	Element doc;
			doc = reader.parse(handle);
			Array<Element> subElements = doc.getChildByName(Properties.Terrains).getChildrenByName(Properties.Terrain);
			for(Element subElement:subElements){
				IdentifiedPropertyValuePairList properties = PropertyValuePair.loadPropertyValuesFromXmlNode(subElement);
				table.put(properties.identifier, new Descriptor(properties.result));
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}
