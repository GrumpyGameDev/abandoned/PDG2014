package com.pdg.dungeondelver.hamquestengine.maze;

import com.pdg.dungeondelver.hamquestengine.gameconstants.CellTypes;
import com.pdg.dungeondelver.hamquestengine.gameconstants.Properties;
import com.pdg.dungeondelver.hamquestengine.gamecore.Game;
import com.pdg.dungeondelver.hamquestengine.gamecore.IGameClient;
import com.pdg.dungeondelver.pdgboardgames.CountedCollection;
import com.pdg.dungeondelver.pdgboardgames.MazeCellInfoBase;
import com.pdg.dungeondelver.hamquestengine.maps.Creature;
import com.pdg.dungeondelver.hamquestengine.maps.Map;

public class CellInfo extends MazeCellInfoBase implements IGameClient
{
    private Game game;
    public Game getGame()
    {
            return game;
    }
    public void setGame(Game value)
        {
            game = value;
        }
    private String cellType = CellTypes.Passageway;
    public CountedCollection<String> Creatures = new CountedCollection<String>();
    public CountedCollection<String> Items = new CountedCollection<String>();
    public int VisitCount = 0;
    public Map Map = null;
    public String getCellType()
    {
            return (cellType);
    }
    public void setCellType(String value)
        {
            cellType = value;
        }
    public boolean isRoom()
    {
            return getCellType().compareToIgnoreCase(CellTypes.Room)==0;
    }
    public CellInfo()
    {
    }
    @Override
    public Object clone()
    {
        CellInfo result = new CellInfo();
        result.setCellType(this.getCellType());
        return (result);
    }
    public void addVisits(int theVisitCount)
    {
        while (theVisitCount > 0)
        {
            addVisit();
            theVisitCount--;
        }
    }
    public void addVisit()
    {
        wanderingMonsterCheck();
        VisitCount++;
    }
    private void wanderingMonsterCheck()
    {
        int roll = getGame().getRandomNumberGenerator().next(1, 6) + getGame().getRandomNumberGenerator().next(1, 6);
        if (roll <= VisitCount - 1)
        {
            VisitCount -= roll;
            Map map = Map;
            int mapColumn;
            int mapRow;
            String itemIdentifier;
            String terrainIdentifier;
            do
            {
                mapColumn = getGame().getRandomNumberGenerator().next(1, map.getColumns() - 1);
                mapRow = getGame().getRandomNumberGenerator().next(1, map.getRows() - 1);
                itemIdentifier = map.get(mapColumn).get(mapRow).getItemIdentifier();
                terrainIdentifier = map.get(mapColumn).get(mapRow).TerrainIdentifier;
            } while (itemIdentifier != "" || !getGame().getTableSet().getTerrainTable().getTerrainDescriptor(terrainIdentifier).<Boolean>getProperty(Properties.Passable) || map.get(mapColumn).get(mapRow).Creature != null);
            Creature creature = new Creature(getGame().getTableSet().getCreatureTable().generateCreature(Properties.SpawnWeight,getGame().getRandomNumberGenerator()), mapColumn, mapRow,getGame());
            creature.Map = map;
            map.getSummonedCreatures().add(creature);
            map.get(mapColumn).get(mapRow).Creature = creature;
        }
    }
}

