package com.pdg.dungeondelver.hamquestengine.descriptorproperties.misc;

import com.badlogic.gdx.utils.XmlReader.Element;
import com.pdg.dungeondelver.hamquestengine.item.CustomPropertyLoader;
import com.pdg.dungeondelver.pdgboardgames.IRandomNumberGenerator;
import com.pdg.dungeondelver.pdgboardgames.RandomNumberGenerator;

public class DefaultRandomNumberGeneratorLoader implements CustomPropertyLoader
{
    public IRandomNumberGenerator loadFromNode(Element node)
    {
        return new RandomNumberGenerator();
    }
}
