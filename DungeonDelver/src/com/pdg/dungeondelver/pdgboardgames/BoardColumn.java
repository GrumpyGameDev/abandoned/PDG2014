package com.pdg.dungeondelver.pdgboardgames;

import java.util.ArrayList;
import java.util.List;

public class BoardColumn<CellType extends BoardCellBase>
{
    private int column;
    private List<CellType> cells;
    public int getRows()
    {
            return (cells.size());
    }
    public CellType get(int row)
    {
            return (cells.get(row));
        }
    public void put(int row, CellType value)
        {
            cells.set(row,value);
            if (cells.get(row)!=null)
            {
            	cells.get(row).column = column;
            	cells.get(row).row = row;
            }
        }
    public BoardColumn(int theColumn,int theRows,BoardCellFactory<CellType> instantiator)
    {
        column = theColumn;
        cells = new ArrayList<CellType>();
        for (int row = 0; row < theRows; ++row)
        {
			cells.add(instantiator.newInstance());
        }
        this.clear();
    }
    public void clear()
    {
        for (CellType cell : cells)
        {
            cell.clear();
        }
    }
}
