package com.playdeezgames.splorr.tokenprocessors;

import java.util.Stack;
import com.playdeezgames.splorr.enums.Gatherables;
import com.playdeezgames.splorr.enums.Hands;
import com.playdeezgames.splorr.items.IItem;
import com.playdeezgames.splorr.world.Avatar;

public class GatherTokenProcessor implements ITokenProcessor {

	public boolean processTokens(Stack<String> tokenizer, Avatar avatar) {
		//gather (resource) [with (hand) hand]
		if(tokenizer.empty()){
			return false;//"gather" has no meaning by itself
		}else{
			//parse the gatherable
			Gatherables gatherable = Gatherables.parse(tokenizer.pop());
			if(gatherable==null){
				return false;
			}else{
				//we know what we are gathering
				Hands hand = null;
				if(tokenizer.empty()){
					hand = avatar.getEmptyHand();
					if(hand==null){
						System.out.println();
						System.out.println("Both hands are full.");
						return true;
					}
				}else{
					if(tokenizer.size()!=3){
						return false;
					}else{
						hand = CommonPhraseParsers.WithHandParser(tokenizer);
						if(hand==null){
							return false;
						}
					}
				}
				if(avatar.getHeldItem(hand)!=null){
					System.out.println();
					System.out.println("That hand is already full.");
					return true;
				}else{
					if(!gatherable.getGenerator().generate(avatar.getRandom())){
						System.out.println();
						System.out.println("You don't find any.");
						return true;
					}else{
						IItem item = gatherable.generateItem();
						System.out.println();
						System.out.printf("You gather %s.\n", item.getItem().getPlural());
						avatar.setHeldItem(hand, item);
						return true;
					}
				}
			}
		}
	}

}
