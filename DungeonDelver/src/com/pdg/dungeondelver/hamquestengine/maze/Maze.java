package com.pdg.dungeondelver.hamquestengine.maze;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;








import com.pdg.dungeondelver.hamquestengine.directions.Directions;
import com.pdg.dungeondelver.hamquestengine.gameconstants.CellTypes;
import com.pdg.dungeondelver.hamquestengine.gameconstants.Placements;
import com.pdg.dungeondelver.hamquestengine.gameconstants.Properties;
import com.pdg.dungeondelver.hamquestengine.gameconstants.PropertyGroups;
import com.pdg.dungeondelver.hamquestengine.gamecore.Game;
import com.pdg.dungeondelver.hamquestengine.gamecore.IGameClient;
import com.pdg.dungeondelver.hamquestengine.item.Descriptor;
import com.pdg.dungeondelver.pdgboardgames.MazeBase;
import com.pdg.dungeondelver.pdgboardgames.MazeCellBase;
import com.pdg.dungeondelver.pdgboardgames.MazeGenerationDelegate;
import com.pdg.dungeondelver.pdgboardgames.WeightedGenerator;
import com.pdg.dungeondelver.hamquestengine.creatures.PlayerDescriptor;
import com.pdg.dungeondelver.hamquestengine.maps.Map;
import com.pdg.dungeondelver.hamquestengine.maps.Creature;
import com.pdg.dungeondelver.hamquestengine.gameconstants.Tags;

public class Maze extends MazeBase<Directions,Portal,CellInfo> implements IGameClient, MazeGenerationDelegate
{
    private Game game;
    public Game getGame()
    {
            return game;
    }
    private PlayerDescriptor playerDescriptor;
    public PlayerDescriptor getPlayerDescriptor(){
    	return playerDescriptor;
    }
    public void setPlayerDescriptor(PlayerDescriptor value){
    	playerDescriptor=value;
    }
    private void populateMaze()
    {
        int column;
        int row;
        int direction;
        Directions directions = new Directions();
        java.util.Map<Integer, Integer> lockCounts = new HashMap<Integer, Integer>();
        List<MazeCellBase<Directions, Portal, CellInfo>> deadEndCells = new ArrayList<MazeCellBase<Directions, Portal, CellInfo>>();
        List<MazeCellBase<Directions, Portal, CellInfo>> nonDeadEndCells = new ArrayList<MazeCellBase<Directions, Portal, CellInfo>>();
        for (column = 0; column < getColumns(); ++column)
        {
            for (row = 0; row < getRows(); ++row)
            {
                MazeCellBase<Directions, Portal, CellInfo> cell = this.get(column).get(row);
                int openCount = 0;
                for (direction = 0; direction < directions.getCount(); ++direction)
                {
                    if (cell.getPortals().get(direction) != null && cell.getPortals().get(direction).Open)
                    {
                        openCount++;
                    }
                }
                int total = getGame().getTableSet().getPropertyGroupTable().getPropertyDescriptor("roomChance").<Integer>getProperty("total");
                int chance = getGame().getTableSet().getPropertyGroupTable().getPropertyDescriptor("roomChance").<Integer>getProperty(String.format("doorCount%1", openCount));
                if (getGame().getRandomNumberGenerator().next(total) < chance)
                {
                    cell.getCellInfo().setCellType(CellTypes.Room);
                }
                else
                {
                    cell.getCellInfo().setCellType(CellTypes.Passageway);
                }
                if (openCount == 1)
                {
                    deadEndCells.add(cell);
                    for (direction = 0; direction < directions.getCount(); ++direction)
                    {
                        if (cell.getPortals().get(direction) != null && cell.getPortals().get(direction).Open)
                        {
                            cell.getPortals().get(direction).setLocked(true);
                            cell.getPortals().get(direction).setLockType(getGame().getTableSet().getItemTable().LockTypeGenerator.generate(getGame().getRandomNumberGenerator()));
                            if (lockCounts.containsKey(cell.getPortals().get(direction).getLockType()))
                            {
                            	lockCounts.put(cell.getPortals().get(direction).getLockType(), lockCounts.get(cell.getPortals().get(direction).getLockType())+1);
                            }
                            else
                            {
                            	lockCounts.put(cell.getPortals().get(direction).getLockType(), 1);
                            }
                        }
                    }
                }
                else
                {
                    nonDeadEndCells.add(cell);
                }
                cell.getCellInfo().setGame(getGame());
                cell.getCellInfo().Map=(new Map(getGame()));
                cell.getCellInfo().Map.generateTerrain(cell);
            }
        }
        for (MazeCellBase<Directions, Portal, CellInfo> cell : deadEndCells)
        {
            for (direction = 0; direction < directions.getCount(); ++direction)
            {
                Portal portal = cell.getPortals().get(direction);
                if (portal != null && portal.Open && portal.isLocked())
                {
                    cell.getNeighbors().get(direction).getCellInfo().Items.add(getGame().getTableSet().getItemTable().getDoorItemIdentifier(portal.getLockType(), directions.getOpposite(direction)));
                }
            }
        }
        for(int lockType : lockCounts.keySet())
        {
            int count = lockCounts.get(lockType);
            while (count > 0)
            {
                count--;
                MazeCellBase<Directions, Portal, CellInfo> cell = nonDeadEndCells.get(getGame().getRandomNumberGenerator().next(nonDeadEndCells.size()));
                cell.getCellInfo().Items.add(getGame().getTableSet().getItemTable().getKeyItemIdentifier(lockType));
            }
        }
        for (String itemIdentifier : getGame().getTableSet().getItemTable().getItemIdentifiers())
        {
            Descriptor itemDescriptor = getGame().getTableSet().getItemTable().getItemDescriptor(itemIdentifier);
            if (itemDescriptor == null) continue;
            if (!itemDescriptor.hasProperty(Properties.ItemCount)) continue;
            int count = itemDescriptor.<Integer>getProperty(Properties.ItemCount);
            while (count > 0)
            {
                count--;
                String placements = itemDescriptor.<String>getProperty(Properties.Placement);
                if (placements == Placements.Either)
                {
                    column = getGame().getRandomNumberGenerator().next(getColumns());
                    row = getGame().getRandomNumberGenerator().next(getRows());
                    this.get(column).get(row).getCellInfo().Items.add(itemIdentifier);
                }
                else
                    if (placements == Placements.DeadEnd)
                    {
                        deadEndCells.get(getGame().getRandomNumberGenerator().next(deadEndCells.size())).getCellInfo().Items.add(itemIdentifier);
                    }
                    else
                        if (placements == Placements.NonDeadEnd)
                        {
                            nonDeadEndCells.get(getGame().getRandomNumberGenerator().next(nonDeadEndCells.size())).getCellInfo().Items.add(itemIdentifier);
                        }
            }
        }
        for (String creatureIdentifier : getGame().getTableSet().getCreatureTable().getCreatureIdentifiers())
        {
            Descriptor creatureDescriptor = getGame().getTableSet().getCreatureTable().getCreatureDescriptor(creatureIdentifier);
            if (creatureDescriptor == null) continue;
            int count = creatureDescriptor.<Integer>getProperty(Properties.CreatureCount);
            while (count > 0)
            {
                count--;
                String placements = creatureDescriptor.<String>getProperty(Properties.Placement);
                if (placements == Placements.Either)
                {
                    column = getGame().getRandomNumberGenerator().next(getColumns());
                    row = getGame().getRandomNumberGenerator().next(getRows());
                    this.get(column).get(row).getCellInfo().Creatures.add(creatureIdentifier);
                }
                else
                    if (placements == Placements.DeadEnd)
                    {
                        deadEndCells.get(getGame().getRandomNumberGenerator().next(deadEndCells.size())).getCellInfo().Creatures.add(creatureIdentifier);
                    }
                    else
                        if (placements == Placements.NonDeadEnd)
                        {
                            nonDeadEndCells.get(getGame().getRandomNumberGenerator().next(nonDeadEndCells.size())).getCellInfo().Creatures.add(creatureIdentifier);
                        }
            }
        }
        do
        {
            column = getGame().getRandomNumberGenerator().next(getColumns());
            row = getGame().getRandomNumberGenerator().next(getRows());
        } while (deadEndCells.indexOf(this.get(column).get(row)) != -1);
        this.get(column).get(row).getCellInfo().Creatures.add("player");
        this.get(column).get(row).getCellInfo().VisitCount++;
        getPlayerDescriptor().MazeColumn = column;
        getPlayerDescriptor().MazeRow = row;
        for (String identifier : getGame().getTableSet().getItemTable().getItemIdentifiers())
        {
            Descriptor descriptor = getGame().getTableSet().getItemTable().getItemDescriptor(identifier);
            if(descriptor == null) continue;
            if (descriptor.hasProperty(Properties.InitialInventory))
            {
                int count = descriptor.<WeightedGenerator<Integer>>getProperty(Properties.InitialInventory).generate(getGame().getRandomNumberGenerator());
                while (count > 0)
                {
                    getPlayerDescriptor().addItem(identifier, false);
                    count--;
                }
            }
        }
        getPlayerDescriptor().autoEquip();
        for (column = 0; column < getColumns(); ++column)
        {
            for (row = 0; row < getRows(); ++row)
            {
                this.get(column).get(row).getCellInfo().Map.generate(this.get(column).get(row));
            }
        }
        getPlayerDescriptor().MapCreature = Map.getPlayerCreature();
        getPlayerDescriptor().updatePathfinding();
        getPlayerDescriptor().revealTraps();
    }
    public Maze(Game theGame)
    {
    	super(
    			theGame.getTableSet().getPropertyGroupTable().getPropertyDescriptor(PropertyGroups.MazeDimensions).<Integer>getProperty(Properties.Columns), 
    			theGame.getTableSet().getPropertyGroupTable().getPropertyDescriptor(PropertyGroups.MazeDimensions).<Integer>getProperty(Properties.Rows), 
    			new MazeCellFactory(), 
    			new CellInfoFactory(), 
    			new MazeDirectionsFactory(), 
    			new MazePortalFactory()
    			);
    	game  = theGame;
        setPlayerDescriptor((PlayerDescriptor)getGame().getTableSet().getCreatureTable().getCreatureDescriptor("player"));
        getPlayerDescriptor().Maze = this;
        OnPostGenerate = this;
    }
    public void respawnItem(String itemIdentifier)
    {
        Descriptor descriptor = getGame().getTableSet().getItemTable().getItemDescriptor(itemIdentifier);
        if (descriptor.<String>getProperty(Properties.Placement).compareToIgnoreCase(Placements.None)==0) return;
        int mazeRow;
        int mazeColumn;
        boolean done = false;
        while (!done)
        {
            mazeRow = getGame().getRandomNumberGenerator().next(getRows());
            mazeColumn = getGame().getRandomNumberGenerator().next(getColumns());
            int doors = 0;
            for (int direction = Directions.North; direction <= Directions.West; ++direction)
            {
                if (this.get(mazeColumn).get(mazeRow).getNeighbors().get(direction) != null && this.get(mazeColumn).get(mazeRow).getPortals().get(direction).Open)
                {
                    doors++;
                }
            }
            if (descriptor.<String>getProperty(Properties.Placement).compareToIgnoreCase(Placements.DeadEnd)==0 && doors != 1)
            {
                done = false;
                continue;
            }
            if (descriptor.<String>getProperty(Properties.Placement).compareToIgnoreCase(Placements.NonDeadEnd)==0 && doors == 1)
            {
                done = false;
                continue;
            }
            int mapColumn;
            int mapRow;
            done = false;
            for (mapColumn = 0; !done && mapColumn < this.get(mazeColumn).get(mazeRow).getCellInfo().Map.getColumns(); ++mapColumn)
            {
                for (mapRow = 0; !done && mapRow < this.get(mazeColumn).get(mazeRow).getCellInfo().Map.getRows(); ++mapRow)
                {
                    if (
                        (getGame().getTableSet().getTerrainTable().getTerrainDescriptor(this.get(mazeColumn).get(mazeRow).getCellInfo().Map.get(mapColumn).get(mapRow).TerrainIdentifier).<Boolean>getProperty(Properties.Passable)) &&
                        (this.get(mazeColumn).get(mazeRow).getCellInfo().Map.get(mapColumn).get(mapRow).getItemIdentifier() == "") &&
                        (this.get(mazeColumn).get(mazeRow).getCellInfo().Map.get(mapColumn).get(mapRow).Creature == null)
                        )
                    {
                        done = true;
                    }
                }
            }
            if (!done) continue;
            do
            {
                mapColumn = getGame().getRandomNumberGenerator().next(this.get(mazeColumn).get(mazeRow).getCellInfo().Map.getColumns());
                mapRow = getGame().getRandomNumberGenerator().next(this.get(mazeColumn).get(mazeRow).getCellInfo().Map.getRows());
            }
            while (
                (!getGame().getTableSet().getTerrainTable().getTerrainDescriptor(this.get(mazeColumn).get(mazeRow).getCellInfo().Map.get(mapColumn).get(mapRow).TerrainIdentifier).<Boolean>getProperty(Properties.Passable)) ||
                (this.get(mazeColumn).get(mazeRow).getCellInfo().Map.get(mapColumn).get(mapRow).getItemIdentifier() != "") ||
                (this.get(mazeColumn).get(mazeRow).getCellInfo().Map.get(mapColumn).get(mapRow).Creature != null)
            );
            this.get(mazeColumn).get(mazeRow).getCellInfo().Map.get(mapColumn).get(mapRow).setItemIdentifier(itemIdentifier);
        }
    }
    public void spawnMonsters(float fraction)
    {
        int spawnCount = 0;
        int column;
        int row;
        for (column = 0; column < getColumns(); ++column)
        {
            for (row = 0; row < getRows(); ++row)
            {
                if (this.get(column).get(row).getCellInfo().VisitCount>0)
                {
                    spawnCount += this.get(column).get(row).getCellInfo().Map.getSpawnCount();
                }
            }
        }
        spawnCount = (int)((float)spawnCount * fraction);
        while (spawnCount > 0)
        {
            column = getGame().getRandomNumberGenerator().next(getColumns());
            row = getGame().getRandomNumberGenerator().next(getRows());
            if (this.get(column).get(row).getCellInfo().VisitCount > 0 && this.get(column).get(row).getCellInfo().Map.hasTrap())
            {
                Map map = this.get(column).get(row).getCellInfo().Map;
                int mapColumn;
                int mapRow;
                String itemIdentifier;
                do
                {
                    mapColumn = getGame().getRandomNumberGenerator().next(map.getColumns());
                    mapRow = getGame().getRandomNumberGenerator().next(map.getRows());
                    itemIdentifier = map.get(mapColumn).get(mapRow).getItemIdentifier();
                } while (itemIdentifier == "" || !getGame().getTableSet().getItemTable().getItemDescriptor(itemIdentifier).hasTag(Tags.Spawns));
                map.get(mapColumn).get(mapRow).setItemIdentifier("");
                this.get(column).get(row).getCellInfo().Items.remove(itemIdentifier);
                Creature creature = new Creature(getGame().getTableSet().getCreatureTable().generateCreature(Properties.SpawnWeight,getGame().getRandomNumberGenerator()), mapColumn, mapRow, getGame());
                creature.Map = map;
                map.getCreatures().add(creature);
                map.get(mapColumn).get(mapRow).Creature = creature;
                spawnCount--;
            }
        }
    }
	@Override
	public void PostGenerate() {
		this.populateMaze();
	}
}

