package com.pdg.dungeondelver.hamquestengine.descriptorproperties.playerspecific;

public interface PlayerStatisticHolderDelegate {
	int get();
}
