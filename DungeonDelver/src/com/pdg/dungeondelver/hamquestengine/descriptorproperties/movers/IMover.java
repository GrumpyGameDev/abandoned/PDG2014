package com.pdg.dungeondelver.hamquestengine.descriptorproperties.movers;

import com.pdg.dungeondelver.hamquestengine.gamecore.Game;
import com.pdg.dungeondelver.hamquestengine.maps.Creature;

public interface IMover {
	DoMoveResult doMove(Creature theCreature,int column,int row, Game theGame);
}
