package com.pdg.dungeondelver.pdgboardgames;

public abstract class BoardCellBase
{
    public int column;
    public int row;
    public abstract void clear();
}
