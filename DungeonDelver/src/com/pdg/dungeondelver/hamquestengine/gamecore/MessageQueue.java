package com.pdg.dungeondelver.hamquestengine.gamecore;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

import com.badlogic.gdx.graphics.Color;
import com.pdg.dungeondelver.hamquestengine.item.Descriptor;

public class MessageQueue extends GameClientBase
{
    private List<MessageQueueCallBack> callBacks=new ArrayList<MessageQueueCallBack>();
    private Queue<String> messages = new ArrayDeque<String>();
    private Queue<Color> colors = new ArrayDeque<Color>();
    private int maximumMessages = 0;
    public List<MessageQueueCallBack> getCallBacks()
    {
    	return callBacks;
    }
    public void addMessage(String message,Object... theParams)
    {
        String colorName = com.pdg.dungeondelver.hamquestengine.gameconstants.Colors.Default;
        Descriptor messageDescriptor = getGame().getTableSet().getMessageTable().getMessageDescriptor(message);
        if (messageDescriptor != null)
        {
            colorName = messageDescriptor.<String>getProperty(com.pdg.dungeondelver.hamquestengine.gameconstants.Properties.TextColor);
        }
        Color color = getGame().getTableSet().getPropertyGroupTable().getPropertyDescriptor(com.pdg.dungeondelver.hamquestengine.gameconstants.PropertyGroups.ColorTable).<Color>getProperty(colorName);
        colors.add(color);
        String translatedMessage = getGame().getTableSet().getMessageTable().translateMessageText(message);
        if (theParams.length > 0)
        {
            translatedMessage = String.format(translatedMessage, theParams);
        }
        messages.add(translatedMessage);
        for(MessageQueueCallBack callback:getCallBacks()){
        	callback.handle(message, color);
        }
        if (messages.size() > maximumMessages)
        {
            messages.remove();
            colors.remove();
        }
    }
    public Color[] getColors()
    {
            return (Color[])colors.toArray();
    }
    public String[] getMessages()
    {
            return (String[])(messages.toArray());
    }
    public MessageQueue(int theMaximumMessages,Game theGame)
    {
    	super(theGame);
        maximumMessages = theMaximumMessages;
    }
}
