package com.playdeezgames.splorr.enums;

public enum Items {
	MOSS("moss","moss"),
	GRASS("grass","grass"),
	FLOWERS("flower","flowers"),
	BERRIES("berry","berries"),
	LEAVES("leaf","leaves"),
	STICKS("stick","sticks");
	private String singular;
	private String plural;
	Items(String singular, String plural){
		this.singular = singular;
		this.plural = plural;
	}
	public String getSingular() {
		return singular;
	}
	public String getPlural(){
		return plural;
	}
	public boolean isToken(String token){
		return token.compareToIgnoreCase(getSingular())==0 || token.compareToIgnoreCase(getPlural())==0; 
	}
	public static Items parse(String token){
		for(Items item: Items.values()){
			if(item.isToken(token)){
				return item;
			}
		}
		return null;
	}
}
