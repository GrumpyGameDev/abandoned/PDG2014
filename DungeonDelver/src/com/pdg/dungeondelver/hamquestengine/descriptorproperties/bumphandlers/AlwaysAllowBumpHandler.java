package com.pdg.dungeondelver.hamquestengine.descriptorproperties.bumphandlers;

import com.pdg.dungeondelver.hamquestengine.item.Descriptor;
import com.pdg.dungeondelver.hamquestengine.maps.Creature;

public class AlwaysAllowBumpHandler implements IBumpHandler
{

    public BumpResult bump(Descriptor theDescriptor, Creature theCreature)
    {
        return BumpResult.Allow;
    }
}
