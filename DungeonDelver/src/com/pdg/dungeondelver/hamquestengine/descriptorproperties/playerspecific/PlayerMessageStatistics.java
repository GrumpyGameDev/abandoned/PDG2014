package com.pdg.dungeondelver.hamquestengine.descriptorproperties.playerspecific;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class PlayerMessageStatistics implements IMessageStatistics
{
    private Map<String,Integer> table = new HashMap<String,Integer>();
    public Set<String> getEntries()
    {
        return table.keySet();
    }

    public void addEntry(String entry)
    {
        if (table.containsKey(entry))
        {
            table.put(entry,table.get(entry)+1);
        }
        else
        {
            table.put(entry,1);
        }
    }

    public int getCount(String entry)
    {
        if (table.containsKey(entry))
        {
            return table.get(entry);
        }
        else
        {
            return 0;
        }
    }
}
