package com.playdeezgames.splorr.enums;

public enum Directions {
	NORTH,
	EAST,
	SOUTH,
	WEST;
	public Directions left(){
		switch(this){
		case NORTH:
			return WEST;
		case EAST:
			return NORTH;
		case SOUTH:
			return EAST;
		case WEST:
			return SOUTH;
		default:
			return this;
		}
	}
	public Directions right(){
		switch(this){
		case NORTH:
			return EAST;
		case EAST:
			return SOUTH;
		case SOUTH:
			return WEST;
		case WEST:
			return NORTH;
		default:
			return this;
		}
	}
	public Directions opposite(){
		switch(this){
		case NORTH:
			return SOUTH;
		case EAST:
			return WEST;
		case SOUTH:
			return NORTH;
		case WEST:
			return EAST;
		default:
			return this;
		}
	}
	public int nextColumn(int column, int row){
		switch(this){
		case EAST:
			return column+1;
		case WEST:
			return column-1;
		default:
			return column;
		}
	}
	public int nextRow(int column,int row){
		switch(this){
		case NORTH:
			return row-1;
		case SOUTH:
			return row+1;
		default:
			return row;
		}
	}
}
