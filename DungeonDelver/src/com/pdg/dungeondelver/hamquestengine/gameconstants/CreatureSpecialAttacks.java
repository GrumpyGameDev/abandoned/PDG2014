package com.pdg.dungeondelver.hamquestengine.gameconstants;

public class CreatureSpecialAttacks {
    public final String Generator = "Generator";
    public final String Ghoul = "Ghoul";
    public final String None = "None";
    public final String RedDragon = "RedDragon";
    public final String Teleport = "Teleport";
    public final String Thief = "Thief";
}
