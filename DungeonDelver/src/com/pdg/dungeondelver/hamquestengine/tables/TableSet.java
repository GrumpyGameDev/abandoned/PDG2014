package com.pdg.dungeondelver.hamquestengine.tables;

public class TableSet
{
    private CreatureTable creatureTable;
    public CreatureTable getCreatureTable()
    {
            return creatureTable;
    }
    private MessageTable messageTable;
    public MessageTable getMessageTable()
    {
            return messageTable;
    }
    private PropertyGroupTable propertyGroupTable;
    public PropertyGroupTable getPropertyGroupTable()
    {
            return propertyGroupTable;
    }
    private TerrainTable terrainTable;
    public TerrainTable getTerrainTable()
    {
            return terrainTable;
    }
    private ItemTable itemTable;
    public ItemTable getItemTable()
    {
            return itemTable;
    }
    public TableSet(CreatureTable theCreatureTable,MessageTable theMessageTable,PropertyGroupTable thePropertyGroupTable,TerrainTable theTerrainTable,ItemTable theItemTable)
    {
        creatureTable = theCreatureTable;
        messageTable = theMessageTable;
        propertyGroupTable = thePropertyGroupTable;
        terrainTable = theTerrainTable;
        itemTable = theItemTable;
    }
}
