package com.playdeezgames.precedence.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.playdeezgames.common.statemachine.State;
import com.playdeezgames.common.statemachine.StateMachine;
import com.playdeezgames.precedence.PrecedenceStates;
import com.playdeezgames.precedence.PrecedenceTextureRegions;
import com.playdeezgames.precedence.ResourceManager;

public class MainMenuState extends StateBase<PrecedenceStates> implements State{
	
	private VerticalGroup verticalGroup;
	private Button startButton;
	private Button instructionsButton;
	private Button aboutButton;
	private Button optionsButton;
	private Button quitButton;
	
	public MainMenuState(StateMachine<PrecedenceStates> stateMachine, ResourceManager resourceManager) {
		super(stateMachine,resourceManager);
	}

	@Override
	public void dispose() {
	}

	@Override
	public void render() {
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		
		getResourceManager().getStage().act(Gdx.graphics.getDeltaTime());
		getResourceManager().getStage().draw();
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void create() {
		Stage stage = getResourceManager().getStage();
		stage.addActor(getVerticalGroup());
		getVerticalGroup().addAction(Actions.alpha(0));
	}

	private VerticalGroup getVerticalGroup() {
		if(verticalGroup==null){
			Stage stage = getResourceManager().getStage();
			verticalGroup=new VerticalGroup();
			verticalGroup.addActor(getStartButton());
			verticalGroup.addActor(getInstructionsButton());
			verticalGroup.addActor(getAboutButton());
			verticalGroup.addActor(getOptionsButton());
			verticalGroup.addActor(getQuitButton());
			verticalGroup.setBounds(stage.getWidth()/2-verticalGroup.getPrefWidth()/2, stage.getHeight()/2-verticalGroup.getPrefHeight()/2, verticalGroup.getPrefWidth(), verticalGroup.getPrefHeight());
		}
		return verticalGroup;
	}

	private Button getQuitButton() {
		if(quitButton==null){
			Button.ButtonStyle style= new Button.ButtonStyle();
			style.up=new TextureRegionDrawable(getResourceManager().getTextureRegionManager().getTextureRegion(PrecedenceTextureRegions.MAIN_MENU_QUIT_NORMAL));
			style.down=new TextureRegionDrawable(getResourceManager().getTextureRegionManager().getTextureRegion(PrecedenceTextureRegions.MAIN_MENU_QUIT_HILITE));
			quitButton=new Button(style);
			quitButton.setTouchable(Touchable.enabled);
		}
		return quitButton;
	}

	private Button getOptionsButton() {
		if(optionsButton==null){
			Button.ButtonStyle style= new Button.ButtonStyle();
			style.up=new TextureRegionDrawable(getResourceManager().getTextureRegionManager().getTextureRegion(PrecedenceTextureRegions.MAIN_MENU_OPTIONS_NORMAL));
			style.down=new TextureRegionDrawable(getResourceManager().getTextureRegionManager().getTextureRegion(PrecedenceTextureRegions.MAIN_MENU_OPTIONS_HILITE));
			optionsButton=new Button(style);
			optionsButton.setTouchable(Touchable.enabled);
		}
		return optionsButton;
	}

	private Button getAboutButton() {
		if(aboutButton==null){
			Button.ButtonStyle style= new Button.ButtonStyle();
			style.up=new TextureRegionDrawable(getResourceManager().getTextureRegionManager().getTextureRegion(PrecedenceTextureRegions.MAIN_MENU_ABOUT_NORMAL));
			style.down=new TextureRegionDrawable(getResourceManager().getTextureRegionManager().getTextureRegion(PrecedenceTextureRegions.MAIN_MENU_ABOUT_HILITE));
			aboutButton=new Button(style);
			aboutButton.setTouchable(Touchable.enabled);
		}
		return aboutButton;
	}

	private Button getInstructionsButton() {
		if(instructionsButton==null){
			Button.ButtonStyle style= new Button.ButtonStyle();
			style.up=new TextureRegionDrawable(getResourceManager().getTextureRegionManager().getTextureRegion(PrecedenceTextureRegions.MAIN_MENU_INSTRUCTIONS_NORMAL));
			style.down=new TextureRegionDrawable(getResourceManager().getTextureRegionManager().getTextureRegion(PrecedenceTextureRegions.MAIN_MENU_INSTRUCTIONS_HILITE));
			instructionsButton=new Button(style);
			instructionsButton.setTouchable(Touchable.enabled);
		}
		return instructionsButton;
	}

	private Button getStartButton() {
		if(startButton==null){
			Button.ButtonStyle style= new Button.ButtonStyle();
			style.up=new TextureRegionDrawable(getResourceManager().getTextureRegionManager().getTextureRegion(PrecedenceTextureRegions.MAIN_MENU_START_NORMAL));
			style.down=new TextureRegionDrawable(getResourceManager().getTextureRegionManager().getTextureRegion(PrecedenceTextureRegions.MAIN_MENU_START_HILITE));
			startButton=new Button(style);
			startButton.setTouchable(Touchable.enabled);
		}
		return startButton;
	}

	@Override
	public void finish() {
		getVerticalGroup().addAction(Actions.alpha(0,1));
		disableControls();
	}

	private void disableControls() {
		getStartButton().setDisabled(true);
		getInstructionsButton().setDisabled(true);
		getAboutButton().setDisabled(true);
		getOptionsButton().setDisabled(true);
		getQuitButton().setDisabled(true);
	}

	@Override
	public void start() {
		getVerticalGroup().addAction(Actions.alpha(1,1));
		enableControls();
	}

	private void enableControls() {
		getStartButton().setDisabled(false);
		getInstructionsButton().setDisabled(false);
		getAboutButton().setDisabled(false);
		getOptionsButton().setDisabled(false);
		getQuitButton().setDisabled(false);
	}

	@Override
	public boolean handle(Event event) {
		if(event instanceof ChangeListener.ChangeEvent){
			ChangeListener.ChangeEvent changeEvent=(ChangeListener.ChangeEvent)event;
			if(changeEvent.getTarget()==getStartButton()){
				System.out.println("Start!");
				return true;
			}else if(changeEvent.getTarget()==getInstructionsButton()){
				System.out.println("Instructions!");
				return true;
			}else if(changeEvent.getTarget()==getAboutButton()){
				System.out.println("About!");
				return true;
			}else if(changeEvent.getTarget()==getOptionsButton()){
				System.out.println("Options!");
				return true;
			}else if(changeEvent.getTarget()==getQuitButton()){
				System.out.println("Quit!");
				return true;
			}
		}
		return false;
	}

}
