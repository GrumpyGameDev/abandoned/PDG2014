package com.pdg.dungeondelver.pdgboardgames;

import java.util.ArrayList;
import java.util.List;

public class Board<CellType extends BoardCellBase>
{
    private List<BoardColumn<CellType>> columns;
    public int getColumns()
    {
    	return (columns.size());
    }
    public int getRows()
    {
        return (columns.get(0).getRows());
    }
    public BoardColumn<CellType> get(int column)
    {
        return (columns.get(column));
    }
    public Board(int theColumns, int theRows,BoardCellFactory<CellType> instantiator)
    {
        columns = new ArrayList<BoardColumn<CellType>>();
        for (int column = 0; column < theColumns; ++column)
        {
            columns.add(new BoardColumn<CellType>(column, theRows,instantiator));
        }
        clear();
    }
    public void clear()
    {
        for (BoardColumn<CellType> column : columns)
        {
            column.clear();
        }
    }
}
