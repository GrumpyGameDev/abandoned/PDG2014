package com.playdeezgames.precedence;

import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.playdeezgames.common.interfaces.TextureDescriptor;

public enum PrecedenceTextures implements TextureDescriptor<PrecedenceFiles>{
	ONE(PrecedenceFiles.ONE,TextureFilter.Linear,TextureFilter.Linear),
	TWO(PrecedenceFiles.TWO,TextureFilter.Linear,TextureFilter.Linear),
	THREE(PrecedenceFiles.THREE,TextureFilter.Linear,TextureFilter.Linear),
	FOUR(PrecedenceFiles.FOUR,TextureFilter.Linear,TextureFilter.Linear),
	FIVE(PrecedenceFiles.FIVE,TextureFilter.Linear,TextureFilter.Linear),
	SIX(PrecedenceFiles.SIX,TextureFilter.Linear,TextureFilter.Linear),
	RED_CIRCLE(PrecedenceFiles.RED_CIRCLE,TextureFilter.Linear,TextureFilter.Linear),
	YELLOW_CIRCLE(PrecedenceFiles.YELLOW_CIRCLE,TextureFilter.Linear,TextureFilter.Linear),
	GREEN_CIRCLE(PrecedenceFiles.GREEN_CIRCLE,TextureFilter.Linear,TextureFilter.Linear),
	CYAN_CIRCLE(PrecedenceFiles.CYAN_CIRCLE,TextureFilter.Linear,TextureFilter.Linear),
	BLUE_CIRCLE(PrecedenceFiles.BLUE_CIRCLE,TextureFilter.Linear,TextureFilter.Linear),
	MAGENTA_CIRCLE(PrecedenceFiles.MAGENTA_CIRCLE,TextureFilter.Linear,TextureFilter.Linear),
	RED_TRIANGLE(PrecedenceFiles.RED_TRIANGLE,TextureFilter.Linear,TextureFilter.Linear),
	YELLOW_TRIANGLE(PrecedenceFiles.YELLOW_TRIANGLE,TextureFilter.Linear,TextureFilter.Linear),
	GREEN_TRIANGLE(PrecedenceFiles.GREEN_TRIANGLE,TextureFilter.Linear,TextureFilter.Linear),
	CYAN_TRIANGLE(PrecedenceFiles.CYAN_TRIANGLE,TextureFilter.Linear,TextureFilter.Linear),
	BLUE_TRIANGLE(PrecedenceFiles.BLUE_TRIANGLE,TextureFilter.Linear,TextureFilter.Linear),
	MAGENTA_TRIANGLE(PrecedenceFiles.MAGENTA_TRIANGLE,TextureFilter.Linear,TextureFilter.Linear),
	RED_SQUARE(PrecedenceFiles.RED_SQUARE,TextureFilter.Linear,TextureFilter.Linear),
	YELLOW_SQUARE(PrecedenceFiles.YELLOW_SQUARE,TextureFilter.Linear,TextureFilter.Linear),
	GREEN_SQUARE(PrecedenceFiles.GREEN_SQUARE,TextureFilter.Linear,TextureFilter.Linear),
	CYAN_SQUARE(PrecedenceFiles.CYAN_SQUARE,TextureFilter.Linear,TextureFilter.Linear),
	BLUE_SQUARE(PrecedenceFiles.BLUE_SQUARE,TextureFilter.Linear,TextureFilter.Linear),
	MAGENTA_SQUARE(PrecedenceFiles.MAGENTA_SQUARE,TextureFilter.Linear,TextureFilter.Linear),
	RED_PENTAGON(PrecedenceFiles.RED_PENTAGON,TextureFilter.Linear,TextureFilter.Linear),
	YELLOW_PENTAGON(PrecedenceFiles.YELLOW_PENTAGON,TextureFilter.Linear,TextureFilter.Linear),
	GREEN_PENTAGON(PrecedenceFiles.GREEN_PENTAGON,TextureFilter.Linear,TextureFilter.Linear),
	CYAN_PENTAGON(PrecedenceFiles.CYAN_PENTAGON,TextureFilter.Linear,TextureFilter.Linear),
	BLUE_PENTAGON(PrecedenceFiles.BLUE_PENTAGON,TextureFilter.Linear,TextureFilter.Linear),
	MAGENTA_PENTAGON(PrecedenceFiles.MAGENTA_PENTAGON,TextureFilter.Linear,TextureFilter.Linear),
	RED_HEXAGON(PrecedenceFiles.RED_HEXAGON,TextureFilter.Linear,TextureFilter.Linear),
	YELLOW_HEXAGON(PrecedenceFiles.YELLOW_HEXAGON,TextureFilter.Linear,TextureFilter.Linear),
	GREEN_HEXAGON(PrecedenceFiles.GREEN_HEXAGON,TextureFilter.Linear,TextureFilter.Linear),
	CYAN_HEXAGON(PrecedenceFiles.CYAN_HEXAGON,TextureFilter.Linear,TextureFilter.Linear),
	BLUE_HEXAGON(PrecedenceFiles.BLUE_HEXAGON,TextureFilter.Linear,TextureFilter.Linear),
	MAGENTA_HEXAGON(PrecedenceFiles.MAGENTA_HEXAGON,TextureFilter.Linear,TextureFilter.Linear),
	RED_OCTAGON(PrecedenceFiles.RED_OCTAGON,TextureFilter.Linear,TextureFilter.Linear),
	YELLOW_OCTAGON(PrecedenceFiles.YELLOW_OCTAGON,TextureFilter.Linear,TextureFilter.Linear),
	GREEN_OCTAGON(PrecedenceFiles.GREEN_OCTAGON,TextureFilter.Linear,TextureFilter.Linear),
	CYAN_OCTAGON(PrecedenceFiles.CYAN_OCTAGON,TextureFilter.Linear,TextureFilter.Linear),
	BLUE_OCTAGON(PrecedenceFiles.BLUE_OCTAGON,TextureFilter.Linear,TextureFilter.Linear),
	MAGENTA_OCTAGON(PrecedenceFiles.MAGENTA_OCTAGON,TextureFilter.Linear,TextureFilter.Linear),
	MAIN_MENU(PrecedenceFiles.MAIN_MENU,TextureFilter.Linear,TextureFilter.Linear),
	SPLASH(PrecedenceFiles.SPLASH,TextureFilter.Linear,TextureFilter.Linear);
	private PrecedenceFiles fileDescriptor;
	private TextureFilter minFilter;
	private TextureFilter magFilter;
	PrecedenceTextures(PrecedenceFiles fileDescriptor, TextureFilter minFilter,TextureFilter magFilter){
		setFileDescriptor(fileDescriptor);
		setMinFilter(minFilter);
		setMagFilter(magFilter);
	}
	private void setMagFilter(TextureFilter magFilter) {
		this.magFilter=magFilter;
	}
	private void setMinFilter(TextureFilter minFilter) {
		this.minFilter=minFilter;
	}
	private void setFileDescriptor(PrecedenceFiles fileDescriptor) {
		this.fileDescriptor=fileDescriptor;
	}
	@Override
	public TextureFilter getMinFilter() {
		return minFilter;
	}

	@Override
	public TextureFilter getMagFilter() {
		return magFilter;
	}
	@Override
	public PrecedenceFiles getFileDescriptor() {
		return fileDescriptor;
	}

}
