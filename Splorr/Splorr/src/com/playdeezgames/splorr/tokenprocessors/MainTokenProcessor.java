package com.playdeezgames.splorr.tokenprocessors;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;
import com.playdeezgames.splorr.world.Avatar;

public class MainTokenProcessor implements ITokenProcessor {
	private static final String QUIT = "quit";
	private static final String TURN = "turn";
	private static final String GO = "go";
	private static final String SAVE = "save";
	private static final String LOGIN = "login";
	private static final String GATHER = "gather";
	private static final String DROP = "drop";
	
	private Map<String,ITokenProcessor> processors = new HashMap<String,ITokenProcessor>();
	
	public MainTokenProcessor(){
		processors.put(QUIT, new QuitTokenProcessor());
		processors.put(TURN, new TurnTokenProcessor());
		processors.put(GO, new GoTokenProcessor());
		processors.put(SAVE, new SaveTokenProcessor());
		processors.put(GATHER,  new GatherTokenProcessor());
		processors.put(DROP, new DropTokenProcessor());
		
		processors.put(LOGIN, new LoginTokenProcessor());
	}

	public boolean processTokens(Stack<String> tokenizer, Avatar avatar) {
		if(!tokenizer.empty()){
			String command = tokenizer.pop().toLowerCase();
			ITokenProcessor processor = processors.get(command);
			if(processor!=null){
				return processor.processTokens(tokenizer, avatar);
			}else{
				return false;
			}
		}
		return false;
	}

}
