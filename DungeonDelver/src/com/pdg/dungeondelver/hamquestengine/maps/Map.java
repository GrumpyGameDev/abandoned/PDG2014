package com.pdg.dungeondelver.hamquestengine.maps;

import java.util.ArrayList;
import java.util.List;

import com.pdg.dungeondelver.hamquestengine.descriptorproperties.misc.IStatisticHolder;
import com.pdg.dungeondelver.hamquestengine.directions.Directions;
import com.pdg.dungeondelver.hamquestengine.gameconstants.CellTypes;
import com.pdg.dungeondelver.hamquestengine.gameconstants.ItemTypes;
import com.pdg.dungeondelver.hamquestengine.gameconstants.Properties;
import com.pdg.dungeondelver.hamquestengine.gameconstants.PropertyGroups;
import com.pdg.dungeondelver.hamquestengine.gameconstants.Tags;
import com.pdg.dungeondelver.hamquestengine.gamecore.Game;
import com.pdg.dungeondelver.hamquestengine.gamecore.IGameClient;
import com.pdg.dungeondelver.hamquestengine.item.Descriptor;
import com.pdg.dungeondelver.hamquestengine.maze.CellInfo;
import com.pdg.dungeondelver.hamquestengine.maze.Portal;
import com.pdg.dungeondelver.pdgboardgames.Board;
import com.pdg.dungeondelver.pdgboardgames.CQMFile;
import com.pdg.dungeondelver.pdgboardgames.CQMLoader;
import com.pdg.dungeondelver.pdgboardgames.MazeCellBase;
import com.pdg.dungeondelver.pdgboardgames.WeightedGenerator;

public class Map extends Board<MapCell> implements IGameClient
{
    private Game game;
    public Game getGame()
    {
            return game;
    }

    private static final String PassagewayBaseFilename = "pway%1.cqm";
    private static final String PassagewayDoorFilename = "pwaydoor%1.cqm";

    private static final String RoomBaseFilename = "room.cqm";
    private static final String RoomOpenFilename = "roomopen%1.cqm";
    private static final String RoomDoorFilename = "roomdoor%1.cqm";

    private static Creature playerCreature = null;
    public static Creature getPlayerCreature()
        {
            return (playerCreature);
        }
        public static void setPlayerCreature(Creature value)
        {
            playerCreature = value;
        }
    private List<Creature> creatures = new ArrayList<Creature>();
    private List<Creature> summonedCreatures = new ArrayList<Creature>();
    public List<Creature> getCreatures()
    {
            return (creatures);
    }
    public List<Creature> getSummonedCreatures()
    {
            return summonedCreatures;
    }
    private List<String> getMapFileList(MazeCellBase<Directions, Portal, CellInfo> cell)
    {
        List<String> result = new ArrayList<String>();
        Directions directions = new Directions();
        if (cell.getCellInfo().getCellType().equalsIgnoreCase(CellTypes.Passageway))
        {
            int direction;
            int pwaybase = 0;
            boolean[] locked = new boolean[directions.getCount()];
            for (direction = directions.getCount() - 1; direction >= 0; --direction)
            {
                Portal portal = cell.getPortals().get(direction);
                locked[direction] = false;
                if (portal != null)
                {
                    if (portal.Open)
                    {
                        pwaybase += (1 << direction);
                        if (portal.isLocked())
                        {
                            locked[direction] = true;
                        }
                    }
                }
            }
            result.add(String.format(PassagewayBaseFilename, pwaybase));
            for (direction = 0; direction < directions.getCount(); ++direction)
            {
                if (locked[direction])
                {
                    result.add(String.format(PassagewayDoorFilename, direction));
                }
            }
        }
        else if (cell.getCellInfo().getCellType().equalsIgnoreCase(CellTypes.Room))
        {
            result.add(RoomBaseFilename);
            int direction;
            for (direction = directions.getCount() - 1; direction >= 0; --direction)
            {
                Portal portal = cell.getPortals().get(direction);
                if (portal != null)
                {
                    if (portal.Open)
                    {
                        if (portal.isLocked())
                        {
                            result.add(String.format(RoomDoorFilename, direction));
                        }
                        else
                        {
                            result.add(String.format(RoomOpenFilename, direction));
                        }
                    }
                }
            }
        }
        return (result);
    }
    public void generateTerrain(MazeCellBase<Directions, Portal, CellInfo> mazeCell)
    {
        List<String> fileList = getMapFileList(mazeCell);
        CQMFile cqm = null;
        for (String fileName : fileList)
        {
            if (cqm == null)
            {
                cqm = CQMLoader.LoadFromFile("/HamQuestSLClient;component/maps/" + fileName);
            }
            else
            {
                cqm.blend(CQMLoader.LoadFromFile("/HamQuestSLClient;component/maps/" + fileName), (byte)0, (byte)0, (byte)255);
            }
        }
        int column;
        int row;
        for (column = 0; column < getColumns(); ++column)
        {
            for (row = 0; row < getRows(); ++row)
            {
                Byte cellValue = cqm.getCellValue((byte)column, (byte)row);
                this.get(column).get(row).TerrainIdentifier = cellValue.toString();
            }
        }
    }
    public void generate(MazeCellBase<Directions, Portal, CellInfo> mazeCell)
    {
        generateTerrain(mazeCell);
        int column;
        int row;
        for (String itemIdentifier : mazeCell.getCellInfo().Items.getIdentifiers())
        {
            int count = mazeCell.getCellInfo().Items.get(itemIdentifier);
            while (count > 0)
            {
                count--;
                int tally = 0;
                Descriptor itemDescriptor = getGame().getTableSet().getItemTable().getItemDescriptor(itemIdentifier);
                do
                {
                    column = itemDescriptor.<WeightedGenerator<Integer>>getProperty(Properties.XGenerator).generate(getGame().getRandomNumberGenerator());
                    row = itemDescriptor.<WeightedGenerator<Integer>>getProperty(Properties.YGenerator).generate(getGame().getRandomNumberGenerator());
                    tally++;
                    if (tally > 100)
                    {
                        System.out.println("blah");
                    }
                } while (this.get(column).get(row).getItemIdentifier() != "" || !getGame().getTableSet().getTerrainTable().getTerrainDescriptor(this.get(column).get(row).TerrainIdentifier).<Boolean>getProperty(Properties.Passable));
                boolean done = true;
                String theItemIdentifier = itemIdentifier;
                do
                {
                    done = true;
                    String newItemIdentifier = itemDescriptor.<WeightedGenerator<String>>getProperty(Properties.SpawnAs).generate(getGame().getRandomNumberGenerator());
                    if (newItemIdentifier != theItemIdentifier)
                    {
                        done = false;
                        theItemIdentifier = newItemIdentifier;
                        itemDescriptor = getGame().getTableSet().getItemTable().getItemDescriptor(theItemIdentifier);
                    }
                }
                while (!done);
                this.get(column).get(row).setItemIdentifier(theItemIdentifier);
            }
        }
        for (String creatureIdentifier : mazeCell.getCellInfo().Creatures.getIdentifiers())
        {
            int count = mazeCell.getCellInfo().Creatures.get(creatureIdentifier);
            while (count > 0)
            {
                count--;
                do
                {
                    column = getGame().getRandomNumberGenerator().next(getColumns() - 2) + 1;
                    row = getGame().getRandomNumberGenerator().next(getRows() - 2) + 1;
                } while (!getGame().getTableSet().getTerrainTable().getTerrainDescriptor(this.get(column).get(row).TerrainIdentifier).<Boolean>getProperty(Properties.Passable) || this.get(column).get(row).getItemIdentifier() != "" || this.get(column).get(row).Creature != null);
                Creature creature = new Creature(creatureIdentifier,column, row,getGame());
                this.get(column).get(row).Creature = creature;
                creature.Map = this;
                if (creatureIdentifier == "player")
                {
                    playerCreature = creature;
                }
                else
                {
                    getCreatures().add(creature);
                }
            }
        }
    }
    public void removeDeadCreatures()
    {
        List<Creature> deadList = new ArrayList<Creature>();
        for (Creature creature : getCreatures())
        {
            if (creature.isDead())
            {
                deadList.add(creature);
            }
        }
        for (Creature creature : deadList)
        {
            creature.remove();
            getCreatures().remove(creature);
        }
    }
    public void moveCreatures(float steps)
    {
        Directions directions = new Directions();
        for (Creature creature : getCreatures())
        {
            if (!creature.isDead())
            {
                creature.Steps += (float)getGame().getTableSet().getCreatureTable().getCreatureDescriptor(creature.getCreatureIdentifier()).<IStatisticHolder>getProperty("speed").getValue() * steps;
                while (creature.Steps >= 1.0f)
                {
                    creature.Steps -= 1.0f;
                    if (playerCreature != null)
                    {
                        //START OF CREATURE MOVEMENT CODE
                        Descriptor descriptor = getGame().getTableSet().getCreatureTable().getCreatureDescriptor(creature.getCreatureIdentifier());

                        if (!descriptor.<WeightedGenerator<Boolean>>getProperty(Properties.MoraleRoll).generate(getGame().getRandomNumberGenerator()))
                        {
                            creature.move(getGame().getRandomNumberGenerator().next(directions.getCount()));
                        }
                        else
                        {
                            int lowestValue = Integer.MAX_VALUE;
                            int chosenDirection = directions.getCount();
                            for (int direction = 0; direction < directions.getCount(); ++direction)
                            {
                                int nextColumn = directions.getNextColumn(creature.Column, creature.Row, direction);
                                int nextRow = directions.getNextRow(creature.Column, creature.Row, direction);
                                if (nextColumn < 0 || nextRow < 0 || nextColumn >= getColumns() || nextRow >= getRows()) continue;
                                if (this.get(nextColumn).get(nextRow).PathfindingValue == Integer.MAX_VALUE) continue;
                                if (this.get(nextColumn).get(nextRow).PathfindingValue < lowestValue)
                                {
                                    lowestValue = this.get(nextColumn).get(nextRow).PathfindingValue;
                                    chosenDirection = direction;
                                }
                            }
                            if (chosenDirection < directions.getCount())
                            {
                                creature.move(chosenDirection);
                            }
                        }
                        //END OF CREATURE MOVEMENT CODE
                    }
                }
            }
        }
        for (Creature summonedCreature : getSummonedCreatures())
        {
        	summonedCreature.Summoned = true;
            getCreatures().add(summonedCreature);
        }
        getSummonedCreatures().clear();


    }
    public Map(Game theGame)
    {
    	super(
    			theGame.getTableSet().getPropertyGroupTable().getPropertyDescriptor(PropertyGroups.MapDimensions).<Integer>getProperty(Properties.Columns), 
    			theGame.getTableSet().getPropertyGroupTable().getPropertyDescriptor(PropertyGroups.MapDimensions).<Integer>getProperty(Properties.Rows),
    			new MapFactory());
        game = theGame;
    }
    public boolean hasCreature()
    {
            for (int column = 0; column < getColumns(); ++column)
            {
                for (int row = 0; row < getRows(); ++row)
                {
                    if (this.get(column).get(row).Creature != null && this.get(column).get(row).Creature != Map.getPlayerCreature())
                    {
                        return true;
                    }
                }
            }
            return false;
    }
    public boolean hasSpawn()
    {
        for (int column = 0; column < getColumns(); ++column)
        {
            for (int row = 0; row < getRows(); ++row)
            {
            	String identifier = this.get(column).get(row).getItemIdentifier();
                if (identifier != "" && getGame().getTableSet().getItemTable().getItemDescriptor(identifier).hasTag(Tags.Spawns))
                {
                    return true;
                }
            }
        }
        return false;
    }
    public boolean hasTrap()
    {
            for (int column = 0; column < getColumns(); ++column)
            {
                for (int row = 0; row < getRows(); ++row)
                {
                	String identifier = this.get(column).get(row).getItemIdentifier();
                    if (identifier != "" && getGame().getTableSet().getItemTable().getItemDescriptor(identifier).hasTag(Tags.Trap))
                    {
                        return true;
                    }
                }
            }
            return false;
    }
    public int getSpawnCount()
    {
            int count = 0;
            for (int column = 0; column < getColumns(); ++column)
            {
                for (int row = 0; row < getRows(); ++row)
                {
                    String identifier = this.get(column).get(row).getItemIdentifier();
                    if (identifier != "" && getGame().getTableSet().getItemTable().getItemDescriptor(identifier).hasTag(Tags.Spawns))
                    {
                        count++;
                    }
                }
            }
            return count;
    }
    public int getTrapCount()
    {
            int count = 0;
            for (int column = 0; column < getColumns(); ++column)
            {
                for (int row = 0; row < getRows(); ++row)
                {
                    String identifier = this.get(column).get(row).getItemIdentifier();
                    if (identifier != "" && getGame().getTableSet().getItemTable().getItemDescriptor(identifier).<Boolean>getProperty("isTrap"))
                    {
                        count++;
                    }
                }
            }
            return count;
    }
    public void revealHiddens(int column, int row)
    {
        if (column >= 0 && row >= 0 && column < getColumns() && row < getRows())
        {
            String itemIdentifier = this.get(column).get(row).getItemIdentifier();
            if (itemIdentifier != "")
            {
                Descriptor itemDescriptor = getGame().getTableSet().getItemTable().getItemDescriptor(itemIdentifier);
                if (itemDescriptor.<String>getProperty(Properties.ItemType).equalsIgnoreCase(ItemTypes.Hidden))
                {
                	this.get(column).get(row).setItemIdentifier(itemDescriptor.<String>getProperty("hidden-item-identifier"));
                }
            }
        }
    }
    public boolean hasItem()
    {
            return hasItemTag(Tags.Item);
    }
    public boolean hasItemTag(String tag)
    {
        for (int column = 0; column < getColumns(); ++column)
        {
            for (int row = 0; row < getRows(); ++row)
            {
                String identifier = this.get(column).get(row).getItemIdentifier();
                if (!(identifier==null || identifier=="") && getGame().getTableSet().getItemTable().getItemDescriptor(identifier).hasTag(tag))
                {
                    return true;
                }
            }
        }
        return false;
    }
    public boolean hasPortal()
    {
            return hasItemTag(Tags.Portal);
    }
    public boolean hasQuest()
    {
            return hasItemTag(Tags.Quest);
    }
    public boolean hasExit()
    {
            return hasItemTag(Tags.Exit);
    }
}
