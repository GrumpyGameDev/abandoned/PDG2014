package com.pdg.dungeondelver.pdgboardgames;

public interface IRandomNumberGenerator {
    Integer next(Integer maximum);
    Integer next(Integer minimum, Integer maximum);
}
