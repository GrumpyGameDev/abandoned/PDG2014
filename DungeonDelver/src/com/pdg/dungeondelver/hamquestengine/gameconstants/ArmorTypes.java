package com.pdg.dungeondelver.hamquestengine.gameconstants;

public class ArmorTypes {
    public static final String Body = "Body";
    public static final String Feet = "Feet";
    public static final String Helmet = "Helmet";
    public static final String Shield = "Shield";
    public static final String Torso = "Torso";
}
