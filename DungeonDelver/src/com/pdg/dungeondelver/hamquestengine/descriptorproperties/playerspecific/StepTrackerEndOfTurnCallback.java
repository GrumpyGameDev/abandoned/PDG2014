package com.pdg.dungeondelver.hamquestengine.descriptorproperties.playerspecific;

public interface StepTrackerEndOfTurnCallback {
	void endOfTurn();
}
