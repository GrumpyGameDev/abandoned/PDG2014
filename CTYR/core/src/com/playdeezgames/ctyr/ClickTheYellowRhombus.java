package com.playdeezgames.ctyr;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.FitViewport;

public class ClickTheYellowRhombus extends ApplicationAdapter {
	private static final float SCREEN_WIDTH = 512;
	private static final float SCREEN_HEIGHT = 512;
	private Stage stage;
	private TextureManager<CtyrTextures> textureManager;
	private BitmapFont font;
	
	@Override
	public void create () {
		textureManager = new TextureManager<CtyrTextures>();
		stage = new Stage(new FitViewport(SCREEN_WIDTH,SCREEN_HEIGHT));
		Image image = new Image(textureManager.get(CtyrTextures.RHOMBUS));
		stage.addActor(image);
		font = new BitmapFont();
		TextButton.TextButtonStyle style = new TextButton.TextButtonStyle();
		style.up=new TextureRegionDrawable(new TextureRegion(textureManager.get(CtyrTextures.RHOMBUS)));
		style.down=style.up;
		style.checked=style.up;
		style.font=font;
		TextButton button = new TextButton("Test",style);
		stage.addActor(button);
		Gdx.input.setInputProcessor(stage);
	}

	@Override
	public void render () {
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		stage.act(Gdx.graphics.getDeltaTime());
		stage.draw();
	}
	
	public void dispose(){
		stage.dispose();
	}
	
	public void resize(int width,int height){
		stage.getViewport().update(width, height,true);
	}
}
