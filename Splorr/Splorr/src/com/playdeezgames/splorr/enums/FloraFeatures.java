package com.playdeezgames.splorr.enums;

public enum FloraFeatures {
	MOSS("There is moss here."),
	GRASS("There is grass here."),
	FLOWERS("There are flowers here."),
	CONIFEROUS_TREES("There are evergreen trees here."),
	DECIDUOUS_TREES("There are trees here."),
	CACTI("There are cacti here."),
	SHRUBS("There are shrubs here."),
	BERRY_BUSHES("There are berry bushes here.");
	private String description;
	FloraFeatures(String description){
		this.description = description;
	}
	public String getDescription(){
		return description;
	}
}
