package com.playdeezgames.splorr.items;

import com.playdeezgames.splorr.enums.Items;

public interface IItem {
	Items getItem();
}
