package com.playdeezgames.splorr.world;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class WorldColumn implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1891995437918502498L;
	private Map<Integer,WorldCell> cells = new HashMap<Integer,WorldCell>();
	
	public WorldCell get(Integer row, WorldCell fromNeighbor, Random random){
		if(cells.containsKey(row)){
			return cells.get(row);
		}else{
			if(fromNeighbor!=null){
				WorldCell cell = fromNeighbor.generateNeighbor(random);
				cells.put(row,cell);
				return cell;
			}else{
				WorldCell cell = new WorldCell(random);
				cells.put(row,cell);
				return cell;
			}
		}
	}
}
