package com.pdg.dungeondelver.pdgboardgames;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;

public class CQMLoader
{
    public static CQMFile LoadFromFile(String fileName)
    {
        CQMFile result = null;
        FileHandle handle = Gdx.files.internal(fileName);
        byte[] theBytes = handle.readBytes();
        int index=0;
        byte width = theBytes[index++];
        byte height = theBytes[index++];
        result = new CQMFile(width, height);
        for (byte y = 0; y < result.getHeight(); ++y)
        {
            for (byte x = 0; x < result.getWidth(); ++x)
            {
                result.setCellValue(x, y, theBytes[index++]);
            }
        }
        return (result);
    }
}
