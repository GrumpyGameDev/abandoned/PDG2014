package com.playdeezgames.splorr.tokenprocessors;

import java.util.Stack;
import com.playdeezgames.splorr.world.Avatar;

public class SaveTokenProcessor implements ITokenProcessor {

	public boolean processTokens(Stack<String> tokenizer, Avatar avatar) {
		if(tokenizer.empty()){
			if(avatar.save()){
				System.out.println();
				System.out.println("Game saved.");
			}else{
				System.out.println();
				System.out.println("Could not save game.");
			}
			return true;
		}else{
			return false;
		}
	}

}
