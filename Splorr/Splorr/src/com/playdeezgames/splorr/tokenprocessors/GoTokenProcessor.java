package com.playdeezgames.splorr.tokenprocessors;

import java.util.Stack;
import com.playdeezgames.splorr.world.Avatar;

public class GoTokenProcessor implements ITokenProcessor {

	private static final String LEFT = "left";
	private static final String RIGHT = "right";
	private static final String BACKWARD = "backward";
	private static final String FORWARD = "forward";
	
	
	public boolean processTokens(Stack<String> tokenizer, Avatar avatar) {
		if(tokenizer.empty()){
			System.out.println();
			System.out.println("You go forward.");
			avatar.goForward();
			return true;
		}else{
			String turnDirection = tokenizer.pop();
			if(!tokenizer.empty()){
				return false;
			}else if(turnDirection.compareToIgnoreCase(LEFT)==0){
				System.out.println();
				System.out.println("You go left.");
				avatar.goLeft();
				return true;
			}else if(turnDirection.compareToIgnoreCase(RIGHT)==0){
				System.out.println();
				System.out.println("You go right.");
				avatar.goRight();
				return true;
			}else if(turnDirection.compareToIgnoreCase(BACKWARD)==0){
				System.out.println();
				System.out.println("You go backward.");
				avatar.goBackward();
				return true;
			}else if(turnDirection.compareToIgnoreCase(FORWARD)==0){
				System.out.println();
				System.out.println("You go forward.");
				avatar.goForward();
				return true;
			}else{
				return false;
			}
		}
	}

}
