package com.playdeezgames.common.managers;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.playdeezgames.common.interfaces.FileDescriptor;

public class FileManager<T extends FileDescriptor> extends ManagerBase{
	private Map<T,FileHandle> table = new HashMap<T,FileHandle>();
	public FileHandle getFile(T descriptor){
		if(table.containsKey(descriptor)){
			return table.get(descriptor);
		}else{
			FileHandle file = Gdx.files.internal(descriptor.getFileName());
			table.put(descriptor, file);
			return file;
		}
	}
	@Override
	public void dispose() {
		table.clear();
	}

}
