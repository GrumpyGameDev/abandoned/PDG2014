package com.pdg.dungeondelver.hamquestengine.gameconstants;

public class FileNames {
    public static final String DefaultCreaturesFile	="config/creatures.xml";
    public static final String DefaultPropertyGroupsFile	="config/propertyGroups.xml";
    public static final String DefaultThemesFile = "config/themes.xml";
    public static final String DefaultItemsFile = "config/items.xml";
	public static final String DefaultMessagesFile = "config/messages.xml";
	public static final String DefaultTerrainsFile = "config/terrains.xml";
}
