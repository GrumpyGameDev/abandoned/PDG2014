package com.pdg.dungeondelver.hamquestengine.gameconstants;

public class PropertyGroups {
    public static final String ColorTable = "colorTable";
    public static final String GameConfiguration = "gameConfiguration";
    public static final String Generators = "generators";
    public static final String PlayerConstants = "playerConstants";
    public static final String RoomChance = "roomChance";
    public static final String MazeDimensions = "mazeDimensions";
    public static final String MapDimensions = "mapDimensions";
}
