package com.pdg.dungeondelver.hamquestengine.item;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.pdg.dungeondelver.hamquestengine.descriptorproperties.misc.IStatisticHolder;
import com.pdg.dungeondelver.hamquestengine.gameconstants.Properties;

public class Descriptor
{
    private Map<String, Object> properties = new HashMap<String, Object>();
    protected boolean hasPropertyIntercept(String property)
    {
        return false;
    }
    public boolean hasProperty(String property)
    {
        if (hasPropertyIntercept(property))
        {
            return true;
        }
        else
        {
            return properties.containsKey(property);
        }
        
    }
    private void setProperty(String property, Object value)
    {
        if (value == null)
        {
            if (hasProperty(property))
            {
                properties.remove(property);
            }
        }
        else
        {
            properties.put(property,value);
        }
    }
    protected <E> E getPropertyIntercept(String property)
    {
        return null;
    }
    public <E> E getProperty(String property)
    {
        if (hasPropertyIntercept(property))
        {
            return getPropertyIntercept(property);
        }
        else
        {
            @SuppressWarnings("unchecked")
			E e = (E)properties.get(property);
            return e;
        }
    }
    protected void addPropertyValues(List<PropertyValuePair> thePropertyValues)
    {
        for (PropertyValuePair propertyValue : thePropertyValues)
        {
            setProperty(propertyValue.getProperty(), propertyValue.getValue());
        }
    }
    public Descriptor(List<PropertyValuePair> thePropertyValues)
    {
        addPropertyValues(thePropertyValues);
    }
    public boolean hasTag(String theTag)
    {
        Set<String> theHashSet = this.<Set<String>>getProperty(Properties.Tags);
        if (theHashSet == null) return false;
        return theHashSet.contains(theTag);
    }
    public int getHealth()
    {
        IStatisticHolder holder = this.<IStatisticHolder>getProperty(Properties.Health);
        if (holder == null) return 0;
        return holder.getValue();
    }
}

