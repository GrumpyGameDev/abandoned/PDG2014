package com.pdg.dungeondelver.hamquestengine.gameconstants;

public class CellTypes {
    public static final String Passageway = "Passageway";
    public static final String Room = "Room";
}
