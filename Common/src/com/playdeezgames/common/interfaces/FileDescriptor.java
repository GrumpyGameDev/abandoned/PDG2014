package com.playdeezgames.common.interfaces;

public interface FileDescriptor {
	String getFileName();
}
