package com.pdg.dungeondelver.hamquestengine.maze;

import com.pdg.dungeondelver.pdgboardgames.PortalFactory;

public class MazePortalFactory implements PortalFactory<Portal> {

	@Override
	public Portal newInstance() {
		return new Portal();
	}

}
