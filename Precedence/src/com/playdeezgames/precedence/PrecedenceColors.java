package com.playdeezgames.precedence;

public enum PrecedenceColors {
	RED,
	YELLOW,
	GREEN,
	CYAN,
	BLUE,
	MAGENTA;
}
