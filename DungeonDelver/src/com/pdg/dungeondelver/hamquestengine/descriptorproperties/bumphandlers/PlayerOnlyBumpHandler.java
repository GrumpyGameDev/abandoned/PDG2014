package com.pdg.dungeondelver.hamquestengine.descriptorproperties.bumphandlers;

import com.pdg.dungeondelver.hamquestengine.creatures.PlayerDescriptor;
import com.pdg.dungeondelver.hamquestengine.item.Descriptor;
import com.pdg.dungeondelver.hamquestengine.maps.Creature;

public class PlayerOnlyBumpHandler implements IBumpHandler
{
    public BumpResult bump(Descriptor theDescriptor, Creature theCreature)
    {
        String creatureIdentifier = theCreature.getCreatureIdentifier();
        Descriptor creatureDescriptor = theCreature.getGame().getTableSet().getCreatureTable().getCreatureDescriptor(creatureIdentifier);
        if (creatureDescriptor instanceof PlayerDescriptor)
        {
            return BumpResult.Allow;
        }
        else
        {
            return BumpResult.Deny;
        }
    }
}
