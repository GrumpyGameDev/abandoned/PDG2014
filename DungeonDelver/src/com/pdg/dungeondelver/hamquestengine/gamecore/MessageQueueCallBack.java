package com.pdg.dungeondelver.hamquestengine.gamecore;

import com.badlogic.gdx.graphics.Color;

public interface MessageQueueCallBack {
	void handle(String message, Color color);
}
