package com.pdg.dungeondelver.pdgboardgames;

public interface BoardCellFactory<CellType extends BoardCellBase> {
	CellType newInstance();
}
