package com.pdg.dungeondelver.pdgboardgames;

import java.util.Random;

public class RandomNumberGenerator implements IRandomNumberGenerator{
	private Random random = new Random();
	@Override
	public Integer next(Integer maximum) {
		return random.nextInt(maximum);
	}

	@Override
	public Integer next(Integer minimum, Integer maximum) {
		return random.nextInt(maximum-minimum+1)+minimum;
	}

}
