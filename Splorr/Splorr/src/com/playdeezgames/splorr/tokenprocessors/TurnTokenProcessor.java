package com.playdeezgames.splorr.tokenprocessors;

import java.util.Stack;
import com.playdeezgames.splorr.world.Avatar;

public class TurnTokenProcessor implements ITokenProcessor {

	private static final String LEFT = "left";
	private static final String RIGHT = "right";
	private static final String AROUND = "around";
	
	public boolean processTokens(Stack<String> tokenizer, Avatar avatar) {
		if(tokenizer.empty()){
			return false;
		}else{
			String turnDirection = tokenizer.pop();
			if(!tokenizer.empty()){
				return false;
			}else if(turnDirection.compareToIgnoreCase(LEFT)==0){
				System.out.println();
				System.out.println("You turn left.");
				avatar.turnLeft();
				return true;
			}else if(turnDirection.compareToIgnoreCase(RIGHT)==0){
				System.out.println();
				System.out.println("You turn right.");
				avatar.turnRight();
				return true;
			}else if(turnDirection.compareToIgnoreCase(AROUND)==0){
				System.out.println();
				System.out.println("You turn around.");
				avatar.turnAround();
				return true;
			}else{
				return false;
			}
		}
	}

}
