package com.pdg.dungeondelver.hamquestengine.maze;

import com.pdg.dungeondelver.pdgboardgames.MazePortalBase;

public class Portal extends MazePortalBase
{
    private int lockType = 0;
    private boolean locked = false;
    public int getLockType()
    {
            return (lockType);
        }
	public void setLockType(int value)
        {
            lockType = value;
        }
    public boolean isLocked()
        {
            return (locked);
        }
        public void setLocked(boolean value)
        {
            locked = value;
        }
    public Portal()
    {
    }
}
