package com.playdeezgames.ctyr;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.utils.Disposable;

public class TextureManager<T extends TextureDescriptor> implements Disposable {
	private Map<T,Texture> table = new HashMap<T,Texture>();
	
	public TextureManager(){
	}
	
	public Texture get(T index){
		if(table.containsKey(index)){
			return table.get(index);
		}else{
			Texture texture = new Texture(index.getFilename());
			table.put(index, texture);
			return texture;
		}
	}

	@Override
	public void dispose() {
		for(Texture texture:table.values()){
			texture.dispose();
		}
	}
}
