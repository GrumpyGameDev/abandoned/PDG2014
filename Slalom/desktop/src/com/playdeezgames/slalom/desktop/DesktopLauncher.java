package com.playdeezgames.slalom.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.playdeezgames.slalom.Slalom;

public class DesktopLauncher {
	
	private static final int SCREEN_WIDTH=1280;
	private static final int SCREEN_HEIGHT=720;
	
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width=SCREEN_WIDTH;
		config.height=SCREEN_HEIGHT;
		config.resizable=false;
		new LwjglApplication(new Slalom(), config);
	}
}
