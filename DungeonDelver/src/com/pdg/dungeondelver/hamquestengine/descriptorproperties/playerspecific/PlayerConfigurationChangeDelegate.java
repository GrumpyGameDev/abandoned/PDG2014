package com.pdg.dungeondelver.hamquestengine.descriptorproperties.playerspecific;

public interface PlayerConfigurationChangeDelegate {
	void onChange();
}
