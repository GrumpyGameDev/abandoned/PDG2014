package com.playdeezgames.splorr.world;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.gamejolt.GameJoltAPI;

import com.playdeezgames.splorr.enums.Directions;
import com.playdeezgames.splorr.enums.Hands;
import com.playdeezgames.splorr.enums.TimeOfDay;
import com.playdeezgames.splorr.generators.WeightedGenerator;
import com.playdeezgames.splorr.items.IItem;

public class Avatar implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7817405072643319352L;
	private static final Directions DIRECTION_DEFAULT = Directions.NORTH;
	private static final int COLUMN_DEFAULT = 0;
	private static final int ROW_DEFAULT = 0;
	private static final boolean QUIT_DEFAULT = false;
	private static final double TIME_DEFAULT = 0;
	
	private static final int GAME_ID=44182;
	private static final String API_KEY="09f5c14717615ff26996b746f1ae3b53";
	private static final int AUTHENTICATE_TROPHY_ID = 14953;
	
	private Directions direction;
	private int column;
	private int row;
	private boolean quit;
	private Random random;
	private World world;
	private WorldCell lastCell;
	private double time;
	private String userName;
	private String userToken;
	private Map<Hands,IItem> heldItems = new HashMap<Hands,IItem>();
	
	public Avatar(){
		setDirection(DIRECTION_DEFAULT);
		setColumn(COLUMN_DEFAULT);
		setRow(ROW_DEFAULT);
		setQuit(QUIT_DEFAULT);
		setRandom(new Random());
		setWorld(new World());
		setTime(TIME_DEFAULT);
		setHeldItem(Hands.LEFT,null);
		setHeldItem(Hands.RIGHT,null);
		lastCell = null;
	}

	public void setHeldItem(Hands hand, IItem item) {
		heldItems.put(hand,item);
	}
	
	public IItem getHeldItem(Hands hand){
		return heldItems.get(hand);
	}
	
	public Hands getEmptyHand(){
		WeightedGenerator<Hands> generator = new WeightedGenerator<Hands>();
		for(Hands hand : Hands.values()){
			if(getHeldItem(hand)==null){
				generator.put(hand, 1);
			}
		}
		return generator.generate(getRandom());
	}

	public Directions getDirection() {
		return direction;
	}

	public void setDirection(Directions direction) {
		this.direction = direction;
	}

	public int getColumn() {
		return column;
	}

	public void setColumn(int column) {
		this.column = column;
	}

	public int getRow() {
		return row;
	}

	public void setRow(int row) {
		this.row = row;
	}

	public boolean isQuit() {
		return quit;
	}

	public void setQuit(boolean quit) {
		this.quit = quit;
	}

	public void turnLeft() {
		setDirection(getDirection().left());
	}

	public void turnRight() {
		setDirection(getDirection().right());
	}

	public void turnAround() {
		setDirection(getDirection().opposite());
	}

	public void goForward() {
		go(getDirection());
	}

	private void go(Directions goDirection) {
		
		WorldCell oldCell = this.getCurrentCell();
		addTime(oldCell.getBiome().getTravelTime()/2);
		
		int nextColumn = goDirection.nextColumn(getColumn(), getRow());
		int nextRow = goDirection.nextRow(getColumn(), getRow());
		setColumn(nextColumn);
		setRow(nextRow);
		
		WorldCell newCell = this.getCurrentCell();
		addTime(newCell.getBiome().getTravelTime()/2);
	}

	private void addTime(double deltaTime) {
		setTime(getTime()+deltaTime);
	}

	public void goLeft() {
		go(getDirection().left());
	}

	public void goRight() {
		go(getDirection().right());
	}

	public void goBackward() {
		go(getDirection().opposite());
	}

	public Random getRandom() {
		return random;
	}

	private void setRandom(Random random) {
		this.random = random;
	}

	public World getWorld() {
		return world;
	}

	private void setWorld(World world) {
		this.world = world;
	}
	
	public WorldCell getCurrentCell(){
		lastCell=getWorld().get(getColumn()).get(getRow(),lastCell,getRandom());
		return lastCell;
	}

	public double getTime() {
		return time;
	}

	public void setTime(double time) {
		this.time = time;
	}
	
	public TimeOfDay getTimeOfDay(){
		double timeOfDay = getTime()-Math.floor(getTime());
		for(TimeOfDay tod : TimeOfDay.values()){
			if(timeOfDay>= tod.getStart() && timeOfDay<tod.getEnd()){
				return tod;
			}
		}
		return null;
	}

	public boolean save() {
		try{
			FileOutputStream fileOut = new FileOutputStream("save.splorr");
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(this);
			out.close();
			fileOut.close();
			return true;
		}catch(IOException i){
			return false;
		}
	}
	
	public boolean authenticate(String userName, String userToken){
		this.userName = userName;
		this.userToken = userToken;
		return achieveTrophy(AUTHENTICATE_TROPHY_ID);
	}

	private boolean achieveTrophy(int authenticateTrophyId) {
		if(this.userName==null){
			return false;
		}
		GameJoltAPI api = new GameJoltAPI(GAME_ID,API_KEY,this.userName,this.userToken);
		if(api.isVerified()){
			return api.achieveTrophy(authenticateTrophyId);
		}else{
			this.userName=null;
			this.userToken=null;
			return false;
		}
	}
}
