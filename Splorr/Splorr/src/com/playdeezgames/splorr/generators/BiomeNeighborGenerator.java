package com.playdeezgames.splorr.generators;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import com.playdeezgames.splorr.enums.Biomes;

public class BiomeNeighborGenerator {
	private static Map<Biomes,IGenerator<Biomes>> biomeNeighborGenerators = null;
	
	public static Biomes generateNeighborBiome(Biomes fromBiome, Random random) {
		if(biomeNeighborGenerators==null){
			biomeNeighborGenerators = new HashMap<Biomes,IGenerator<Biomes>>();
			biomeNeighborGenerators.put(Biomes.ALPINE, new WeightedGenerator<Biomes>()
					.put(Biomes.ALPINE, 10)
					.put(Biomes.CHAPARRAL, 1)
					.put(Biomes.DESERT, 1)
					.put(Biomes.FOREST, 1)
					.put(Biomes.GRASSLAND, 1)
					.put(Biomes.RAINFOREST, 1)
					.put(Biomes.SAVANNA, 1)
					.put(Biomes.SCRUB, 1)
					.put(Biomes.TAIGA, 1)
					.put(Biomes.TUNDRA, 1)
					);
			biomeNeighborGenerators.put(Biomes.CHAPARRAL, new WeightedGenerator<Biomes>()
					.put(Biomes.CHAPARRAL, 5)
					.put(Biomes.FOREST, 5)
					.put(Biomes.GRASSLAND, 5)
					.put(Biomes.SCRUB, 2)
					.put(Biomes.ALPINE, 1)
					);
			biomeNeighborGenerators.put(Biomes.DESERT, new WeightedGenerator<Biomes>()
					.put(Biomes.DESERT, 10)
					.put(Biomes.SCRUB, 5)
					.put(Biomes.GRASSLAND, 2)
					.put(Biomes.ALPINE, 1)
					);
			biomeNeighborGenerators.put(Biomes.FOREST, new WeightedGenerator<Biomes>()
					.put(Biomes.FOREST, 10)
					.put(Biomes.TAIGA, 5)
					.put(Biomes.GRASSLAND, 5)
					.put(Biomes.ALPINE, 1)
					.put(Biomes.CHAPARRAL, 2)
					.put(Biomes.RAINFOREST, 2)
					.put(Biomes.SCRUB, 2)
					);
			biomeNeighborGenerators.put(Biomes.GRASSLAND, new WeightedGenerator<Biomes>()
					.put(Biomes.GRASSLAND, 5)
					.put(Biomes.ALPINE, 5)
					.put(Biomes.FOREST, 5)
					.put(Biomes.DESERT, 2)
					.put(Biomes.CHAPARRAL, 2)
					.put(Biomes.GRASSLAND, 10)
					);
			biomeNeighborGenerators.put(Biomes.RAINFOREST, new WeightedGenerator<Biomes>()
					.put(Biomes.RAINFOREST, 10)
					.put(Biomes.SAVANNA, 5)
					.put(Biomes.FOREST, 2)
					.put(Biomes.ALPINE, 1)
					);
			biomeNeighborGenerators.put(Biomes.SAVANNA, new WeightedGenerator<Biomes>()
					.put(Biomes.SAVANNA, 10)
					.put(Biomes.RAINFOREST, 5)
					.put(Biomes.SCRUB, 5)
					.put(Biomes.FOREST, 2)
					.put(Biomes.ALPINE, 1)
					);
			biomeNeighborGenerators.put(Biomes.SCRUB, new WeightedGenerator<Biomes>()
					.put(Biomes.SCRUB, 10)
					.put(Biomes.DESERT, 10)
					.put(Biomes.GRASSLAND, 2)
					.put(Biomes.ALPINE, 1)
					.put(Biomes.FOREST, 2)
					.put(Biomes.SAVANNA, 5)
					);
			biomeNeighborGenerators.put(Biomes.TAIGA, new WeightedGenerator<Biomes>()
					.put(Biomes.TAIGA, 10)
					.put(Biomes.TUNDRA, 5)
					.put(Biomes.ALPINE, 1)
					.put(Biomes.FOREST, 5)
					.put(Biomes.GRASSLAND, 2)
					);
			biomeNeighborGenerators.put(Biomes.TUNDRA, new WeightedGenerator<Biomes>()
					.put(Biomes.TUNDRA, 10)
					.put(Biomes.TAIGA, 5)
					.put(Biomes.ALPINE, 1)
					);
		}
		return biomeNeighborGenerators.get(fromBiome).generate(random);
	}

}
