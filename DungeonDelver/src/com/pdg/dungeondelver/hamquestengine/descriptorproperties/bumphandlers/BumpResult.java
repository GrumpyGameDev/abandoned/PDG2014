package com.pdg.dungeondelver.hamquestengine.descriptorproperties.bumphandlers;

public enum BumpResult
{
    Deny,
    Allow;
}
