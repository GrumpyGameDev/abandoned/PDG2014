package com.pdg.dungeondelver.hamquestengine.maps;

import com.pdg.dungeondelver.pdgboardgames.BoardCellBase;

public class MapCell extends BoardCellBase
{
    public String TerrainIdentifier;
    public String getItemIdentifier()
        {
            return theItemIdentifier;
        }
        public void setItemIdentifier(String value)
        {
            theItemIdentifier = value;
        }
    private String theItemIdentifier = "";
    public Creature Creature = null;
    public int PathfindingValue = 0;
    @Override
    public void clear()
    {
        setItemIdentifier("");
        TerrainIdentifier = "0";
    }
    public Object Clone()
    {
        MapCell result = new MapCell();
        result.TerrainIdentifier = this.TerrainIdentifier;
        result.setItemIdentifier(this.getItemIdentifier());
        //TODO fix DDMapCreature cloning
        return (Object)result;
    }
    public MapCell()
    {
    }


}
