package com.playdeezgames.common.board;

public interface BoardCellFactory<D extends DirectionDescriptor<D>,T extends BoardCell<D,T>> {
	T create();
}
