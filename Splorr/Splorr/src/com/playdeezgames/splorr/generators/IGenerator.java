package com.playdeezgames.splorr.generators;

import java.util.Random;

public interface IGenerator<T> {
	T generate(Random random);
}
