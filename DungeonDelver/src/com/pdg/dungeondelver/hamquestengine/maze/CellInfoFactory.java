package com.pdg.dungeondelver.hamquestengine.maze;

import com.pdg.dungeondelver.pdgboardgames.MazeCellInfoFactory;

public class CellInfoFactory implements MazeCellInfoFactory<CellInfo> {

	@Override
	public CellInfo newInstance() {
		return new CellInfo();
	}

}
